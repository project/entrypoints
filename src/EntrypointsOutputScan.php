<?php

namespace Drupal\entrypoints;

use Drupal\Component\Serialization\SerializationInterface;
use Drupal\Core\File\FileSystemInterface;

/**
 * The entrypoints output scan service.
 */
class EntrypointsOutputScan {

  use OutputDirectoryTrait;

  /**
   * The app root path.
   *
   * @var string
   */
  protected $appRoot;

  /**
   * The local uri resolver.
   *
   * @var \Drupal\entrypoints\LocalUriResolver
   */
  protected $localUriResolver;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The JSON serialization service.
   *
   * @var \Drupal\Component\Serialization\SerializationInterface
   */
  protected $jsonSerialization;

  /**
   * The EntrypointsOutputScan service constructor.
   *
   * @param \Drupal\entrypoints\LocalUriResolver $local_uri_resolver
   *   The local uri resolver.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \Drupal\Component\Serialization\SerializationInterface $json_serialization
   *   The JSON serialization service.
   */
  public function __construct(LocalUriResolver $local_uri_resolver, FileSystemInterface $file_system, SerializationInterface $json_serialization) {
    $this->appRoot = _entrypoints_app_root();
    $this->localUriResolver = $local_uri_resolver;
    $this->fileSystem = $file_system;
    $this->jsonSerialization = $json_serialization;
  }

  /**
   * Scans the given uri for entrypoint definitions, manifest and stale files.
   *
   * @param string $uri
   *   The uri that contains the entrypoints.json file and asset builds.
   *
   * @return array
   *   An associative array, keyed by 'asset_files', 'manifest' and
   *   'stale_files'. The entrypoint asset files are grouped per entrypoint
   *   definition by 'css' and 'js' assets. The asset definition values are
   *   either absolute paths to the asset files, i.e. realpath values, or an
   *   absolute URL. The keys are their original URL from the entrypoints.json
   *   definition. The 'stale_files' array has the local file uri both as key
   *   and value. Stale files are those which are not being referenced by the
   *   currently existing entrypoints.json and manifest.json definitions, and
   *   are not license-related definition-related files (like manifest.json and
   *   entrypoints.json).
   *
   * @throws \Drupal\entrypoints\EntrypointsInvalidUriException
   *   In case the given uri does not resolve properly to an existing
   *   entrypoints output directory.
   * @throws \Drupal\entrypoints\EntrypointsNotFoundException
   *   In case given definitions point to missing asset files, or the
   *   entrypoints.json file itself either does not exist, is not readable
   *   or is malformed.
   */
  public function scan($uri) {
    $asset_files = [];
    $stale_files = [];
    $manifest = [];

    $uri = $this->outputFolderTransformRelative($uri);
    $realpath = $this->localUriResolver->getRealpath($uri);
    if (@is_file($realpath)) {
      throw new EntrypointsInvalidUriException($uri, "The given entrypoints uri ${uri} is a file, not a directory.");
    }
    if (!@is_dir($realpath)) {
      throw new EntrypointsNotFoundException($uri, "The entrypoints directory ${uri} does not exist.");
    }
    if (!($entrypoints_json = @file_get_contents($realpath . DIRECTORY_SEPARATOR . 'entrypoints.json'))) {
      throw new EntrypointsNotFoundException($uri, "The entrypoints directory ${uri} does not contain a readable entrypoints.json file. Please make sure that this file exists and is readable.");
    }
    if (!($manifest_json = @file_get_contents($realpath . DIRECTORY_SEPARATOR . 'manifest.json'))) {
      throw new EntrypointsNotFoundException($uri, "The entrypoints directory ${uri} does not contain a readable manifest.json file. Please make sure that this file exists and is readable.");
    }
    if (!($entrypoints_json = $this->jsonSerialization->decode($entrypoints_json))) {
      throw new EntrypointsNotFoundException($uri, "The entrypoints.json file is empty, could not be read, or contains invalid JSON at ${uri}. Please make sure that this file is readable and valid.");
    }
    if (!($manifest_json = $this->jsonSerialization->decode($manifest_json))) {
      throw new EntrypointsNotFoundException($uri, "The manifest.json file is empty, could not be read, or contains invalid JSON at ${uri}. Please make sure that this file is readable and valid.");
    }
    if (empty($entrypoints_json['entrypoints'])) {
      throw new EntrypointsNotFoundException($uri, "The entrypoints.json file does not contain any 'entrypoints' key with entrypoint definitions. Please make sure that this file contains valid entries.");
    }
    $existing_files = $this->fileSystem->scanDirectory($realpath, '/.*/');
    // The Drupal file system service does not consistently work with directory
    // separators on different operating systems. To avoid false mismatches,
    // it needs to be made sure that the file path values contain only one type
    // of separator.
    if (\DIRECTORY_SEPARATOR !== '/') {
      foreach ($existing_files as $uri => $file) {
        unset($existing_files[$uri]);
        $uri = str_replace('/', DIRECTORY_SEPARATOR, $uri);
        $file->uri = $uri;
        $existing_files[$uri] = $file;
      }
    }
    $used_files = [];
    foreach ($entrypoints_json['entrypoints'] as $name => $info) {
      $asset_files[$name] = ['js' => [], 'css' => []];
      $keys = ['js', 'css'];
      foreach ($keys as $key) {
        if (!empty($info[$key])) {
          foreach ($info[$key] as $file_url) {
            if ($file_path = $this->transformRealpath($file_url)) {
              if (isset($existing_files[$file_path]) || strpos($file_path, '//') !== FALSE) {
                $asset_files[$name][$key][$file_url] = $file_path;
                $used_files[$file_path] = TRUE;
                continue;
              }
            }
            throw new EntrypointsNotFoundException($uri, "The entrypoints.json file within ${uri} points to an asset file ${file_url} that could not be found within the entrypoints output folder.");
          }
        }
      }
    }

    foreach ($manifest_json as $key => $file_url) {
      $file_path = $this->transformRealpath($file_url);
      $manifest[$key] = [
        'url' => $file_url,
        'file' => $file_path,
      ];
      $used_files[$file_path] = TRUE;
    }

    $stale_excludes = [
      '@\/manifest\.json$@',
      '@\/entrypoints\.json$@',
      '@\/LICENSE\.txt$@',
      '@\/\.htaccess$@',
      '@\/\.drupal_compile@',
    ];
    foreach ($existing_files as $file) {
      $file_path = $file->uri;
      if (substr($file->filename, -12) === '.LICENSE.txt') {
        $file_path = substr($file_path, 0, strlen($file_path) - 12);
      }
      foreach ($stale_excludes as $exclude) {
        if (preg_match($exclude, str_replace(DIRECTORY_SEPARATOR, '/', $file_path))) {
          continue 2;
        }
      }
      if (!isset($used_files[$file_path])) {
        // Use the unchanged uri property here,
        // to include license files as stale files too.
        $stale_files[$file->uri] = $file->uri;
      }
    }

    return [
      'asset_files' => $asset_files,
      'stale_files' => $stale_files,
      'manifest' => $manifest,
    ];
  }

  /**
   * Transforms the given file URL to a realpath, if given.
   *
   * @param string $asset_file_url
   *   The asset file url to be resolved.
   *
   * @return string|false
   *   The realpath or unchanged absolute URL, FALSE if there was a failure.
   */
  protected function transformRealpath($asset_file_url) {
    if (strpos($asset_file_url, '//') !== FALSE) {
      if (strpos($asset_file_url, 'http:') === 0 || strpos($asset_file_url, 'https:') === 0) {
        $file_path = \Drupal::service('file_url_generator')->transformRelative($asset_file_url);
        if (strpos($asset_file_url, 'http:') === 0 || strpos($asset_file_url, 'https:') === 0) {
          // Still absolute, return it.
          return $file_path;
        }
      }
      else {
        return $this->fileSystem->realpath($asset_file_url);
      }
    }
    else {
      $file_path = str_replace('/', DIRECTORY_SEPARATOR, $asset_file_url);
    }

    if (strpos($file_path, DIRECTORY_SEPARATOR) === 0) {
      $file_path = substr($file_path, strlen(DIRECTORY_SEPARATOR));
    }

    if (!empty($this->appRoot) && !(strpos($file_path, $this->appRoot) === 0)) {
      $file_path = substr($this->appRoot, -strlen(DIRECTORY_SEPARATOR)) === DIRECTORY_SEPARATOR ? $this->appRoot . $file_path : $this->appRoot . DIRECTORY_SEPARATOR . $file_path;
    }

    return $file_path;
  }

}

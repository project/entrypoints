<?php

namespace Drupal\entrypoints;

/**
 * Trait for working with output directories.
 */
trait OutputDirectoryTrait {

  /**
   * The entrypoints compilation output directory, relative from app root path.
   *
   * @var string
   */
  protected $outputBasedir;

  /**
   * Set the entrypoints compilation output directory, relative from app root.
   *
   * @param string $basedir
   *   The relative basedir, e.g. sites/all/assets.
   */
  public function setOutputBasedir($basedir) {
    $this->outputBasedir = str_replace('/', DIRECTORY_SEPARATOR, $basedir);
  }

  /**
   * Get the entrypoints compilation output directory, relative from app root.
   */
  public function getOutputBasedir() {
    if (!isset($this->outputBasedir)) {
      $this->setOutputBasedir(\Drupal::getContainer()->getParameter('entrypoints.output_basedir'));
    }
    return $this->outputBasedir;
  }

  /**
   * Transform the given output folder to a relative path to the output folder.
   *
   * Usually needs to be transformed when a user sets a new output folder to be
   * used, or sets an absolute path for the new output folder.
   *
   * @param string $output_uri
   *   The output uri that needs to be transformed. Might be a single folder
   *   name or an absolute path.
   *
   * @return string
   *   The relative path to the output folder from the app root.
   */
  protected function outputFolderTransformRelative($output_uri) {
    $app_root = isset($this->appRoot) ? $this->appRoot : _entrypoints_app_root();
    $basedir = $this->getOutputBasedir();
    if (!empty($app_root) && strpos($output_uri, $app_root) === 0) {
      $output_uri = substr($output_uri, strlen($app_root));
    }
    if (strpos($output_uri, $basedir) === 0) {
      return $output_uri;
    }
    elseif (strpos($output_uri, DIRECTORY_SEPARATOR . $basedir) === 0) {
      return substr($output_uri, strlen(DIRECTORY_SEPARATOR));
    }
    if (strpos($output_uri, DIRECTORY_SEPARATOR) !== 0 && strpos($output_uri, '://') === FALSE && strpos($output_uri, $basedir) === FALSE) {
      // Given input is a single folder name.
      $output_uri = $basedir . DIRECTORY_SEPARATOR . $output_uri;
    }
    return $output_uri;
  }

}

<?php

namespace Drupal\entrypoints;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\entrypoints\Plugin\EntrypointsRendererPluginManager;

/**
 * The entrypoints server-side renderer (SSR) negotiator.
 */
class EntrypointsRendererNegotiator {

  /**
   * A list of allowed renderers.
   *
   * @var string[]
   */
  protected $renderersAllowed;

  /**
   * A cached list of available renderers.
   *
   * @var array
   */
  protected $renderersAvailable;

  /**
   * The default renderer.
   *
   * @var string|null
   */
  protected $defaultRenderer = NULL;

  /**
   * The manager for entrypoint server-side renderer plugins.
   *
   * @var \Drupal\entrypoints\Plugin\EntrypointsRendererPluginManager
   */
  protected $pluginManager;

  /**
   * The logger channel factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;

  /**
   * Internal in-memory cache.
   *
   * @var array
   */
  protected $cache;

  /**
   * The EntrypointsRendererNegotiator constructor.
   *
   * @param \Drupal\entrypoints\Plugin\EntrypointsRendererPluginManager $plugin_manager
   *   The manager for entrypoint server-side renderer plugins.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger channel factory.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache backend.
   * @param array $renderers
   *   A list of allowed renderers.
   */
  public function __construct(EntrypointsRendererPluginManager $plugin_manager, LoggerChannelFactoryInterface $logger_factory, ConfigFactoryInterface $config_factory, CacheBackendInterface $cache, array $renderers) {
    $this->reset();
    $this->pluginManager = $plugin_manager;
    $this->loggerFactory = $logger_factory;
    $this->cacheBackend = $cache;
    $this->renderersAllowed = array_combine($renderers, $renderers);
    if ($renderer = $config_factory->get('entrypoints.settings')->get('renderer')) {
      try {
        $this->setDefaultRenderer($renderer);
      }
      catch (EntrypointsRendererNotAllowedException $e) {
        $this->loggerFactory->get('entrypoints')->error($e->getMessage());
      }
    }
  }

  /**
   * Get the available server-side renderers.
   *
   * @return array
   *   The available renderers, keyed by renderer, values are an array that
   *   consist of label and version information.
   */
  public function getRenderersAvailable() {
    if (!isset($this->renderersAvailable)) {
      $this->renderersAvailable = [];
      foreach ($this->renderersAllowed as $renderer) {
        $plugin = $this->getPlugin($renderer);
        if ($version = $plugin->getVersion()) {
          $this->renderersAvailable[$renderer] = [
            'version' => $version,
            'label' => $plugin->getPluginDefinition()['label'],
          ];
        }
      }
    }
    return $this->renderersAvailable;
  }

  /**
   * Set the default server-side renderer.
   *
   * @param string $renderer
   *   The renderer to set.
   *
   * @throws \Drupal\entrypoints\EntrypointsRendererNotAllowedException
   *   If the given renderer is not allowed.
   */
  public function setDefaultRenderer($renderer) {
    if (!isset($this->renderersAllowed[$renderer])) {
      throw new EntrypointsRendererNotAllowedException($renderer);
    }
    $this->defaultRenderer = $renderer;
  }

  /**
   * Get the plugin instance of the given or configured renderer to use.
   *
   * @param string $renderer
   *   (Optional) The renderer to use. Skip this argument to use the currently
   *   configured or previously set default renderer.
   *
   * @return \Drupal\entrypoints\Plugin\EntrypointsRendererPluginInterface|null
   *   The plugin of the renderer, or NULL if not configured or not available.
   */
  public function get($renderer = NULL) {
    if ($renderer === NULL) {
      $renderer = $this->defaultRenderer;
    }
    if (empty($renderer) || !$this->isAvailable($renderer)) {
      return NULL;
    }
    return $this->getPlugin($renderer);
  }

  /**
   * Checks if the given server-side renderer is available.
   *
   * This method uses caching. If you want to make sure that you always
   * know the current availability, work with ::getRenderersAvailable() instead.
   *
   * @param string $renderer
   *   (Optional) The renderer to check for. Skip this argument to check
   *   for the currently configured or previously set default renderer.
   *
   * @return bool
   *   Returns TRUE if the renderer is available, FALSE otherwise.
   */
  public function isAvailable($renderer = NULL) {
    if ($renderer === NULL) {
      $renderer = $this->defaultRenderer;
    }
    if (empty($renderer)) {
      return FALSE;
    }
    $cid = 'entrypoints:renderers';
    if (isset($this->cache[$cid])) {
      $available = $this->cache[$cid];
    }
    elseif ($cached = $this->cacheBackend->get($cid)) {
      $available = $cached->data;
      $this->cache[$cid] = $available;
    }
    else {
      $available = $this->getRenderersAvailable();
      $this->cacheBackend->set($cid, $available);
      $this->cache[$cid] = $available;
    }
    return isset($available[$renderer]);
  }

  /**
   * Resets the internal in-memory cache.
   */
  public function reset() {
    $this->cache = [];
  }

  /**
   * Clears the whole server-side renderer cache.
   */
  public function clearCache() {
    foreach (array_keys($this->pluginManager->getDefinitions()) as $plugin_id) {
      $this->getPlugin($plugin_id)->clearCache();
    }
    $this->cacheBackend->delete('entrypoints:renderers');
    $this->reset();
  }

  /**
   * Get the server-side renderer plugin instance for the given plugin ID.
   *
   * @param string $plugin_id
   *   The plugin ID.
   *
   * @return \Drupal\entrypoints\Plugin\EntrypointsRendererPluginInterface
   *   The server-side renderer plugin instance.
   */
  protected function getPlugin($plugin_id) {
    if (!isset($this->cache['plugin'][$plugin_id])) {
      $this->cache['plugin'][$plugin_id] = $this->pluginManager->createInstance($plugin_id);
    }
    return $this->cache['plugin'][$plugin_id];
  }

}

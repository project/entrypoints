<?php

namespace Drupal\entrypoints;

use Drupal\Component\FileSecurity\FileSecurity;

/**
 * The entrypoints output protection service.
 */
class EntrypointsOutputProtection extends EntrypointsRuntimeTask {

  /**
   * Makes sure that SSR output directories are not publicly accessible.
   *
   * Server-side rendering logic might use sensible information, but would
   * be saved in a publicly accessible output directory. This method adds
   * a .htaccess file to the SSR output directories, so that the Apache
   * webserver does not allow public access to those files.
   *
   * @return bool
   *   Returns TRUE if protection steps were run successful, FALSE otherwise.
   */
  public function ensure() {
    $success = TRUE;
    $projects = $this->config()->get('projects');
    if (!empty($projects)) {
      foreach ($projects as $project) {
        if (empty($project['build'])) {
          continue;
        }
        foreach ($project['build'] as $build_info) {
          if (empty($build_info['location_ssr'])) {
            continue;
          }
          $directory = $this->localUriResolver->getRealpath($build_info['location_ssr']);
          if (empty($directory) || !@file_exists($directory)) {
            $this->log("The SSR output directory '%directory' does not exist or is not accessible. Please check your configuration or access permissions.", ['%directory' => $directory], 'error');
            $success = FALSE;
            continue;
          }
          if (!FileSecurity::writeHtaccess($directory)) {
            $this->log("Failed to write .htaccess protection file to the SSR output directory '%directory'.", ['%directory' => $directory], 'error');
            $success = FALSE;
          }
        }
      }
    }
    return $success;
  }

}

<?php

namespace Drupal\entrypoints;

use Drupal\Core\Logger\RfcLogLevel;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Generic trait for message log purposes.
 */
trait MessageLogTrait {

  /**
   * The logger channel factory, if given.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory = NULL;

  /**
   * The io instance, if given.
   *
   * @var \Symfony\Component\Console\Style\SymfonyStyle
   */
  protected $io = NULL;

  /**
   * The currently active level of messages being logged.
   *
   * @var int|null
   */
  protected $messageLogLevel = RfcLogLevel::INFO;

  /**
   * Get a logger channel.
   *
   * @param string $channel
   *   The channel to get. Default set to the 'entrypoints' channel.
   *
   * @return \Drupal\Core\Logger\LoggerChannelInterface
   *   The required logger channel.
   */
  protected function logger($channel = 'entrypoints') {
    if (!isset($this->loggerFactory)) {
      $this->loggerFactory = static::loggerFactory();
    }
    return $this->loggerFactory->get($channel);
  }

  /**
   * Logs the given message.
   *
   * @param string $message
   *   The message to log.
   * @param array $context
   *   (Optional) A context array containing further info.
   * @param string $level
   *   (Optional) The log level. Default set to 'info'.
   */
  protected function log($message, array $context = [], $level = 'info') {
    if (!isset($this->messageLogLevel)) {
      return;
    }

    $rfc_log_level = RfcLogLevel::INFO;
    switch ($level) {
      case 'debug':
        $rfc_log_level = RfcLogLevel::DEBUG;
        break;

      case 'info':
        $rfc_log_level = RfcLogLevel::INFO;
        break;

      case 'notice':
        $rfc_log_level = RfcLogLevel::NOTICE;
        break;

      case 'warning':
        $rfc_log_level = RfcLogLevel::WARNING;
        break;

      case 'error':
        $rfc_log_level = RfcLogLevel::ERROR;
        break;

      case 'critical':
        $rfc_log_level = RfcLogLevel::CRITICAL;
        break;

      case 'alert':
        $rfc_log_level = RfcLogLevel::ALERT;
        break;

      case 'emergency':
        $rfc_log_level = RfcLogLevel::EMERGENCY;
        break;
    }

    if ($this->messageLogLevel < $rfc_log_level) {
      return;
    }

    if (isset($this->io)) {
      switch ($level) {
        case 'debug':
          $this->io->text(strtr($message, $context));
          break;

        case 'info':
          $this->io->success(strtr($message, $context));
          break;

        case 'notice':
          $this->io->note(strtr($message, $context));
          break;

        case 'warning':
          $this->io->warning(strtr($message, $context));
          break;

        case 'error':
          $this->io->error(strtr($message, $context));
          break;

        default:
          $this->logger()->log($level, $message, $context);
      }
    }
    else {
      $this->logger()->log($level, $message, $context);
    }
  }

  /**
   * Set the io instance.
   *
   * @param \Symfony\Component\Console\Style\SymfonyStyle $io
   *   The io instance to set.
   */
  public function setIo(SymfonyStyle $io = NULL) {
    $this->io = $io;
  }

  /**
   * Set the message log level.
   *
   * @param int $level
   *   The level to set. Use NULL to disable logging at all.
   *
   * @see \Drupal\Core\Logger\RfcLogLevel
   */
  public function setMessageLogLevel($level) {
    $this->messageLogLevel = $level;
  }

  /**
   * Get currently active level of messages being logged.
   *
   * @return int|null
   *   Returns the level, or NULL if logging is disabled.
   */
  public function getMessageLogLevel() {
    return $this->messageLogLevel;
  }

  /**
   * Get the logger factory.
   *
   * @return \Drupal\Core\Logger\LoggerChannelFactoryInterface
   *   The logger factory.
   */
  protected static function loggerFactory() {
    return \Drupal::service('logger.factory');
  }

}

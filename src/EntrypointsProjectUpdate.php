<?php

namespace Drupal\entrypoints;

/**
 * The entrypoints project update service.
 *
 * This service is not available as a standalone command, because it only
 * makes sense to perform package updates, when the entrypoints are being
 * rebuild. That is why you can use the --update option on the
 * entrypoints:rebuild command, but it is not available as a standalone command.
 */
class EntrypointsProjectUpdate extends EntrypointsRuntimeTask {

  /**
   * Updates the packages within known project sources.
   *
   * By default, only project sources that have the 'watch_outdated' setting
   * set as enabled are included. See the parameters below if you want to
   * include all project sources.
   *
   * @param string $source_uri
   *   (Optional) Set to a specific source uri that exist in the configuration,
   *   so that the update will only run for this source and skips any other.
   *   Skip this argument to run the update on all known source projects.
   * @param bool $enforce_all
   *   Set to TRUE if you want to ignore existing 'watch_outdated' settings, and
   *   instead enforce to run updates on all projects, even if a project has the
   *   'watch_outdated' flag set as disabled.
   *
   * @return array|false
   *   Returns an array grouped by project source uri. Each entry per uri is
   *   either an array that is grouped by 'added', 'updated' and 'removed'
   *   or the boolean value FALSE on failure. Each array group lists packages
   *   with the package name as key and the package version as value. Returns
   *   FALSE instead of an array when something is wrong in general, e.g. when
   *   no default runtime is available or a source uri could not be resolved.
   */
  public function updatePackages($source_uri = NULL, $enforce_all = FALSE) {
    if (!$this->ensureDefaultRuntime()) {
      return FALSE;
    }

    $sources = [];
    if ($projects = $this->config()->get('projects')) {
      foreach ($projects as $project) {
        if (!$enforce_all && empty($project['source']['watch_outdated'])) {
          continue;
        }
        if (!empty($source_uri)) {
          if (empty($project['source']['location']) || ($project['source']['location'] !== $source_uri)) {
            continue;
          }
        }
        elseif (empty($project['source']['location'])) {
          continue;
        }
        try {
          $realpath = $this->localUriResolver->getRealpath($project['source']['location']);
        }
        catch (EntrypointsInvalidUriException $e) {
          $this->log("The entrypoints config contains a source project uri '%uri', but that does not resolve to a local folder. Aborted updates.", ['%uri' => $project['source']['location']], 'error');
          return FALSE;
        }
        if (!@file_exists($realpath)) {
          $this->log("The entrypoints config contains a source project that points to '%folder', but that directory does not exist. Aborted updates.", ['%folder' => $project['source']['location']], 'error');
          return FALSE;
        }
        $sources[$project['source']['location']] = $realpath;
      }
    }

    if (empty($sources)) {
      if (empty($source_uri)) {
        $this->log("No project sources are known from the entrypoints configuration that could be updated.", [], 'notice');
        return [];
      }
      else {
        $this->log("The given source project uri '%uri' is not present in the entrypoints configuration. Aborted updates.", ['%uri' => $source_uri], 'error');
        return FALSE;
      }
    }

    $results = [];
    foreach ($sources as $uri => $cwd) {
      if ($result = $this->runtime->get()->update($cwd)) {
        $this->log("Updated the project source at '%uri' (%updated updated, %added added, %removed removed)",
        [
          '%uri' => $uri,
          '%updated' => count($result['updated']),
          '%added' => count($result['added']),
          '%removed' => count($result['removed']),
        ]);
      }
      else {
        $this->log("Something went wrong when trying to update packages of project source at '%uri'. Make sure the project source exists, is accessible and contains valid project files (e.g. a valid package.json file).", ['%uri' => $uri], 'error');
      }
      $results[$uri] = $result;
    }
    return $results;
  }

}

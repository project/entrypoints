<?php

namespace Drupal\entrypoints;

/**
 * Entrypoints invalid uri exception class.
 */
class EntrypointsInvalidUriException extends \RuntimeException {

  /**
   * The invalid uri.
   *
   * @var string
   */
  protected $uri;

  /**
   * Constructs an entrypoints invalid uri exception.
   *
   * @param string $uri
   *   The uri that is not valid.
   * @param string $message
   *   (optional) The exception message.
   * @param int $code
   *   (optional) The error code.
   * @param \Exception $previous
   *   (optional) The previous exception.
   */
  public function __construct($uri, $message = '', $code = 0, \Exception $previous = NULL) {
    $this->uri = (string) $uri;
    $message = $message ?: "The uri '{$this->uri}' is not valid.";
    parent::__construct($message, $code, $previous);
  }

}

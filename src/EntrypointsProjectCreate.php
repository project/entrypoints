<?php

namespace Drupal\entrypoints;

use Drupal\Component\FileSecurity\FileSecurity;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;

/**
 * Service for creating new entrypoint source projects including output folders.
 */
class EntrypointsProjectCreate extends EntrypointsRuntimeTask {

  use OutputDirectoryTrait;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The stream wrapper manager.
   *
   * @var \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface
   */
  protected $streamWrapperManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The project register service.
   *
   * @var \Drupal\entrypoints\EntrypointsProjectRegister
   */
  protected $projectRegister;

  /**
   * Creates a new source project and adds it to the entrypoints configuration.
   *
   * @param string $source_uri
   *   The local path, either relative from the drupal application or absolute.
   * @param string $output_uri
   *   (Optional) The production asset build folder. Must be either a folder
   *   name to be created under 'entrypoints.output_basedir', or a uri that
   *   resolves locally to a sub-folder of 'entrypoints.output_basedir'.
   *   If not set, the folder name of the source project is being used.
   * @param string $output_dev_uri
   *   (Optional) The development asset build folder. If not set, the name for
   *   the output folder will be used, with '_dev' suffix appended.
   * @param string $entrypoint_name
   *   (Optional) The name of the entrypoint. By default 'index' will be used
   *   as entrypoint name.
   * @param string $template
   *   (Optional) The project template to use. Use 'default' for a plain and
   *   empty project, or 'react' when you want to create a React project. Can
   *   also be an absolute path that points to your own custom template folder.
   *   You can also use 'default-ssr' for a plain example or 'react-ssr' for a
   *   React example that additionally includes a server-side rendering part.
   * @param bool $watch_outdated
   *   (Optional) Whether the project should be watched by Drupal status report
   *   for having outdated packages. Enabled by default.
   * @param string $output_uri_ssr
   *   (Optional) The production build folder of assets to be rendered on the
   *   server-side (SSR). If not set, the name for the output folder will be
   *   used, with '_ssr' suffix appended. The SSR output folder will only be
   *   created when an SSR entrypoint file exists in the template folder,
   *   namely index.ssr.js.
   * @param string $output_dev_uri_ssr
   *   (Optional) The development build folder of assets to be rendered on the
   *   server-side (SSR). If not set, the name for the development output folder
   *   will be used, with '_ssr' suffix appended. The SSR output folder will
   *   only be created when an SSR entrypoint file exists in the template
   *   folder, namely index.ssr.js.
   *
   * @return bool
   *   Returns TRUE on success, FALSE otherwise.
   */
  public function createSourceProject($source_uri, $output_uri = NULL, $output_dev_uri = NULL, $entrypoint_name = 'index', $template = 'default', $watch_outdated = TRUE, $output_uri_ssr = NULL, $output_dev_uri_ssr = NULL) {
    if (!$this->ensureDefaultRuntime()) {
      return FALSE;
    }

    $template_directory = strpos($template, DIRECTORY_SEPARATOR) !== FALSE ? $template : $this->appRoot . DIRECTORY_SEPARATOR . $this->moduleHandler->getModule('entrypoints')->getPath() . DIRECTORY_SEPARATOR . 'project-template' . DIRECTORY_SEPARATOR . $template;
    if (substr($template_directory, -strlen(DIRECTORY_SEPARATOR)) === DIRECTORY_SEPARATOR) {
      $template_directory = substr($template_directory, 0, strlen($template_directory) - strlen(DIRECTORY_SEPARATOR));
    }
    if (empty($template_directory)) {
      $this->log("The chosen project template '%template' is not set or unknown. Aborted project creation.", ['%template' => $template], 'error');
      return FALSE;
    }
    if (!@is_dir($template_directory)) {
      $this->log("The template directory '%directory' does not exist or is not accessible. Aborted project creation.", ['%directory' => $template_directory], 'error');
      return FALSE;
    }
    foreach (['package.json', 'webpack.config.js', 'index.js'] as $required_file) {
      if (!file_exists($template_directory . DIRECTORY_SEPARATOR . $required_file)) {
        $this->log("The chosen project template '%template' does not contain required template files. At least one of package.json, webpack.config.js or index.js file is missing. Aborted project creation.", ['%template' => $template], 'error');
        return FALSE;
      }
    }
    $has_ssr = file_exists($template_directory . DIRECTORY_SEPARATOR . 'index.ssr.js');

    if (!empty($this->appRoot) && strpos($source_uri, $this->appRoot) === 0) {
      $source_uri = substr($this->appRoot, -strlen(DIRECTORY_SEPARATOR)) === DIRECTORY_SEPARATOR ? substr($source_uri, strlen($this->appRoot)) : substr($source_uri, strlen($this->appRoot) + strlen(DIRECTORY_SEPARATOR));
    }

    $source_directory = $this->resolveDirectory($source_uri);
    $source_folder = $this->fileSystem->basename($source_directory);

    if (empty($output_uri)) {
      $output_uri = $source_folder;
    }
    if (empty($output_dev_uri)) {
      $output_dev_uri = $output_uri . '_dev';
    }
    $output_uri = $this->normalizeOutputUri($output_uri);
    $output_dev_uri = $this->normalizeOutputUri($output_dev_uri);
    if (strpos($output_uri, $this->outputBasedir) === FALSE) {
      $this->log("The given output directory is not a child directory of '%basedir'. Aborted project creation.", ['%basedir' => $this->outputBasedir], 'error');
      return FALSE;
    }
    if (strpos($output_dev_uri, $this->outputBasedir) === FALSE) {
      $this->log("The given development output directory is not a child directory of '%basedir'. Aborted project creation.", ['%basedir' => $this->outputBasedir], 'error');
      return FALSE;
    }
    $output_uri_ssr = NULL;
    $output_dev_uri_ssr = NULL;
    if ($has_ssr) {
      if (empty($output_uri_ssr)) {
        $output_uri_ssr = $output_uri . '_ssr';
      }
      if (empty($output_dev_uri_ssr)) {
        $output_dev_uri_ssr = $output_dev_uri . '_ssr';
      }
      $output_uri_ssr = $this->normalizeOutputUri($output_uri_ssr);
      $output_dev_uri_ssr = $this->normalizeOutputUri($output_dev_uri_ssr);
      if (strpos($output_uri_ssr, $this->outputBasedir) === FALSE) {
        $this->log("The given SSR output directory is not a child directory of '%basedir'. Aborted project creation.", ['%basedir' => $this->outputBasedir], 'error');
        return FALSE;
      }
      if (strpos($output_dev_uri_ssr, $this->outputBasedir) === FALSE) {
        $this->log("The given development SSR output directory is not a child directory of '%basedir'. Aborted project creation.", ['%basedir' => $this->outputBasedir], 'error');
        return FALSE;
      }
      if ($output_uri === $output_uri_ssr || $output_dev_uri === $output_dev_uri_ssr || $output_uri === $output_dev_uri_ssr || $output_dev_uri === $output_uri_ssr) {
        $this->log("Equal output folders for client-side and server-side rendering entrypoints are not allowed. Aborted project creation.", [], 'error');
        return FALSE;
      }
    }

    // Check within configuration, whether the source or output already exists.
    $config = $this->config();
    $existing_projects = $config->get('projects');
    if (!empty($existing_projects)) {
      foreach ($existing_projects as $existing_project) {
        if (!empty($existing_project['source']['location']) && ($existing_project['source']['location'] == $source_uri)) {
          $this->log("The given source uri %uri already exists in the entrypoints configuration. Aborted project creation.", ['%uri' => $source_uri], 'error');
          return FALSE;
        }
        if (!empty($existing_project['build'])) {
          foreach ($existing_project['build'] as $build_mode => $build_settings) {
            if (!empty($build_settings['location'])) {
              if ($build_settings['location'] === $output_uri) {
                $this->log("The given entrypoints output '%uri' already exists in the entrypoints configuration. Aborted project creation.", ['%uri' => $output_uri], 'error');
                return FALSE;
              }
              if ($build_settings['location'] === $output_dev_uri) {
                $this->log("The given entrypoints development output '%uri' already exists in the entrypoints configuration. Aborted project creation.", ['%uri' => $output_dev_uri], 'error');
                return FALSE;
              }
            }
          }
        }
      }
    }

    $output_directory = $this->resolveDirectory($output_uri);
    $output_dev_directory = $this->resolveDirectory($output_dev_uri);

    if (!$this->fileSystem->prepareDirectory($source_directory, FileSystemInterface::CREATE_DIRECTORY)) {
      $this->log("Could not prepare source directory '%directory'. Aborted project creation.", ['%directory' => $source_directory], 'error');
      return FALSE;
    }
    if (!$this->fileSystem->prepareDirectory($output_directory, FileSystemInterface::CREATE_DIRECTORY)) {
      $this->log("Could not prepare output directory '%directory'. Aborted project creation.", ['%directory' => $output_directory], 'error');
      return FALSE;
    }
    if (!$this->fileSystem->prepareDirectory($output_dev_directory, FileSystemInterface::CREATE_DIRECTORY)) {
      $this->log("Could not prepare development output directory '%directory'. Aborted project creation.", ['%directory' => $output_dev_directory], 'error');
      return FALSE;
    }

    if ($has_ssr) {
      $output_directory_ssr = $this->resolveDirectory($output_uri_ssr);
      $output_dev_directory_ssr = $this->resolveDirectory($output_dev_uri_ssr);
      if (!$this->fileSystem->prepareDirectory($output_directory_ssr, FileSystemInterface::CREATE_DIRECTORY)) {
        $this->log("Could not prepare SSR output directory '%directory'. Aborted project creation.", ['%directory' => $output_directory_ssr], 'error');
        return FALSE;
      }
      if (!FileSecurity::writeHtaccess($output_directory_ssr, TRUE, TRUE)) {
        $this->log("Could not write .htaccess file into the SSR output directory '%directory'. Aborted project creation.", ['%directory' => $output_directory_ssr], 'error');
        return FALSE;
      }
      if (!$this->fileSystem->prepareDirectory($output_dev_directory_ssr, FileSystemInterface::CREATE_DIRECTORY)) {
        $this->log("Could not prepare development SSR output directory '%directory', aborted project creation.", ['%directory' => $output_dev_directory_ssr], 'error');
        return FALSE;
      }
      if (!FileSecurity::writeHtaccess($output_dev_directory_ssr, TRUE, TRUE)) {
        $this->log("Could not write .htaccess file into the development SSR output directory '%directory'. Aborted project creation.", ['%directory' => $output_dev_directory_ssr], 'error');
        return FALSE;
      }
    }

    // If resolvable, use a relative path to the drupal root.
    if (strpos($source_directory, $this->appRoot) === 0) {
      $drupal_root = '';
      $root_parts = explode(DIRECTORY_SEPARATOR, $this->appRoot);
      $relative_path = str_replace($this->appRoot . DIRECTORY_SEPARATOR, '', $source_directory);
      $gone_too_far = FALSE;
      while (strpos($relative_path, '..' . DIRECTORY_SEPARATOR) === 0) {
        $root_part = array_pop($root_parts);
        if (empty($root_part) || (strpos($root_part, ':') !== FALSE && DIRECTORY_SEPARATOR !== '/')) {
          $gone_too_far = TRUE;
          break;
        }
        $drupal_root = DIRECTORY_SEPARATOR . $root_part . $drupal_root;
        $relative_path = substr($relative_path, strlen('..' . DIRECTORY_SEPARATOR));
      }
      $drupal_root = $gone_too_far ? $this->appRoot : preg_replace('/[^\\' . DIRECTORY_SEPARATOR . ']+/', '..', $relative_path) . $drupal_root;
    }
    else {
      $drupal_root = $this->appRoot;
    }

    // Write the initial files into the source directory by using the template.
    $basedir_absolute = substr($this->appRoot, -strlen(DIRECTORY_SEPARATOR)) === DIRECTORY_SEPARATOR ? $this->appRoot . $this->outputBasedir . DIRECTORY_SEPARATOR : $this->appRoot . DIRECTORY_SEPARATOR . $this->outputBasedir . DIRECTORY_SEPARATOR;
    $config_params = [
      ':entrypoint' => $entrypoint_name,
      ':drupal_root' => str_replace(DIRECTORY_SEPARATOR, '/', $drupal_root),
      ':output_basedir' => str_replace(DIRECTORY_SEPARATOR, '/', $this->outputBasedir),
      ':output_folder' => str_replace(DIRECTORY_SEPARATOR, '/', str_replace($basedir_absolute, '', $output_directory)),
      ':output_dev_folder' => str_replace(DIRECTORY_SEPARATOR, '/', str_replace($basedir_absolute, '', $output_dev_directory)),
    ];
    $config_content = strtr(file_get_contents($template_directory . DIRECTORY_SEPARATOR . 'webpack.config.js'), $config_params);
    file_put_contents($source_directory . DIRECTORY_SEPARATOR . 'webpack.config.js', $config_content);
    $src_folder = $source_directory . DIRECTORY_SEPARATOR . 'src';
    if (!$this->fileSystem->prepareDirectory($src_folder, FileSystemInterface::CREATE_DIRECTORY)) {
      $this->log("Could not create '%folder' folder. Aborted project creation. Initially created source and output folders need to be removed manually.", ['%folder' => $src_folder], 'error');
      return FALSE;
    }
    if (empty($this->fileSystem->copy($template_directory . DIRECTORY_SEPARATOR . 'index.js', $src_folder . DIRECTORY_SEPARATOR . $entrypoint_name . '.js'))) {
      $this->log("Failed to copy entrypoint file into the source folder, please check permissions. Aborted project creation. Initially created source and output folders need to be removed manually.", [], 'error');
      return FALSE;
    }
    if ($has_ssr) {
      if (empty($this->fileSystem->copy($template_directory . DIRECTORY_SEPARATOR . 'index.ssr.js', $src_folder . DIRECTORY_SEPARATOR . $entrypoint_name . '.ssr.js'))) {
        $this->log("Failed to copy SSR entrypoint file into the source folder, please check permissions. Aborted project creation. Initially created source and output folders need to be removed manually.", [], 'error');
        return FALSE;
      }
    }

    $package_json = json_decode(file_get_contents($template_directory . DIRECTORY_SEPARATOR . 'package.json'), TRUE);
    if (empty($package_json['name']) || $package_json['name'] === ':source') {
      $package_json['name'] = $source_folder;
    }
    $source_name = $package_json['name'];
    $packages = [];
    if (!empty($package_json['dependencies'])) {
      foreach ($package_json['dependencies'] as $package => $version) {
        if ($version === ':latest') {
          $packages[] = $package;
          // Package will be manually installed, so remove it from here.
          unset($package_json['dependencies'][$package]);
        }
      }
    }
    $dev_packages = [];
    if (!empty($package_json['devDependencies'])) {
      foreach ($package_json['devDependencies'] as $package => $version) {
        if ($version === ':latest') {
          $dev_packages[] = $package;
          unset($package_json['devDependencies'][$package]);
        }
      }
    }
    file_put_contents($source_directory . DIRECTORY_SEPARATOR . 'package.json', json_encode($package_json, JSON_PRETTY_PRINT | JSON_FORCE_OBJECT | JSON_UNESCAPED_SLASHES));

    $this->log("Project source folder created at '%folder'. Source name determined as '%name'.",
    [
      '%folder' => $source_directory,
      '%name' => $source_name,
    ]);

    // Run installation tasks.
    $this->log("Installing packages for source '%name', this may take a while.", ['%name' => $source_name]);
    if (FALSE === $this->runtime->get()->install($source_directory, $packages)) {
      $this->log("Failed to execute installation of packages. Aborted project creation. Initially created source and output folders need to be removed manually.", [], 'error');
      return FALSE;
    }
    if (FALSE === $this->runtime->get()->install($source_directory, $dev_packages, 'dev')) {
      $this->log("Failed to execute installation of development packages. Aborted project creation. Initially created source and output folders need to be removed manually.", [], 'error');
      return FALSE;
    }
    if (!empty($dev_packages)) {
      // Lastly make sure that all packages are installed.
      $this->runtime->get()->install($source_directory, [], 'dev');
    }
    $this->log("Installation of source packages of '%name' completed.", ['%name' => $source_name]);

    // Compile the entrypoints for the first time in production mode.
    $build_modes = $this->runtime->getBuildModesAvailable();
    $build_mode = isset($build_modes['production']) ? 'production' : key($build_modes);
    if (!empty($build_mode)) {
      $this->log("Compiling assets from '%name' in %mode mode for the first time.",
      [
        '%name' => $source_name,
        '%mode' => $build_modes[$build_mode],
      ]);
      if (!($output = $this->runtime->get()->script($source_directory, $build_mode))) {
        $this->log("Failed to run the %mode compile script. Aborted project creation. Initially created source and output folders need to be removed manually.", ['%mode' => $build_modes[$build_mode]], 'error');
        return FALSE;
      }
      $this->log($output);
      $this->log("Asset compilation from '%name' completed.",
        ['%name' => $source_name]);
    }
    else {
      $this->log("Skipped asset compilation of entrypoints for source project '%name', because no build mode is available on this environment.", ['%name' => $source_name], 'notice');
    }

    $this->projectRegister->register($source_uri, $output_uri, $output_dev_uri, 'production', 'development', $watch_outdated, $output_uri_ssr, $output_dev_uri_ssr);

    $this->log("Project creation of '%name' completed.", ['%name' => $source_name]);
    $this->log("Consider adding the development output directory '%folder' to your main project's .gitignore file.", ['%folder' => $output_dev_directory], 'notice');
    return TRUE;
  }

  /**
   * Resolves the directory represented by the given uri to an absolute path.
   *
   * @param string $uri
   *   The uri.
   *
   * @return string
   *   The resolved absolute local path to the directory.
   */
  protected function resolveDirectory($uri) {
    $directory = $this->localUriResolver->getRealpath($uri);
    if (@file_exists($directory) && !empty($this->fileSystem->scanDirectory($directory, '/.*/', ['recurse' => FALSE]))) {
      throw new EntrypointsInvalidUriException($uri, "The directory '${uri}' is not empty, aborted project creation.");
    }
    return $directory;
  }

  /**
   * Normalize the given output uri to a relative path from the app root.
   *
   * @param string $output_uri
   *   The output uri that needs to be normalized. Might be a single folder
   *   name or an absolute path.
   *
   * @return string
   *   The relative path to the output folder from the app root.
   */
  protected function normalizeOutputUri($output_uri) {
    $normalized = $this->outputFolderTransformRelative($output_uri);
    if ($output_uri != $normalized) {
      $this->log("The output folder '%folder' will be located in '%directory'.",
      [
        '%folder' => $output_uri,
        '%directory' => $this->appRoot . DIRECTORY_SEPARATOR . $normalized,
      ]);
    }
    return $normalized;
  }

  /**
   * Set the stream wrapper manager.
   *
   * @param \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface $stream_wrapper_manager
   *   The stream wrapper manager.
   */
  public function setStreamWrapperManager(StreamWrapperManagerInterface $stream_wrapper_manager) {
    $this->streamWrapperManager = $stream_wrapper_manager;
  }

  /**
   * Set the file system service.
   *
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   */
  public function setFileSystem(FileSystemInterface $file_system) {
    $this->fileSystem = $file_system;
  }

  /**
   * Set the module handler.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function setModuleHandler(ModuleHandlerInterface $module_handler) {
    $this->moduleHandler = $module_handler;
  }

  /**
   * Set the project register service.
   *
   * @param \Drupal\entrypoints\EntrypointsProjectRegister $project_register
   *   The project register service.
   */
  public function setProjectRegister(EntrypointsProjectRegister $project_register) {
    $this->projectRegister = $project_register;
  }

}

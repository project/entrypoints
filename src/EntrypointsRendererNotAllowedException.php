<?php

namespace Drupal\entrypoints;

/**
 * Entrypoints renderer not allowed exception class.
 */
class EntrypointsRendererNotAllowedException extends \RuntimeException {

  /**
   * The entrypoints renderer that is not allowed.
   *
   * @var string
   */
  protected $entrypointsRenderer;

  /**
   * Constructs an entrypoints renderer not allowed exception.
   *
   * @param string $renderer
   *   The renderer that is not allowed.
   * @param string $message
   *   (optional) The exception message.
   * @param int $code
   *   (optional) The error code.
   * @param \Exception $previous
   *   (optional) The previous exception.
   */
  public function __construct($renderer, $message = '', $code = 0, \Exception $previous = NULL) {
    $this->entrypointsRuntime = (string) $renderer;
    $message = $message ?: "The entrypoints renderer '{$this->entrypointsRenderer}' is not allowed to be run on this environment.";
    parent::__construct($message, $code, $previous);
  }

}

<?php

namespace Drupal\entrypoints;

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Base class for entrypoints tasks that use the runtime.
 */
abstract class EntrypointsRuntimeTask {

  use MessageLogTrait;

  /**
   * The app root path.
   *
   * @var string
   */
  protected $appRoot;

  /**
   * The runtime negotiator service.
   *
   * @var \Drupal\entrypoints\EntrypointsRuntimeNegotiator
   */
  protected $runtime;

  /**
   * The local uri resolver.
   *
   * @var \Drupal\entrypoints\LocalUriResolver
   */
  protected $localUriResolver;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The EntrypointsRuntimeTask constructor.
   *
   * @param \Drupal\entrypoints\EntrypointsRuntimeNegotiator $runtime
   *   The runtime negotiator service.
   * @param \Drupal\entrypoints\LocalUriResolver $local_uri_resolver
   *   The local uri resolver.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   */
  public function __construct(EntrypointsRuntimeNegotiator $runtime, LocalUriResolver $local_uri_resolver, ConfigFactoryInterface $config_factory) {
    $this->appRoot = _entrypoints_app_root();
    $this->runtime = $runtime;
    $this->localUriResolver = $local_uri_resolver;
    $this->configFactory = $config_factory;
  }

  /**
   * Get the configuration object for the given id.
   *
   * @param string $id
   *   The config ID. By default, the entrypoints settings ID will be used.
   *
   * @return \Drupal\Core\Config\Config
   *   The configuration object.
   */
  protected function config($id = 'entrypoints.settings') {
    if ($id === 'entrypoints.settings') {
      return $this->configFactory->getEditable($id);
    }
    return $this->configFactory->get($id);
  }

  /**
   * Checks if the default runtime is available and logs an error if it's not.
   *
   * @return bool
   *   Returns TRUE if it is available, FALSE otherwise.
   */
  protected function ensureDefaultRuntime() {
    if (!$this->runtime->isAvailable()) {
      $this->log("No runtime is available or not yet configured for this environment. Please assign the default runtime to use within entrypoints.settings at /admin/config/entrypoints. You can also set the runtime via Drush, e.g. 'drush cset entrypoints.settings runtime npm' to set NPM as runtime (or set 'yarn' if you have and want to use YARN instead).", [], 'error');
      return FALSE;
    }
    return TRUE;
  }

}

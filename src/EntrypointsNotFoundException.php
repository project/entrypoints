<?php

namespace Drupal\entrypoints;

/**
 * Entrypoints not found exception class.
 */
class EntrypointsNotFoundException extends \RuntimeException {

  /**
   * The uri where no entrypoints were found.
   *
   * @var string
   */
  protected $uri;

  /**
   * Constructs an entrypoints not found exception.
   *
   * @param string $uri
   *   The uri where no entrypoints were found.
   * @param string $message
   *   (optional) The exception message.
   * @param int $code
   *   (optional) The error code.
   * @param \Exception $previous
   *   (optional) The previous exception.
   */
  public function __construct($uri, $message = '', $code = 0, \Exception $previous = NULL) {
    $this->uri = (string) $uri;
    $message = $message ?: "No entrypoints were found at uri '{$this->uri}'.";
    parent::__construct($message, $code, $previous);
  }

}

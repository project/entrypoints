<?php

namespace Drupal\entrypoints;

/**
 * Entrypoints no default runtime available exception class.
 */
class EntrypointsNoDefaultRuntimeAvailableException extends \RuntimeException {

  /**
   * Constructs an entrypoints no default runtime available exception.
   *
   * @param string $message
   *   (Optional) The exception message.
   * @param int $code
   *   (Ooptional) The error code.
   * @param \Exception $previous
   *   (Optional) The previous exception.
   */
  public function __construct($message = '', $code = 0, \Exception $previous = NULL) {
    $message = $message ?: "No default entrypoints runtime is available or configured on this environment. Check your entrypoints.settings configuration at /admin/config/entrypoints and assign an available runtime.";
    parent::__construct($message, $code, $previous);
  }

}

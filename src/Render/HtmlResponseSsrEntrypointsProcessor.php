<?php

namespace Drupal\entrypoints\Render;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Render\HtmlResponse;

/**
 * Processes server-side rendering for HTML responses with entrypoint libraries.
 */
class HtmlResponseSsrEntrypointsProcessor {

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The HtmlResponseSsrEntrypointsProcessor constructor method.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * Processes the server-side rendering for attached entrypoint libraries.
   *
   * @param \Drupal\Core\Render\HtmlResponse $response
   *   The response that might contain entrypoint library attachments.
   *
   * @return \Drupal\Core\Render\HtmlResponse
   *   The updated response that contains the result of server-side renderings.
   */
  public function processSsrEntrypoints(HtmlResponse $response) {
    if (!$this->configFactory->get('entrypoints.settings')->get('renderer')) {
      return $response;
    }
    $attached = $response->getAttachments();
    if (empty($attached['library'])) {
      return $response;
    }
    $names = [];
    foreach ($attached['library'] as $library) {
      if (strpos($library, 'entrypoints/') === 0) {
        $name = substr($library, 12);
        $names[$name] = $name;
      }
    }
    if (empty($names)) {
      return $response;
    }
    $definitions = static::loadEntrypointDefinitions($names);
    $ssr_definitions = [];
    foreach ($names as $name) {
      if (!isset($definitions[$name])) {
        // When a specific entrypoint could not be loaded, the registry would
        // log a corresponding error message, thus no need to log here too.
        continue;
      }
      /** @var \Drupal\entrypoints\EntrypointDefinition $definition */
      $definition = $definitions[$name];
      if ($ssr_definition = $definition->getSsrDefinition()) {
        $ssr_definitions[$ssr_definition->getName()] = $ssr_definition;
      }
    }
    if (empty($ssr_definitions)) {
      return $response;
    }
    unset($attached, $names, $definitions);
    return static::renderSsrEntrypoints($ssr_definitions, $response);
  }

  /**
   * Loads entrypoint definitions.
   *
   * The static method is being used to reduce overhead by only loading class
   * namespaces and registry services when they are needed.
   *
   * @param array $names
   *   A list of entrypoint names to load.
   *
   * @return \Drupal\entrypoints\EntrypointDefinition[]
   *   The entrypoint definitions.
   */
  protected static function loadEntrypointDefinitions(array $names) {
    return \Drupal::service('entrypoints.registry')->getDefinitions($names);
  }

  /**
   * Performs server-side rendering for the given entrypoints.
   *
   * @param \Drupal\entrypoints\EntrypointDefinition[] $definitions
   *   The entrypoint definitions to be rendered on the server-side.
   * @param \Drupal\Core\Render\HtmlResponse $response
   *   The response that might contain entrypoint library attachments.
   *
   * @return \Drupal\Core\Render\HtmlResponse
   *   The updated response that contains the result of server-side renderings.
   */
  protected static function renderSsrEntrypoints(array $definitions, HtmlResponse $response) {
    /** @var \Drupal\entrypoints\EntrypointsRendererNegotiator $negotiator */
    $negotiator = \Drupal::service('entrypoints.renderer');
    if (!($renderer = $negotiator->get())) {
      if (!empty($definitions)) {
        \Drupal::logger('entrypoints')->error("Entrypoints cannot be rendered on the server-side, because no default renderer is available. Make sure that the entrypoints configuration has an available renderer assigned.");
      }
      return $response;
    }
    /** @var \Drupal\entrypoints\Plugin\EntrypointsInputHandlerPluginManager $input_handler_manager */
    $input_handler_manager = \Drupal::service('plugin.manager.entrypoints.input_handler');
    /** @var \Drupal\entrypoints\Plugin\EntrypointsInputHandlerPluginInterface[] $input_handlers */
    $input_handlers = [];
    foreach (array_keys($input_handler_manager->getDefinitions()) as $plugin_id) {
      $input_handlers[$plugin_id] = $input_handler_manager->createInstance($plugin_id);
    }
    $input = [];
    foreach ($input_handlers as $input_handler) {
      $input_handler->prepareRender($input, $definitions, $renderer, $response);
    }
    foreach ($definitions as $definition) {
      foreach ($input_handlers as $input_handler) {
        $input_handler->beforeRender($input, $definition, $renderer, $response);
      }
      $output = $renderer->render($definition, $input);
      foreach ($input_handlers as $input_handler) {
        $input_handler->afterRender($input, $output, $definition, $renderer, $response);
      }
      if (FALSE !== $output) {
        $input = $output;
      }
    }
    foreach ($input_handlers as $input_handler) {
      $input_handler->finishRender($output, $definitions, $renderer, $response);
    }
    return $response;
  }

}

<?php

namespace Drupal\entrypoints;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\State\StateInterface;

/**
 * The entrypoints registry.
 */
class EntrypointsRegistry {

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The cache backend for entrypoints.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;

  /**
   * The cache backend for manifest entries.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackendManifest;

  /**
   * The state storage.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The logger channel factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * Internal in-memory cache.
   *
   * @var array
   */
  protected $cache;

  /**
   * Whether a ::deriveAll() was already executed before.
   *
   * This flag is used to prevent repetitions of expensive directory scans.
   *
   * @var bool
   */
  protected $derived;

  /**
   * The EntrypointsRegistry constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache backend for entrypoints.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_manifest
   *   The cache backend for manifest entries.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state storage.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger channel factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory, CacheBackendInterface $cache, CacheBackendInterface $cache_manifest, StateInterface $state, LoggerChannelFactoryInterface $logger_factory) {
    $this->reset();
    $this->configFactory = $config_factory;
    $this->cacheBackend = $cache;
    $this->cacheBackendManifest = $cache_manifest;
    $this->state = $state;
    $this->loggerFactory = $logger_factory;
  }

  /**
   * Get a list of all known entrypoint names.
   *
   * @return string[]
   *   The list of all known entrypoint names.
   */
  public function getNames() {
    if (!isset($this->cache['names'])) {
      $cid = 'entrypoints:names';
      if ($cached = $this->cacheBackend->get($cid)) {
        $this->cache['names'] = $cached->data;
      }
      else {
        $this->deriveAll();
        $this->cacheBackend->set($cid, $this->cache['names']);
      }
    }
    return $this->cache['names'];
  }

  /**
   * Get a list of Entrypoint definitions.
   *
   * @param string[] $entrypoints
   *   An array of entrypoint names. Wildcards are allowed by using the
   *   star (*) symbol. Example: 'navbar.*' would load all definitions whose
   *   name begin with 'navbar.'. You can use an empty array or the (*) alone
   *   to load all existing entrypoint instances.
   * @param bool $log_failures
   *   (Optional) Whether failures should be logged as errors, when entrypoint
   *   names exist but whose definition could not be loaded. Please note that
   *   this method does not throw an exception when certain entrypoint names
   *   could not be loaded. Default is set to TRUE.
   *
   * @return \Drupal\entrypoints\EntrypointDefinition[]
   *   The entrypoint instances that match the given criteria, keyed by
   *   entrypoint name.
   */
  public function getDefinitions(array $entrypoints = ['*'], $log_failures = TRUE) {
    if (empty($entrypoints)) {
      $entrypoints = ['*'];
    }

    $to_load = [];
    foreach ($entrypoints as $entrypoint) {
      if (strpos($entrypoint, '*') === FALSE) {
        $to_load[$entrypoint] = $entrypoint;
      }
      else {
        $pattern = '/^' . str_replace('\*', '.*', preg_quote($entrypoint, '/')) . '$/';
        foreach ($this->getNames() as $name) {
          if ($entrypoint === $name) {
            $to_load[$entrypoint] = $entrypoint;
            continue 2;
          }
          elseif ($entrypoint === '*') {
            $to_load = $this->getNames();
            break 2;
          }
          elseif (preg_match($pattern, $name)) {
            $to_load[$name] = $name;
          }
        }
      }
    }

    $cids = [];
    foreach ($to_load as $name) {
      if (!isset($this->cache['definitions'][$name])) {
        $cids[] = 'entrypoints:definition:' . $name;
      }
    }
    if (!empty($cids)) {
      $cache_items = $this->cacheBackend->getMultiple($cids);
      foreach ($to_load as $name) {
        $cid = 'entrypoints:definition:' . $name;
        if (isset($cache_items[$cid])) {
          $this->cache['definitions'][$name] = $cache_items[$cid]->data;
        }
      }
    }

    $definitions = [];
    $cache_items = [];
    foreach ($to_load as $name) {
      if (!isset($this->cache['definitions'][$name])) {
        $this->deriveAll();
        if (!isset($this->cache['definitions'][$name])) {
          $this->cache['definitions'][$name] = FALSE;
        }
        $cid = 'entrypoints:definition:' . $name;
        $cache_items[$cid] = ['data' => $this->cache['definitions'][$name]];
      }
      if (!empty($this->cache['definitions'][$name])) {
        $definitions[$name] = $this->cache['definitions'][$name];
      }
      elseif ($log_failures) {
        $this->loggerFactory->get('entrypoints')->error("The requested entrypoint @name was not found.", ['@name' => $name]);
      }
    }

    if (!empty($cache_items)) {
      $this->cacheBackend->setMultiple($cache_items);
    }

    return $definitions;
  }

  /**
   * Get a list of Entrypoint definitions that match the given settings.
   *
   * @param array $settings
   *   The subset of settings whose values should match. For example,
   *   ['library' => ['header' => TRUE], 'attach' => ['everywhere' => TRUE]] is
   *   a settings subset that would match to all existing entrypoints that are
   *   supposed to be included in the header, and to be included on every page.
   *   See the 'entrypoints.schema.yml' for available settings within the
   *   'entrypoints_settings' type definition.
   *
   * @return \Drupal\entrypoints\EntrypointDefinition[]
   *   The entrypoint instances that match the given criteria.
   */
  public function getDefinitionsBySettings(array $settings) {
    $config = $this->configFactory->get('entrypoints.settings');

    $availables = $config->get('overrides') ?: [];
    $availables[] = $config->get('defaults') + ['entrypoint' => '*'];

    $names = $this->getNames();

    $includes = [];
    foreach ($availables as $i => $available) {
      if (strpos($available['entrypoint'], '*') !== FALSE) {
        // Direct matches apply first, wildcards will be handled afterwards.
        continue;
      }
      $name = $available['entrypoint'];
      if (isset($names[$name])) {
        if ($available == NestedArray::mergeDeepArray([$available, $settings], TRUE)) {
          $includes[$name] = $name;
        }
        // Unset to not change the decision for the given entrypoint.
        unset($names[$name]);
      }
      // Unset to not loop over it again.
      unset($availables[$i]);
    }
    // Loop on the rest, which contain wildcards.
    if (!empty($names)) {
      foreach ($availables as $available) {
        $matched = ($available == NestedArray::mergeDeepArray(
          [$available, $settings], TRUE));
        $pattern = '/^' . str_replace('\*', '.*', preg_quote($available['entrypoint'], '/')) . '$/';
        foreach ($names as $name) {
          if ($available['entrypoint'] === $name || $available['entrypoint'] === '*' || preg_match($pattern, $name)) {
            if ($matched) {
              $includes[$name] = $name;
            }
            unset($names[$name]);
          }
        }
      }
    }

    return $this->getDefinitions($includes);
  }

  /**
   * Get a list of manifest.json entries by the given entry keys.
   *
   * @param string[] $keys
   *   The entry keys. Leave empty to get all existing entries.
   *   In many occasions, the manifest keys include the base directory of the
   *   output folders (which is the value of the 'entrypoints.output_basedir'
   *   service parameter). For those entries, the base directory part does not
   *   need to be included. For example, when 'entrypoints.output_basedir' is
   *   set to be 'sites/all/assets', and an entry exists in the manifest.json
   *   with a key 'sites/all/assets/entry/index.js', then it can be obtained by
   *   setting the $keys argument as ['entry/index.js'].
   *
   * @return array
   *   A list of manifest entries, keyed by the original manifest keys (which
   *   occasionally includes the 'entrypoints.output_basedir' parameter as a
   *   prefix). Each entry is an array that is keyed by 'url' and 'file' string
   *   values. The 'url' might be absolute or relative. The 'file' value might
   *   be an absolute local path or an external url. If a given key does not
   *   exist, no entry value exists for it in the returned array.
   */
  public function getManifestEntries(array $keys = []) {
    if (empty($keys)) {
      if (!isset($this->cache['manifest'])) {
        $cid = 'entrypoints:manifest';
        if ($cached = $this->cacheBackendManifest->get($cid)) {
          $this->cache['manifest'] = $cached->data;
        }
        else {
          $this->deriveAll();
          $this->cacheBackendManifest->set($cid, $this->cache['manifest']);
        }
      }
      return $this->cache['manifest'];
    }

    $entries = [];
    $cids = [];

    if ($output_basedir = static::getOutputBasedirRawValue()) {
      $keys_prepended = [];
      if (DIRECTORY_SEPARATOR !== '/') {
        $separator = '/';
        $output_basedir = str_replace(DIRECTORY_SEPARATOR, $separator, $output_basedir);
      }
      foreach ($keys as $key) {
        if (strpos($key, $separator) !== FALSE && strpos($key, $output_basedir) === FALSE) {
          $keys_prepended[$key] = $output_basedir . $separator . $key;
        }
        elseif (DIRECTORY_SEPARATOR !== $separator && strpos($key, DIRECTORY_SEPARATOR) !== FALSE && strpos($key, $output_basedir) === FALSE) {
          $output_basedir = str_replace($separator, DIRECTORY_SEPARATOR, $output_basedir);
          $separator = DIRECTORY_SEPARATOR;
          if (strpos($key, $output_basedir) === FALSE) {
            $keys_prepended[$key] = $output_basedir . $separator . $key;
          }
        }
      }
      if (!empty($keys_prepended)) {
        if ($entries = $this->getManifestEntries($keys_prepended)) {
          foreach ($keys as $i => $key) {
            if (!isset($keys_prepended[$key])) {
              continue;
            }
            $prepended = $keys_prepended[$key];
            if (isset($entries[$prepended])) {
              unset($keys[$i]);
            }
          }
        }
      }
    }

    foreach ($keys as $key) {
      $cid = 'entrypoints:manifest:' . $key;
      if (!isset($this->cache[$cid])) {
        if (isset($this->cache['manifest'][$key])) {
          $this->cache[$cid] = $this->cache['manifest'][$key];
        }
        else {
          $cids[] = $cid;
        }
      }
    }
    if (!empty($cids)) {
      foreach ($this->cacheBackendManifest->getMultiple($cids) as $cid => $cached) {
        $this->cache[$cid] = $cached->data;
      }
    }

    $cache_items = [];

    foreach ($keys as $key) {
      $cid = 'entrypoints:manifest:' . $key;
      if (!isset($this->cache[$cid])) {
        $this->deriveAll();
        $this->cache[$cid] = isset($this->cache['manifest'][$key]) ? $this->cache['manifest'][$key] : FALSE;
        $cache_items[$cid] = ['data' => $this->cache[$cid]];
      }
      if (!empty($this->cache[$cid])) {
        $entries[$key] = $this->cache[$cid];
      }
    }

    if (!empty($cache_items)) {
      $this->cacheBackendManifest->setMultiple($cache_items);
    }

    return $entries;
  }

  /**
   * Get a single manifest.json entry.
   *
   * @param string $key
   *   The entry key.
   *   In many occasions, the manifest keys include the base directory of the
   *   output folders (which is the value of the 'entrypoints.output_basedir'
   *   service parameter). For those entries, the base directory part does not
   *   need to be included. For example, when 'entrypoints.output_basedir' is
   *   set to be 'sites/all/assets', and an entry exists in the manifest.json
   *   with a key 'sites/all/assets/entry/index.js', then it can be obtained by
   *   setting the $key argument as 'entry/index.js'.
   *
   * @return array|false
   *   If exists, the entry as array with 'url' and 'file' key.
   *   The 'url' might be absolute or relative. The 'file' value might be
   *   an absolute local path or an external url. If it does not exists,
   *   the method returns the boolean value FALSE.
   *
   * @see self::getManifestEntries()
   */
  public function getManifestEntry($key) {
    $entries = $this->getManifestEntries([$key]);
    return !empty($entries) ? reset($entries) : FALSE;
  }

  /**
   * Get a list of file content of manifest.json entries by given entry keys.
   *
   * @param string[] $keys
   *   The entry keys. Leave empty to get all existing entries.
   *   In many occasions, the manifest keys include the base directory of the
   *   output folders (which is the value of the 'entrypoints.output_basedir'
   *   service parameter). For those entries, the base directory part does not
   *   need to be included. For example, when 'entrypoints.output_basedir' is
   *   set to be 'sites/all/assets', and an entry exists in the manifest.json
   *   with a key 'sites/all/assets/entry/index.js', then it can be obtained by
   *   setting the $keys argument as ['entry/index.js'].
   * @param bool $use_cache
   *   (Optional) Set to TRUE if the file contents should be cached once loaded
   *   or not. Caching is enabled by default.
   *
   * @return array
   *   An array of file contents, keyed by the manifest keys. The file content
   *   value might be a string or FALSE on failure when reading the file.
   *   If the manifest key itself does not exist, no value exists for the key in
   *   the returned array.
   */
  public function getManifestEntriesContent(array $keys = [], $use_cache = TRUE) {
    $entries = $this->getManifestEntries($keys);
    $contents = [];

    if (!$use_cache) {
      foreach ($entries as $key => $value) {
        $contents[$key] = @file_get_contents($value['file']);
      }
      return $contents;
    }

    $cids = [];
    $cid_prefix = 'entrypoints:manifest_content:';
    $cid_prefix_length = strlen($cid_prefix);
    foreach (array_keys($entries) as $key) {
      $cids[] = $cid_prefix . $key;
    }
    if (!empty($cids)) {
      foreach ($this->cacheBackendManifest->getMultiple($cids) as $cid => $cached) {
        $key = substr($cid, $cid_prefix_length);
        $contents[$key] = $cached->data;
      }
    }
    $cache_items = [];
    foreach ($cids as $cid) {
      $key = substr($cid, $cid_prefix_length);
      $contents[$key] = @file_get_contents($entries[$key]['file']);
      $cache_items[$cid] = ['data' => $contents[$key]];
    }

    if (!empty($cache_items)) {
      $this->cacheBackendManifest->setMultiple($cache_items);
    }

    return $contents;
  }

  /**
   * Get the file content of a manifest.json entry.
   *
   * @param string $key
   *   The entry key.
   *   In many occasions, the manifest keys include the base directory of the
   *   output folders (which is the value of the 'entrypoints.output_basedir'
   *   service parameter). For those entries, the base directory part does not
   *   need to be included. For example, when 'entrypoints.output_basedir' is
   *   set to be 'sites/all/assets', and an entry exists in the manifest.json
   *   with a key 'sites/all/assets/entry/index.js', then it can be obtained by
   *   setting the $key argument as 'entry/index.js'.
   * @param bool $use_cache
   *   (Optional) Set to TRUE if the file content should be cached once loaded
   *   or not. Caching is enabled by default.
   *
   * @return string|false
   *   If exists, the file content of the manifest entry.
   *   Returns FALSE if it does not exist or if there was a failure on reading
   *   the file.
   */
  public function getManifestEntryContent($key, $use_cache = TRUE) {
    $contents = $this->getManifestEntriesContent([$key], $use_cache);
    return !empty($contents) ? reset($contents) : FALSE;
  }

  /**
   * Checks if a single manifest.json entry exists.
   *
   * @param string $key
   *   The entry key to check for.
   *
   * @return bool
   *   Returns TRUE if the entry exists, FALSE otherwise.
   *
   * @see self::getManifestEntries()
   */
  public function hasManifestEntry($key) {
    $entries = $this->getManifestEntries([$key]);
    return !empty($entries);
  }

  /**
   * Resets the internal in-memory cache.
   */
  public function reset() {
    $this->derived = FALSE;
    $this->cache = [];
  }

  /**
   * Clears the whole registry cache.
   */
  public function clearCache() {
    $cids = ['entrypoints:names'];
    foreach ($this->getDefinitions() as $definition) {
      $cids[] = 'entrypoints:definition:' . $definition->getName();
    }
    $cids_manifest = ['entrypoints:manifest'];
    foreach (array_keys($this->getManifestEntries()) as $manifest_key) {
      $cids_manifest[] = 'entrypoints:manifest:' . $manifest_key;
      $cids_manifest[] = 'entrypoints:manifest_content:' . $manifest_key;
    }
    $this->state->delete('entrypoints_registry_cache');
    $this->cacheBackend->deleteMultiple($cids);
    $this->cacheBackendManifest->deleteMultiple($cids_manifest);
    $this->reset();
  }

  /**
   * Get a list of currently known stale files.
   *
   * This operation does not necessarily perform a fresh lookup on
   * the file system. If you want to make sure that all recent files are
   * included, clear the cache by calling ::clearCache() before.
   *
   * @return string[]
   *   An array whose values are local file uris to stale files.
   */
  public function getStaleFiles() {
    if (!isset($this->cache['stale_files'])) {
      $this->deriveAll();
    }
    return $this->cache['stale_files'];
  }

  /**
   * Derives all entrypoint definitions and stores the information in cache.
   *
   * The derivation includes all existing entrypoint definitions as objects,
   * a list of all existing entrypoint names, stale files and manifest entries.
   *
   * The call of this method should be avoided if possible. The derivation comes
   * from existing state or expensive file system scans. Once the derivation
   * performed an expensive file system scan, it caches that information as
   * persistent state. Despite the caching, loading all information at once
   * might be an unnecessary overhead for certain requests. If possible, try to
   * obtain required information part-wise from the cache backend instead.
   * This method should only serve as a fallback, if the information could not
   * be obtained otherwise.
   */
  protected function deriveAll() {
    if ($this->derived) {
      return;
    }
    $this->derived = TRUE;

    if ($cached = $this->state->get('entrypoints_registry_cache')) {
      $this->cache = $cached;
      return;
    }

    $definitions = [];
    $stale_files = [];
    $manifest = [];

    $config = $this->configFactory->get('entrypoints.settings');
    $build_mode = $config->get('build_mode');
    $projects = $config->get('projects') ?: [];
    foreach ($projects as $project) {
      if (empty($project['build'][$build_mode]['location']) && empty($project['build'][$build_mode]['location_ssr'])) {
        continue;
      }
      $source = NULL;
      if (!empty($project['source']['location'])) {
        try {
          $source = EntrypointsProjectSource::load($project['source']['location']);
        }
        catch (\RuntimeException $e) {
          $this->loggerFactory->get('entrypoints')->error($e->getMessage());
        }
        if (!$source || !$source->exists()) {
          $this->loggerFactory->get('entrypoints')->warning("A project source location '@location' is defined, but it does not exist or is not accessible. If it does not exist on this environment, remove it from the entrypoints configuration.", ['@location' => $project['source']['location']]);
        }
      }
      $info = !empty($project['build'][$build_mode]['location']) ? static::scan($project['build'][$build_mode]['location'])
        : ['asset_files' => [], 'manifest' => [], 'stale_files' => []];
      $info_ssr = !empty($project['build'][$build_mode]['location_ssr']) ? static::scan($project['build'][$build_mode]['location_ssr'])
        : ['asset_files' => [], 'manifest' => [], 'stale_files' => []];
      foreach ($info_ssr['asset_files'] as $name => $grouped_assets) {
        if (empty($grouped_assets['js'])) {
          $this->loggerFactory->get('entrypoints')->error("The SSR output folder '@folder' of entrypoint '@name' does not provide any Javascript files for server-side rendering.",
            [
              '@folder' => $project['build'][$build_mode]['location_ssr'],
              '@name' => $name,
            ]);
          continue;
        }
        if (!empty($grouped_assets['css']) && empty($info['asset_files'][$name]['css'])) {
          $info['asset_files'][$name]['css'] = $grouped_assets['css'];
        }
        $info['asset_files'][$name]['ssr'] = $grouped_assets['js'];
      }
      foreach ($info['asset_files'] as $name => $grouped_assets) {
        $definition = [
          'name' => $name,
          'library_name' => 'entrypoints/' . $name,
          'settings' => $this->loadEntrypointSettings($name),
          'assets' => $grouped_assets,
          'build_mode' => $build_mode,
          'source' => !empty($project['source']) ? $project['source'] : [],
        ];
        $definition += $project['build'][$build_mode];
        $definition += [
          'location' => NULL,
          'location_ssr' => NULL,
          'script' => NULL,
        ];
        $definition['source'] += [
          'location' => NULL,
          'watch_outdated' => NULL,
        ];
        if (!empty($source)) {
          $definition['source'] += [
            'exists' => $source->exists(),
            'name' => $source->exists() ? $source->getName() : NULL,
            'version' => $source->exists() ? $source->getVersion() : NULL,
          ];
        }
        else {
          $definition['source'] += [
            'exists' => NULL,
            'name' => NULL,
            'version' => NULL,
          ];
        }
        if (isset($definitions[$name])) {
          $existing = $definitions[$name]->toArray();
          if (!empty($existing['assets']['ssr']) && !empty($definition['assets']['ssr'])) {
            $this->loggerFactory->get('entrypoints')->warning("Multiple SSR entrypoint definitions exist for '@entrypoint'. It is not clearly determined which definition will be used, but only one of the multiple definitions can be used. If you want to avoid this undefined behavior, change to unique entrypoint names.", ['@entrypoint' => $name]);
          }
          elseif ((!empty($existing['assets']['css']) && !empty($definition['assets']['css'])) || (!empty($existing['assets']['js']) && !empty($definition['assets']['js']))) {
            $this->loggerFactory->get('entrypoints')->warning("Multiple entrypoint definitions exist for '@entrypoint'. It is not clearly determined which definition will be used, but only one of the multiple definitions can be used. If you want to avoid this undefined behavior, change to unique entrypoint names.", ['@entrypoint' => $name]);
          }
          else {
            if (!empty($existing['assets']['ssr'])) {
              $definition['location_ssr'] = $existing['location_ssr'];
              $definition['assets']['ssr'] = $existing['assets']['ssr'];
              if (empty($definition['assets']['css']) && !empty($existing['assets']['css'])) {
                $definition['assets']['css'] = $existing['assets']['css'];
              }
            }
            elseif (!empty($definition['assets']['ssr'])) {
              $existing['location_ssr'] = $definition['location_ssr'];
              $existing['assets']['ssr'] = $definition['assets']['ssr'];
              if (empty($existing['assets']['css']) && !empty($definition['assets']['css'])) {
                $existing['assets']['css'] = $definition['assets']['css'];
              }
              $definition = $existing;
            }
          }
        }
        $definitions[$name] = new EntrypointDefinition($definition);
      }
      if (!empty($info['manifest'])) {
        $manifest += $info['manifest'];
      }
      if (!empty($info['stale_files'])) {
        $stale_files += $info['stale_files'];
      }
      if (!empty($info_ssr['manifest'])) {
        $manifest += $info_ssr['manifest'];
      }
      if (!empty($info_ssr['stale_files'])) {
        $stale_files += $info_ssr['stale_files'];
      }
    }

    if (count($stale_files) > 1000) {
      $this->loggerFactory->get('entrypoints')->warning("Your entrypoint output folders contain a huge amount of stale files. Make sure your output directories are not getting over-polluted.");
    }

    $this->cache = [
      'names' => array_combine(array_keys($definitions), array_keys($definitions)),
      'definitions' => $definitions,
      'stale_files' => array_values($stale_files),
      'manifest' => $manifest,
    ];
    $this->state->set('entrypoints_registry_cache', $this->cache);

    $cache_items = ['entrypoints:names' => ['data' => $this->cache['names']]];
    foreach ($this->cache['definitions'] as $name => $definition) {
      $cache_items['entrypoints:definition:' . $name] = ['data' => $definition];
    }
    $this->cacheBackend->setMultiple($cache_items);
    $cache_items = [
      'entrypoints:manifest' => ['data' => $this->cache['manifest']],
    ];
    foreach ($this->cache['manifest'] as $manifest_key => $manifest_value) {
      $cache_items['entrypoints:manifest:' . $manifest_key] = ['data' => $manifest_value];
    }
    $this->cacheBackendManifest->setMultiple($cache_items);
  }

  /**
   * Loads the effective entrypoint-specific settings.
   *
   * @return array
   *   The effective settings for the given entrypoint name.
   */
  protected function loadEntrypointSettings($entrypoint) {
    $config = $this->configFactory->get('entrypoints.settings');
    if ($overrides = $config->get('overrides')) {
      // A direct match applies first.
      foreach ($overrides as $override) {
        if ($override['entrypoint'] === $entrypoint) {
          unset($override['entrypoint']);
          return $override;
        }
      }
      // No direct match found, let the first wildcard match apply.
      foreach ($overrides as $override) {
        if (strpos($override['entrypoint'], '*') !== FALSE) {
          $pattern = '/^' . str_replace('\*', '.*', preg_quote($override['entrypoint'], '/')) . '$/';
          if (preg_match($pattern, $entrypoint)) {
            unset($override['entrypoint']);
            return $override;
          }
        }
      }
    }
    return $config->get('defaults');
  }

  /**
   * Helper wrapper to scan the entrypoints output directory.
   *
   * The service is called via Drupal service container instead of
   * dependency injection, so that it only gets loaded when necessary.
   *
   * @param string $uri
   *   The uri that contains the entrypoints.json file and asset builds.
   *
   * @return array
   *   An associative array, keyed by 'asset_files', 'manifest' and
   *   'stale_files'. See the referenced method beneath for more information.
   *
   * @see \Drupal\entrypoints\EntrypointsOutputScan::scan()
   */
  protected static function scan($uri) {
    return \Drupal::service('entrypoints.output_scan')->scan($uri);
  }

  /**
   * Helper wrapper to get the raw 'entrypoints.output_basedir' parameter value.
   *
   * @return string
   *   The raw unprocessed parameter value.
   */
  protected static function getOutputBasedirRawValue() {
    return \Drupal::getContainer()->getParameter('entrypoints.output_basedir');
  }

}

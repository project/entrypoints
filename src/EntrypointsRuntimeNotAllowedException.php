<?php

namespace Drupal\entrypoints;

/**
 * Entrypoints runtime not allowed exception class.
 */
class EntrypointsRuntimeNotAllowedException extends \RuntimeException {

  /**
   * The entrypoints runtime that is not allowed.
   *
   * @var string
   */
  protected $entrypointsRuntime;

  /**
   * Constructs an entrypoints runtime not allowed exception.
   *
   * @param string $runtime
   *   The runtime that is not allowed.
   * @param string $message
   *   (optional) The exception message.
   * @param int $code
   *   (optional) The error code.
   * @param \Exception $previous
   *   (optional) The previous exception.
   */
  public function __construct($runtime, $message = '', $code = 0, \Exception $previous = NULL) {
    $this->entrypointsRuntime = (string) $runtime;
    $message = $message ?: "The entrypoints runtime '{$this->entrypointsRuntime}' is not allowed to be run on this environment.";
    parent::__construct($message, $code, $previous);
  }

}

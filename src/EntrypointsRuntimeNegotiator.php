<?php

namespace Drupal\entrypoints;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\entrypoints\Plugin\EntrypointsRuntimePluginManager;

/**
 * The entrypoints runtime negotiator.
 */
class EntrypointsRuntimeNegotiator {

  /**
   * A list of allowed build modes, keyed by machine name and label as value.
   *
   * @var string[]
   */
  protected $buildModesAllowed;

  /**
   * A list of allowed runtimes.
   *
   * @var string[]
   */
  protected $runtimesAllowed;

  /**
   * A cached list of available runtimes.
   *
   * @var array
   */
  protected $runtimesAvailable;

  /**
   * The default runtime.
   *
   * @var string|null
   */
  protected $defaultRuntime = NULL;

  /**
   * The manager for entrypoint runtime plugins.
   *
   * @var \Drupal\entrypoints\Plugin\EntrypointsRuntimePluginManager
   */
  protected $pluginManager;

  /**
   * The logger channel factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;

  /**
   * Internal in-memory cache.
   *
   * @var array
   */
  protected $cache;

  /**
   * The EntrypointsRuntimeNegotiator constructor.
   *
   * @param \Drupal\entrypoints\Plugin\EntrypointsRuntimePluginManager $plugin_manager
   *   The manager for entrypoint runtime plugins.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger channel factory.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache backend.
   * @param array $build_modes
   *   A list of allowed compilation build modes, keyed by machine name.
   * @param array $runtimes
   *   A list of allowed runtimes.
   */
  public function __construct(EntrypointsRuntimePluginManager $plugin_manager, LoggerChannelFactoryInterface $logger_factory, ConfigFactoryInterface $config_factory, CacheBackendInterface $cache, array $build_modes, array $runtimes) {
    $this->reset();
    $this->pluginManager = $plugin_manager;
    $this->loggerFactory = $logger_factory;
    $this->cacheBackend = $cache;
    $this->buildModesAllowed = $build_modes;
    $this->runtimesAllowed = array_combine($runtimes, $runtimes);
    if ($runtime = $config_factory->get('entrypoints.settings')->get('runtime')) {
      try {
        $this->setDefaultRuntime($runtime);
      }
      catch (EntrypointsRuntimeNotAllowedException $e) {
        $this->loggerFactory->get('entrypoints')->error($e->getMessage());
      }
    }
  }

  /**
   * Get the available compilation build modes.
   *
   * @return string[]
   *   The available compilation build modes, keyed by machine name and label as
   *   value.
   */
  public function getBuildModesAvailable() {
    return $this->buildModesAllowed;
  }

  /**
   * Get the available runtimes.
   *
   * @return array
   *   The available runtimes, keyed by runtime, values are an array that
   *   consist of label and version information.
   */
  public function getRuntimesAvailable() {
    if (!isset($this->runtimesAvailable)) {
      $this->runtimesAvailable = [];
      foreach ($this->runtimesAllowed as $runtime) {
        $plugin = $this->getPlugin($runtime);
        if ($version = $plugin->getVersion()) {
          $this->runtimesAvailable[$runtime] = [
            'version' => $version,
            'label' => $plugin->getPluginDefinition()['label'],
          ];
        }
      }
    }
    return $this->runtimesAvailable;
  }

  /**
   * Set the default runtime.
   *
   * @param string $runtime
   *   The runtime to set.
   *
   * @throws \Drupal\entrypoints\EntrypointsRuntimeNotAllowedException
   *   If the given runtime is not allowed.
   */
  public function setDefaultRuntime($runtime) {
    if (!isset($this->runtimesAllowed[$runtime])) {
      throw new EntrypointsRuntimeNotAllowedException($runtime);
    }
    $this->defaultRuntime = $runtime;
  }

  /**
   * Get the plugin instance of the given or configured runtime to use.
   *
   * @param string $runtime
   *   (Optional) The runtime to use. Skip this argument to use the currently
   *   configured or previously set default runtime.
   *
   * @return \Drupal\entrypoints\Plugin\EntrypointsRuntimePluginInterface|null
   *   The plugin of the runtime, or NULL if not configured or not available.
   */
  public function get($runtime = NULL) {
    if ($runtime === NULL) {
      $runtime = $this->defaultRuntime;
    }
    if (empty($runtime) || !$this->isAvailable($runtime)) {
      return NULL;
    }
    return $this->getPlugin($runtime);
  }

  /**
   * Checks if the given runtime is available.
   *
   * This method uses caching. If you want to make sure that you always
   * know the current availability, work with ::getRuntimesAvailable() instead.
   *
   * @param string $runtime
   *   (Optional) The runtime to check for. Skip this argument to check
   *   for the currently configured or previously set default runtime.
   *
   * @return bool
   *   Returns TRUE if the runtime is available, FALSE otherwise.
   */
  public function isAvailable($runtime = NULL) {
    if ($runtime === NULL) {
      $runtime = $this->defaultRuntime;
    }
    if (empty($runtime)) {
      return FALSE;
    }
    $cid = 'entrypoints:runtimes';
    if (isset($this->cache[$cid])) {
      $available = $this->cache[$cid];
    }
    elseif ($cached = $this->cacheBackend->get($cid)) {
      $available = $cached->data;
      $this->cache[$cid] = $available;
    }
    else {
      $available = $this->getRuntimesAvailable();
      $this->cacheBackend->set($cid, $available);
      $this->cache[$cid] = $available;
    }
    return isset($available[$runtime]);
  }

  /**
   * Resets the internal in-memory cache.
   */
  public function reset() {
    $this->cache = [];
  }

  /**
   * Clears the whole runtime cache.
   */
  public function clearCache() {
    $this->cacheBackend->delete('entrypoints:runtimes');
    $this->reset();
  }

  /**
   * Get the runtime plugin instance for the given plugin ID.
   *
   * @param string $plugin_id
   *   The plugin ID.
   *
   * @return \Drupal\entrypoints\Plugin\EntrypointsRuntimePluginInterface
   *   The runtime plugin instance.
   */
  protected function getPlugin($plugin_id) {
    if (!isset($this->cache['plugin'][$plugin_id])) {
      $this->cache['plugin'][$plugin_id] = $this->pluginManager->createInstance($plugin_id);
    }
    return $this->cache['plugin'][$plugin_id];
  }

}

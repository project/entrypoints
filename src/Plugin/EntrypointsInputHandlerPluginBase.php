<?php

namespace Drupal\entrypoints\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\entrypoints\EntrypointDefinition;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for entrypoints input handler plugins.
 */
abstract class EntrypointsInputHandlerPluginBase extends PluginBase implements EntrypointsInputHandlerPluginInterface, ContainerFactoryPluginInterface {

  /**
   * The logger channel factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The JSON serialization service.
   *
   * @var \Drupal\Component\Serialization\SerializationInterface
   */
  protected $jsonSerialization;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->loggerFactory = $container->get('logger.factory');
    $instance->jsonSerialization = $container->get('serialization.json');
    return $instance;
  }

  /**
   * Get a logger channel.
   *
   * @param string $channel
   *   The channel to get. Default set to the 'entrypoints' channel.
   *
   * @return \Drupal\Core\Logger\LoggerChannelInterface
   *   The required logger channel.
   */
  protected function logger($channel = 'entrypoints') {
    return $this->loggerFactory->get($channel);
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRender(array &$input, array &$definitions, EntrypointsRendererPluginInterface &$renderer, &$context = NULL) {}

  /**
   * {@inheritdoc}
   */
  public function beforeRender(array &$input, EntrypointDefinition $definition, EntrypointsRendererPluginInterface $renderer, &$context = NULL) {}

  /**
   * {@inheritdoc}
   */
  public function afterRender(array $input, &$output, EntrypointDefinition $definition, EntrypointsRendererPluginInterface $renderer, &$context = NULL) {}

  /**
   * {@inheritdoc}
   */
  public function finishRender(array &$output, array &$definitions, EntrypointsRendererPluginInterface &$renderer, &$context = NULL) {}

}

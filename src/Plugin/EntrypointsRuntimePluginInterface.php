<?php

namespace Drupal\entrypoints\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * The interface implemented by all entrypoint runtime plugins.
 */
interface EntrypointsRuntimePluginInterface extends PluginInspectionInterface {

  /**
   * Get the currently installed version of the runtime.
   *
   * @return string
   *   The version string, or NULL if the runtime is not available or not
   *   accessible.
   */
  public function getVersion();

  /**
   * Installs packages in the given working directory.
   *
   * @param string $cwd
   *   The working directory with a valid and accessible package.json file.
   * @param string[] $packages
   *   A list of packages to install. Leave empty to run the installation
   *   command for all currently known packages listed in the package.json file.
   * @param string $dependency_type
   *   (Optional) The dependency type. The value be one of 'prod', 'dev' or
   *   'optional'. By default, the list of packages will be added to the
   *   production ('prod') dependencies.
   *
   * @return array|false
   *   An array that is grouped by 'added', 'updated' and 'removed'. Each
   *   group lists packages with the package name as key and the package version
   *   as value. Returns FALSE if something went wrong on runtime execution.
   */
  public function install($cwd, array $packages = [], $dependency_type = 'prod');

  /**
   * Updates (i.e. upgrades) packages in the given working directory.
   *
   * @param string $cwd
   *   The working directory with a valid and accessible package.json file.
   * @param string[] $packages
   *   A list of packages to update. Leave empty to update all currently known
   *   packages listed in the package.json file.
   *
   * @return array|false
   *   An array that is grouped by 'added', 'updated' and 'removed'. Each
   *   group lists packages with the package name as key and the package version
   *   as value. Returns FALSE if something went wrong on runtime execution.
   */
  public function update($cwd, array $packages = []);

  /**
   * Checks for outdated packages in the given working directory.
   *
   * @param string $cwd
   *   The working directory with a valid and accessible package.json file.
   * @param string[] $packages
   *   A list of packages to check for. Leave empty to check for all currently
   *   known packages listed in the package.json file.
   *
   * @return array|false
   *   An array that lists the outdated packages as keys and as a value an array
   *   that consists of 'current', 'wanted' and 'latest' version entries. If the
   *   working directory is up to date, an empty array is being returned.
   *   Returns boolean FALSE if something went wrong on runtime execution.
   */
  public function outdated($cwd, array $packages = []);

  /**
   * Runs the script in the given working directory.
   *
   * @param string $cwd
   *   The working directory with a valid and accessible package.json file.
   * @param string $name
   *   The script name as it is defined as a key in the package.json file.
   *
   * @return string|false
   *   The complete stdout of the executed script. Returns FALSE if something
   *   went wrong on runtime execution.
   */
  public function script($cwd, $name);

  /**
   * Resets the internal in-memory cache.
   */
  public function reset();

}

<?php

namespace Drupal\entrypoints\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\Process\Process;

/**
 * Base class for entrypoint runtime plugins.
 */
abstract class EntrypointsRuntimePluginBase extends PluginBase implements EntrypointsRuntimePluginInterface, ContainerFactoryPluginInterface {

  use RuntimePluginTrait;

  /**
   * Builds an executable process object.
   *
   * @param array $arguments
   *   The arguments for the runtime.
   * @param string|null $cwd
   *   The working directory or null to use the working dir of the current PHP
   *   process.
   * @param array|null $env
   *   The environment variables or null to use the same environment as the
   *   current PHP process.
   * @param mixed|null $input
   *   The input as stream resource, scalar or \Traversable, or null for no
   *   input.
   * @param int|float|null $timeout
   *   (Optional) The timeout in seconds, 0 or NULL for unlimited time. The
   *   limitation factor might be cut to the current maximum execution time.
   *
   * @return \Symfony\Component\Process\Process
   *   The wrapped process object, ready to be executed.
   */
  protected function buildExecutableProcess(array $arguments, $cwd = NULL, array $env = NULL, $input = NULL, $timeout = NULL) {
    if ($timeout == 0) {
      $timeout = NULL;
    }
    if ($max_execution_time = $this->getMaxExecutionTime()) {
      if ($max_execution_time > 0 && ($timeout === NULL || $max_execution_time < $timeout)) {
        if ($max_execution_time < 240 && empty($this->cache['warning_emitted'])) {
          $this->logger()->warning("A very limited time window for the entrypoints runtime execution was detected. Please avoid a too much limited max_execution_time resource when using the entrypoints runtime. Make sure that long-running entrypoint runtime executions will not be run on regular public site requests.");
          $this->cache['warning_emitted'] = TRUE;
        }
        $timeout = $max_execution_time > 25 ? $max_execution_time - 5 : $max_execution_time - 1;
        if ($timeout <= 0 && empty($this->cache['warning_emitted'])) {
          $this->logger()->critical("Too limited time window for the entrypoints runtime execution detected. The execution time is most likely too low. Please make sure to have a sufficient max_execution time available.");
          $this->cache['warning_emitted'] = TRUE;
          $timeout = 0.5;
        }
      }
    }
    $command = array_merge([$this->bin], $arguments);
    return new Process($command, $cwd, $env, $input, $timeout);
  }

}

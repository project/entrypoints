<?php

namespace Drupal\entrypoints\Plugin\Block;

use Drupal\Component\Plugin\Exception\ContextException;
use Drupal\Component\Utility\Crypt;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * The Entrypoint block plugin.
 *
 * @Block(
 *   id = "entrypoint",
 *   admin_label = @Translation("Entrypoint"),
 *   category = @Translation("Entrypoints")
 * )
 */
class EntrypointBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The entrypoints registry.
   *
   * @var \Drupal\entrypoints\EntrypointsRegistry
   */
  protected $entrypointsRegistry;

  /**
   * The context repository.
   *
   * @var \Drupal\Core\Plugin\Context\ContextRepositoryInterface
   */
  protected $contextRepository;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->entrypointsRegistry = $container->get('entrypoints.registry');
    $instance->contextRepository = $container->get('context.repository');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->moduleHandler = $container->get('module_handler');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $default = ['entrypoint' => NULL];
    return $default + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();
    $form['entrypoint'] = [
      '#type' => 'select',
      '#title' => $this->t('Entrypoint'),
      '#options' => $this->getAllowedEntrypointOptions($form_state),
      '#required' => TRUE,
      '#empty_value' => '_none',
      '#default_value' => isset($config['entrypoint']) ? $config['entrypoint'] : NULL,
      '#weight' => 10,
    ];
    $context_values_options = [
      '_none' => $this->t('- None -'),
      'serialization' => $this->t('Serialization'),
      'text' => $this->t('Formatted text'),
    ];
    $form['context_values'] = [
      '#type' => 'select',
      '#title' => $this->t('Provide context values with'),
      '#options' => $context_values_options,
      '#default_value' => !empty($config['context_values']) ? $config['context_values'] : NULL,
      '#empty_value' => '_none',
      '#weight' => 30,
    ];
    $form['context_values_help'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $this->t('The chosen context and its method for context values will be provided as Drupal Javascript settings (within <em>drupalSettings.entrypoints</em>) and will be passed to the server-side rendering process (within <em>DrupalServer.entrypoints</em>).<br/>
        If you want to use <em>serialization</em>, make sure that the Core serialization module is installed. Please be careful, when using serialization, that you do not expose sensitive information. You can prevent this by specifying a restricted subset of fields only to be included.<br/>
        If you use formatted text, make sure you have the Core filter module installed. You can use <a href="https://www.drupal.org/project/token_filter" target="_blank" rel="noopener noreferrer">Token Filter</a> or <a href="https://www.drupal.org/project/mustache_templates" target="_blank" rel="noopener noreferrer">Mustache Templates</a> to insert dynamic values provided by given context. When using Tokens in any way, the <a href="https://www.drupal.org/project/token" target="_blank" rel="noopener noreferrer">contrib Token module</a> is recommended to be installed.'),
      '#weight' => 40,
    ];
    $restrict_fields = !empty($config['serialization']['fields']) ? $config['serialization']['fields'] : [];
    $form['serialization']['#tree'] = TRUE;
    $form['serialization']['#weight'] = 50;
    $form['serialization']['fields'] = [
      '#type' => 'textarea',
      '#default_value' => implode("\n", $restrict_fields),
      '#title' => $this->t('Serialization: restrict to fields'),
      '#rows' => '10',
      '#description' => $this->t('Enter the machine name of the field here. Write one machine name per line to include multiple fields. <b>If no field is specified here, all values of the entity will be exposed</b> - this could be dangerous, so you might want to restrict to certain fields here.'),
      '#states' => [
        'visible' => [
          ':input[name="settings[context_values]"]' => ['value' => 'serialization'],
        ],
      ],
      '#weight' => 10,
    ];
    $form['text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Text'),
      '#default_value' => !empty($config['text']['value']) ? $config['text']['value'] : '',
      '#tree' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="settings[context_values]"]' => ['value' => 'text'],
        ],
      ],
      '#weight' => 60,
    ];
    if ($this->moduleHandler->moduleExists('filter')) {
      $form['text']['#type'] = 'text_format';
      $form['text']['#format'] = !empty($config['text']['format']) ? $config['text']['format'] : NULL;
    }

    if (!empty($config['context_values']) && $config['context_values'] === 'serialization' && !$this->moduleHandler->moduleExists('serialization')) {
      $this->messenger()->addWarning($this->t('The Core serialization module is not installed. The serialization method to provide context values will not work unless that module is installed.'));
    }

    return $form + parent::blockForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['context_mapping']['#tree'] = TRUE;
    $form['context_mapping']['#weight'] = 20;
    $form['context_mapping']['entrypoint'] = [
      '#title' => $this->t('Context'),
      '#type' => 'select',
      '#options' => $this->getAllowedContextOptions($form_state),
      '#required' => FALSE,
      '#empty_value' => '_none',
      '#default_value' => isset($config['context_mapping']['entrypoint']) ? $config['context_mapping']['entrypoint'] : NULL,
      '#weight' => 10,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $entrypoints_allowed = $this->getAllowedEntrypointOptions($form_state);
    $entrypoint = $form_state->getValue('entrypoint');
    if (empty($entrypoint) || $entrypoint === '_none') {
      $this->configuration['entrypoint'] = NULL;
    }
    elseif (isset($entrypoints_allowed[$entrypoint])) {
      $this->configuration['entrypoint'] = $entrypoint;
    }

    $context_allowed = $this->getAllowedContextOptions($form_state);
    $context = $form_state->getValue(['context_mapping', 'entrypoint']);
    if (empty($context) || $context === '_none' || !isset($context_allowed[$context])) {
      $form_state->unsetValue(['context_mapping', 'entrypoint']);
    }
    else {
      $this->configuration['context_mapping']['entrypoint'] = $context;
    }

    $context_values = $form_state->getValue('context_values');
    if (empty($context_values) || $context_values === '_none') {
      $this->configuration['context_values'] = NULL;
    }
    else {
      $this->configuration['context_values'] = $context_values;
    }

    $restricted_fields = $form_state->getValue(['serialization', 'fields']);
    if (empty($restricted_fields)) {
      $restricted_fields = '';
    }
    $restricted_fields = explode('<br />', nl2br($restricted_fields));
    $fields = [];
    foreach ($restricted_fields as $field) {
      $field = strtolower(trim($field));
      if (!empty($field)) {
        $fields[$field] = $field;
      }
    }
    $this->configuration['serialization']['fields'] = array_values($fields);

    $text = $form_state->getValue('text');
    if (is_string($text)) {
      $text = ['value' => $text, 'format' => NULL];
    }
    $this->configuration['text'] = $text;

    parent::blockSubmit($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();
    if (empty($config['entrypoint'])) {
      return [];
    }
    $entrypoint = $config['entrypoint'];
    if (!($definitions = $this->entrypointsRegistry->getDefinitions([$entrypoint]))) {
      return [];
    }
    $definition = reset($definitions);
    if (!($library_name = $definition->getLibraryName())) {
      return [];
    }
    $id = Crypt::randomBytesBase64(8);
    $context_info = (object) ['value' => NULL];
    $build = [
      'container' => [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#attributes' => [
          'id' => $id,
          'data-entrypoint' => $entrypoint,
        ],
        '#attached' => [
          'library' => [
            'core/drupalSettings',
            $library_name,
          ],
          'drupalSettings' => [
            'entrypoints' => [
              $entrypoint => [
                $id => $context_info,
              ],
            ],
          ],
        ],
      ],
    ];
    if (!empty($config['context_mapping']['entrypoint'])) {
      $context = $this->getContext('entrypoint');
      if ($context->hasContextValue()) {
        $context_value = $context->getContextValue();
        if ($context_value instanceof EntityInterface) {
          $entity = $context_value;
          $context_info->type = $entity->getEntityTypeId();
          $context_info->is_new = $entity->isNew();
          $context_info->id = $entity->isNew() ? NULL : $entity->id();
          if ($entity instanceof RevisionableInterface) {
            $context_info->rid = $entity->isNew() ? NULL : $entity->getRevisionId();
          }
          $context_info->uuid = $entity->uuid();
          $context_info->langcode = $context_value->language()->getId();
        }
      }
    }
    if (!empty($config['context_values'])) {
      if ($config['context_values'] === 'serialization' && isset($entity) && $this->moduleHandler->moduleExists('serialization')) {
        $serializer = static::getSerializer();
        if (empty($config['serialization']['fields'])) {
          $context_info->value = $serializer->normalize($entity, 'json');
        }
        elseif ($entity instanceof ContentEntityInterface) {
          $fields = [];
          foreach ($config['serialization']['fields'] as $field) {
            if ($entity->hasField($field)) {
              $fields[$field] = $serializer->normalize($entity->get($field), 'json');
            }
          }
          $context_info->value = $fields;
        }
      }
      elseif ($config['context_values'] === 'text') {
        if (isset($entity)) {
          if ($this->moduleHandler->moduleExists('token_filter')) {
            $token_filter_entity = &drupal_static('token_filter_entity');
            $token_filter_entity = $entity;
          }
          if ($this->moduleHandler->moduleExists('mustache_templates')) {
            $mustache_tokens = &drupal_static('mustache_tokens', []);
            $mustache_tokens[$entity->getEntityTypeId()] = $entity;
          }
        }
        $text_value = isset($config['text']['value']) ? $config['text']['value'] : '';
        $text_format = isset($config['text']['format']) ? $config['text']['format'] : NULL;
        $text_build = [];
        if (!isset($text_format) || !$this->moduleHandler->moduleExists('filter')) {
          $text_build = ['#markup' => $text_value];
        }
        else {
          $text_build = [
            '#type' => 'processed_text',
            '#text' => $text_value,
            '#format' => $text_format,
          ];
        }
        $renderer = static::getRenderer();
        $render_context = new RenderContext();
        $context_info->value = $renderer->executeInRenderContext($render_context, function () use (&$text_build, $renderer) {
          return trim((string) $renderer->render($text_build));
        });
        /** @var \Drupal\Core\Render\BubbleableMetadata $bubbleable_metadata */
        $bubbleable_metadata = $render_context->pop();
        $bubbleable_metadata->applyTo($build);
      }
    }
    return $build;
  }

  /**
   * Get allowed entrypoint options for the given form state.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The allowed entrypoint options.
   */
  protected function getAllowedEntrypointOptions(FormStateInterface $form_state) {
    $options = ['_none' => $this->t('- None -')];
    $names = $this->entrypointsRegistry->getNames();
    $options += array_combine($names, $names);
    return $options;
  }

  /**
   * Get allowed context options for the given form state.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The allowed context options.
   */
  protected function getAllowedContextOptions(FormStateInterface $form_state) {
    $options = ['_none' => $this->t('- None -')];
    $contexts = $form_state->hasTemporaryValue('gathered_contexts') ? $form_state->getTemporaryValue('gathered_contexts') : [];
    $contexts += $this->getContexts();
    $contexts += $this->contextRepository->getAvailableContexts();
    unset($contexts['entrypoint']);

    // When using Layout Builder blocks, make sure the "entity being viewed"
    // context is at first place on the selection list.
    if (isset($contexts['layout_builder.entity'])) {
      /** @var \Drupal\Core\Plugin\Context\EntityContext $entity_context */
      $entity_context = $contexts['layout_builder.entity'];
      $label = new TranslatableMarkup('@entity being viewed', [
        '@entity' => $entity_context->getContextValue()->getEntityType()->getSingularLabel(),
      ]);
      $entity_context->getContextDefinition()->setLabel($label);
      unset($contexts['layout_builder.entity']);
      $contexts = ['layout_builder.entity' => $entity_context] + $contexts;
    }

    foreach ($contexts as $context_id => $context) {
      $data_type = explode(':', $context->getContextDefinition()->getDataType(), 3);
      if (!(reset($data_type) === 'entity') || !($entity_type_id = next($data_type))) {
        continue;
      }
      /** @var \Drupal\Core\Entity\EntityTypeInterface $type_definition */
      $type_definition = $this->entityTypeManager->getDefinition($entity_type_id);
      if ($type_definition instanceof ContentEntityTypeInterface) {
        $options[$context_id] = $context->getContextDefinition()->getLabel();
      }
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function getContextDefinitions() {
    $config = $this->getConfiguration();
    if (empty($config['context_mapping']['entrypoint'])) {
      return parent::getContextDefinitions();
    }
    return parent::getContextDefinitions() + [
      'entrypoint' => $this->getContextDefinition('entrypoint'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getContextDefinition($name) {
    if ($name !== 'entrypoint') {
      return parent::getContextDefinition($name);
    }
    $config = $this->getConfiguration();
    if (empty($config['context_mapping']['entrypoint'])) {
      throw new ContextException("The entrypoint context is not configured.");
    }
    $context_id = $config['context_mapping']['entrypoint'];
    if (isset($this->context[$context_id])) {
      return $this->context[$context_id]->getContextDefinition();
    }
    $definitions = parent::getContextDefinitions();
    if (isset($definitions[$context_id])) {
      return $definitions[$context_id];
    }
    $contexts = $this->contextRepository->getAvailableContexts();
    if (isset($contexts[$context_id])) {
      $definition = clone $contexts[$context_id]->getContextDefinition();
      $definition->setRequired(FALSE);
      return $definition;
    }
    throw new ContextException(sprintf("The configured entrypoint context %s does not exist.", $context_id));
  }

  /**
   * Get the serializer service.
   *
   * @return \Symfony\Component\Serializer\Serializer
   *   The serializer service.
   */
  protected static function getSerializer() {
    return \Drupal::service('serializer');
  }

  /**
   * Get the renderer service.
   *
   * @return \Drupal\Core\Render\Renderer
   *   The renderer service.
   */
  protected static function getRenderer() {
    return \Drupal::service('renderer');
  }

}

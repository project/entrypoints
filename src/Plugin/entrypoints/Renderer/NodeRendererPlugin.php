<?php

namespace Drupal\entrypoints\Plugin\entrypoints\Renderer;

use Drupal\Core\File\FileSystemInterface;
use Drupal\entrypoints\EntrypointDefinition;
use Drupal\entrypoints\Plugin\EntrypointsRendererPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The node.js server-side renderer plugin.
 *
 * @EntrypointsRenderer(
 *   id="node",
 *   label="Node.js"
 * )
 */
class NodeRendererPlugin extends EntrypointsRendererPluginBase {

  /**
   * The cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;

  /**
   * The state storage.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The local uri resolver.
   *
   * @var \Drupal\entrypoints\LocalUriResolver
   */
  protected $localUriResolver;

  /**
   * The lock backend.
   *
   * @var \Drupal\Core\Lock\LockBackendInterface
   */
  protected $lock;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->cacheBackend = $container->get('cache.entrypoints');
    $instance->state = $container->get('state');
    $instance->fileSystem = $container->get('file_system');
    $instance->localUriResolver = $container->get('entrypoints.local_uri');
    $instance->lock = $container->get('lock');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function render(EntrypointDefinition $definition, array $input) {
    if (!($assets = $definition->getAssetFiles('ssr'))) {
      $this->logger()->error("The entrypoint '%name' is configured for server-side rendering (SSR), but it has no SSR assets.", ['%name' => $definition->getName()]);
      return $input;
    }
    if (!isset($this->cache['node'])) {
      if ($cached = $this->cacheBackend->get('entrypoints:renderer:node')) {
        $this->cache['node'] = $cached->data;
      }
      elseif ($cached = $this->state->get('entrypoints_renderer_node_cache')) {
        $this->cache['node'] = $cached;
        $this->cacheBackend->set('entrypoints:renderer:node', $cached);
      }
      else {
        $this->cache['node'] = [];
      }
    }
    if (!empty($definition->getSsrOutputLocation())) {
      $ssr_output_folder = $this->localUriResolver->getRealpath($definition->getSsrOutputLocation());
    }
    if (empty($ssr_output_folder)) {
      $ssr_output_folder = $this->fileSystem->dirname(reset($assets));
    }

    $name = $definition->getName();
    $cwd = $ssr_output_folder . DIRECTORY_SEPARATOR . '.drupal_compile' . DIRECTORY_SEPARATOR . 'node';
    $compile_file = $cwd . DIRECTORY_SEPARATOR . $name . '.js';

    if (!isset($this->cache['node']['compiled'][$compile_file]) || !@file_exists($compile_file)) {
      $lock_key = 'entrypoints_compile_' . substr($name, 0, 100);
      if (!$this->lock->acquire($lock_key, 5.0)) {
        $this->lock->wait($lock_key, 3);
        $this->cache['compile_attempts'][$name] = isset($this->cache['compile_attempts'][$name]) ? $this->cache['compile_attempts'][$name]++ : 1;
        unset($this->cache['node']);
        if ($this->cache['compile_attempts'][$name] < 5) {
          return $this->render($definition, $input);
        }
        else {
          throw new \RuntimeException(sprintf("Lock acquisition deadlock occurred when trying to compile server-side rendering entrypoint '%s'.", $name));
        }
      }

      $contents = <<<JS
require('process');
var DrupalServer = JSON.parse(process.argv[2]);
JS;
      foreach ($assets as $file_path) {
        if ($asset_contents = @file_get_contents($file_path)) {
          $contents .= "\n" . $asset_contents;
        }
      }
      $contents .= "\n" . "process.stdout.write(JSON.stringify(DrupalServer));\n";

      if (!$this->fileSystem->prepareDirectory($cwd, FileSystemInterface::CREATE_DIRECTORY) || !@file_put_contents($compile_file, $contents)) {
        throw new \RuntimeException(sprintf("Compilation failure occurred for server-side rendering entrypoint '%s'.", $name));
      }

      $this->cache['node']['compiled'][$compile_file] = TRUE;
      if ($current_state = $this->state->get('entrypoints_renderer_node_cache')) {
        if (isset($current_state['compiled'])) {
          $this->cache['node']['compiled'] += $current_state['compiled'];
        }
      }
      $this->state->set('entrypoints_renderer_node_cache', $this->cache['node']);
      $this->cacheBackend->set('entrypoints:renderer:node', $this->cache['node']);
      $this->lock->release($lock_key);
    }

    $arguments = [
      $this->fileSystem->basename($compile_file),
      $this->jsonSerialization->encode($input),
    ];
    $process = $this->buildExecutableProcess($arguments, $cwd);

    if ($process->run() != 0) {
      throw new \RuntimeException(sprintf("Server-side rendering of entrypoint '%s' failed. The error was: %s", $name, $process->getErrorOutput()));
    }

    if (!($output = $process->getIncrementalOutput())) {
      return FALSE;
    }
    return $this->jsonSerialization->decode($output);
  }

  /**
   * {@inheritdoc}
   */
  public function clearCache() {
    $this->state->delete('entrypoints_renderer_node_cache');
    $this->cacheBackend->delete('entrypoints:renderer:node');
    parent::clearCache();
  }

}

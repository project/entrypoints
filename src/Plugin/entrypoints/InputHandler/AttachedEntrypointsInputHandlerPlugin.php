<?php

namespace Drupal\entrypoints\Plugin\entrypoints\InputHandler;

use Drupal\Core\Render\HtmlResponse;
use Drupal\entrypoints\Plugin\EntrypointsInputHandlerPluginBase;
use Drupal\entrypoints\Plugin\EntrypointsRendererPluginInterface;

/**
 * The input handler plugin that provides info about attached entrypoints.
 *
 * An entrypoint can be added as a block with the EntrypointBlock plugin.
 * That block attaches information into the drupalSettings.entrypoints, keyed
 * by entrypoint names. Every entrypoint object has entries, keyed by HTML ID
 * that identifies the block within the DOM. And these entries themselves may
 * have a context, e.g. a node, user or other sort of entity. That context
 * information is being exposed by the block configuration, e.g. as a complete
 * serialized object or a text that is concatenated by dynamic values. Example:
 *
 * "entrypoints":{
 *   "index":{ // The entrypoint name.
 *     "s7oHDbzHOUg":{ // The HTML ID in the DOM.
 *       "value":"john", // A context value, provided by the block config.
 *       "type":"user", // The entity type of the given context.
 *       "is_new":false, // Whether the entity is new or not.
 *       "id":"1",  // The entity ID.
 *       "rid":null, // The revision ID, if the entity would have a revision.
 *       "uuid":"...", // The entity UUID.
 *       "langcode":"en" // The entity langcode.
 *     }
 *   }
 * }
 *
 * @EntrypointsInputHandler(
 *   id="attached",
 *   label="Attached entrypoints",
 *   priority=1
 * )
 */
class AttachedEntrypointsInputHandlerPlugin extends EntrypointsInputHandlerPluginBase {

  /**
   * {@inheritdoc}
   */
  public function prepareRender(array &$input, array &$definitions, EntrypointsRendererPluginInterface &$renderer, &$context = NULL) {
    if (!($context instanceof HtmlResponse)) {
      return;
    }
    $attachments = $context->getAttachments();
    if (!isset($attachments['drupalSettings']['entrypoints'])) {
      return;
    }
    $input['entrypoints'] = $attachments['drupalSettings']['entrypoints'];
  }

}

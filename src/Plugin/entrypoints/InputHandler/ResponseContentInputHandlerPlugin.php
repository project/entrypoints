<?php

namespace Drupal\entrypoints\Plugin\entrypoints\InputHandler;

use Drupal\Core\Render\HtmlResponse;
use Drupal\entrypoints\Plugin\EntrypointsInputHandlerPluginBase;
use Drupal\entrypoints\Plugin\EntrypointsRendererPluginInterface;

/**
 * The entrypoints response content input handler plugin.
 *
 * The response content is mutable by the SSR process. It can be completely
 * changed by directly changing the value of DrupalServer.content.
 *
 * @EntrypointsInputHandler(
 *   id="content",
 *   label="Response content",
 *   priority=0
 * )
 */
class ResponseContentInputHandlerPlugin extends EntrypointsInputHandlerPluginBase {

  /**
   * {@inheritdoc}
   */
  public function prepareRender(array &$input, array &$definitions, EntrypointsRendererPluginInterface &$renderer, &$context = NULL) {
    if (!($context instanceof HtmlResponse)) {
      return;
    }
    $input['content'] = $context->getContent();
  }

  /**
   * {@inheritdoc}
   */
  public function finishRender(array &$output, array &$definitions, EntrypointsRendererPluginInterface &$renderer, &$context = NULL) {
    if (!($context instanceof HtmlResponse)) {
      return;
    }
    if (isset($output['content'])) {
      $context->setContent($output['content']);
    }
  }

}

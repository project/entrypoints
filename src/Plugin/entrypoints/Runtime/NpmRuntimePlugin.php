<?php

namespace Drupal\entrypoints\Plugin\entrypoints\Runtime;

use Drupal\entrypoints\Plugin\EntrypointsRuntimePluginBase;

/**
 * The NPM runtime plugin.
 *
 * @EntrypointsRuntime(
 *   id = "npm",
 *   label = "NPM"
 * )
 */
class NpmRuntimePlugin extends EntrypointsRuntimePluginBase {

  /**
   * {@inheritdoc}
   */
  public function install($cwd, array $packages = [], $dependency_type = 'prod') {
    $arguments = array_merge(['--silent', '--json', 'install'], $packages);
    if (empty($packages)) {
      if ($dependency_type === 'prod') {
        array_unshift($arguments, '--production');
      }
      elseif ($dependency_type === 'dev') {
        array_unshift($arguments, '--production=false');
      }
    }
    elseif (in_array($dependency_type, ['prod', 'dev', 'optional'])) {
      array_unshift($arguments, '--save-' . $dependency_type);
    }

    $process = $this->buildExecutableProcess($arguments, $cwd);
    if ($process->run() != 0) {
      return FALSE;
    }

    $result = ['added' => [], 'updated' => [], 'removed' => []];
    if ($actions = $process->getIncrementalOutput()) {
      $actions = $this->jsonSerialization->decode($actions);
      foreach (['added', 'updated', 'removed'] as $action_type) {
        if (!empty($actions[$action_type])) {
          foreach ($actions[$action_type] as $action) {
            $result[$action_type][$action['name']] = $action['version'];
          }
        }
      }
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function update($cwd, array $packages = []) {
    $arguments = array_merge(['--silent', '--json', 'update'], $packages);

    $process = $this->buildExecutableProcess($arguments, $cwd);
    if ($process->run() != 0) {
      return FALSE;
    }

    $result = ['added' => [], 'updated' => [], 'removed' => []];
    if ($actions = $process->getIncrementalOutput()) {
      $actions = $this->jsonSerialization->decode($actions);
      foreach (['added', 'updated'] as $action_type) {
        if (!empty($actions[$action_type])) {
          foreach ($actions[$action_type] as $action) {
            $result[$action_type][$action['name']] = $action['version'];
          }
        }
      }
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function outdated($cwd, array $packages = []) {
    $arguments = array_merge(['--silent', '--json', 'outdated'], $packages);

    $process = $this->buildExecutableProcess($arguments, $cwd);
    $exit_status = $process->run();
    $output = $process->getOutput();

    if (($exit_status != 0 && empty($output)) || !empty($process->getErrorOutput())) {
      return FALSE;
    }

    $result = [];
    if ($outdateds = $process->getIncrementalOutput()) {
      $outdateds = $this->jsonSerialization->decode($outdateds);
      if (!empty($outdateds)) {
        foreach ($outdateds as $name => $outdated) {
          $result[$name] = [
            'current' => $outdated['current'],
            'wanted' => $outdated['wanted'],
            'latest' => $outdated['latest'],
          ];
        }
      }
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function script($cwd, $name) {
    $arguments = ['--silent', 'run', $name];
    $process = $this->buildExecutableProcess($arguments, $cwd);
    if ($process->run() != 0) {
      return FALSE;
    }
    return $process->getOutput();
  }

}

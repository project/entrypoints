<?php

namespace Drupal\entrypoints\Plugin\entrypoints\Runtime;

use Drupal\entrypoints\Plugin\EntrypointsRuntimePluginBase;

/**
 * The YARN runtime plugin.
 *
 * @EntrypointsRuntime(
 *   id = "yarn",
 *   label = "YARN"
 * )
 */
class YarnRuntimePlugin extends EntrypointsRuntimePluginBase {

  /**
   * {@inheritdoc}
   */
  public function install($cwd, array $packages = [], $dependency_type = 'prod') {
    $installed_before = $this->getInstalledPackages($cwd);
    if ($installed_before === FALSE) {
      return FALSE;
    }

    $cmd = empty($packages) ? 'install' : 'add';
    $arguments = array_merge(['--silent', '--json', $cmd], $packages);
    if (empty($packages)) {
      if ($dependency_type === 'prod') {
        array_unshift($arguments, '--production');
      }
      elseif ($dependency_type === 'dev') {
        array_unshift($arguments, '--production=false');
      }
    }
    elseif (in_array($dependency_type, ['prod', 'dev', 'optional'])) {
      array_unshift($arguments, '--' . $dependency_type);
    }

    $process = $this->buildExecutableProcess($arguments, $cwd);
    if ($process->run() != 0) {
      return FALSE;
    }

    $installed_after = $this->getInstalledPackages($cwd);
    if ($installed_after === FALSE) {
      return FALSE;
    }

    return $this->installedInfo($installed_before, $installed_after);
  }

  /**
   * {@inheritdoc}
   */
  public function update($cwd, array $packages = []) {
    $installed_before = $this->getInstalledPackages($cwd);
    if ($installed_before === FALSE) {
      return FALSE;
    }

    if (empty($installed_before)) {
      return $this->install($cwd, $packages);
    }

    $arguments = array_merge(['--silent', '--json', 'upgrade'], $packages);

    $process = $this->buildExecutableProcess($arguments, $cwd);
    if ($process->run() != 0) {
      return FALSE;
    }

    $installed_after = $this->getInstalledPackages($cwd);
    if ($installed_after === FALSE) {
      return FALSE;
    }

    return $this->installedInfo($installed_before, $installed_after);
  }

  /**
   * {@inheritdoc}
   */
  public function outdated($cwd, array $packages = []) {
    $arguments = array_merge(['--silent', '--json', 'outdated'], $packages);

    $process = $this->buildExecutableProcess($arguments, $cwd);
    $exit_status = $process->run();
    $output = $process->getOutput();

    if (($exit_status != 0 && empty($output)) || !empty($process->getErrorOutput())) {
      return FALSE;
    }

    $result = [];
    $line = strtok($output, "\r\n");
    while ($line !== FALSE) {
      $line = $this->jsonSerialization->decode($line);
      if (!empty($line['data']['head']) && !empty($line['data']['body']) && !empty($line['type']) && $line['type'] === 'table') {
        foreach ($line['data']['body'] as $values) {
          $outdated = array_combine($line['data']['head'], $values);
          $result[$outdated['Package']] = [
            'current' => $outdated['Current'],
            'wanted' => $outdated['Wanted'],
            'latest' => $outdated['Latest'],
          ];
        }
      }
      $line = strtok("\r\n");
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function script($cwd, $name) {
    $arguments = ['--silent', 'run', $name];

    $process = $this->buildExecutableProcess($arguments, $cwd);
    if ($process->run() != 0) {
      return FALSE;
    }

    return $process->getOutput();
  }

  /**
   * Get a list of installed packages from the given working directory.
   *
   * @param string $cwd
   *   The working directory with a valid and accessible package.json file.
   *
   * @return array|false
   *   An array that lists the installed packages with the package name as key
   *   and the installed version as value. Returns FALSE if something went wrong
   *   on runtime execution.
   */
  protected function getInstalledPackages($cwd) {
    if (!@file_exists($cwd . DIRECTORY_SEPARATOR . 'yarn.lock')) {
      return [];
    }

    $arguments = ['--silent', '--json', 'list'];

    $process = $this->buildExecutableProcess($arguments, $cwd);
    if ($process->run() != 0) {
      return FALSE;
    }

    $installed = [];
    $output = $process->getOutput();
    $line = strtok($output, "\r\n");
    while ($line !== FALSE) {
      $line = $this->jsonSerialization->decode($line);
      if (!empty($line['data']['trees']) && !empty($line['data']['type']) && $line['data']['type'] === 'list') {
        foreach ($line['data']['trees'] as $tree) {
          $info = explode('@', $tree['name']);
          $installed[$info[0]] = $info[1];
        }
      }
      $line = strtok("\r\n");
    }

    return $installed;
  }

  /**
   * Builds up the information about installed, updated and removed packages.
   *
   * @param array $installed_before
   *   The list of packages with their previously installed version.
   * @param array $installed_after
   *   The list of packages with ther installed version afterwards.
   *
   * @return array
   *   The information as array, keyed by 'added', 'removed' and 'updated'.
   *   Each value of the group consists of the last used package version.
   */
  protected function installedInfo(array $installed_before, array $installed_after) {
    $result = [];
    $result['added'] = array_diff_key($installed_after, $installed_before);
    $result['removed'] = array_diff_key($installed_before, $installed_after);
    $result['updated'] = array_intersect_key($installed_after, $installed_before);
    foreach ($result['updated'] as $package => $version) {
      if ($version == $installed_before[$package]) {
        unset($result['updated'][$package]);
      }
    }
    return $result;
  }

}

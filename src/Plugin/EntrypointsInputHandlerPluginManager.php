<?php

namespace Drupal\entrypoints\Plugin;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * The entrypoints input handler plugin manager.
 */
class EntrypointsInputHandlerPluginManager extends DefaultPluginManager {

  /**
   * The EntrypointsInputHandlerPluginManager constructor.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/entrypoints/InputHandler', $namespaces, $module_handler, 'Drupal\entrypoints\Plugin\EntrypointsInputHandlerPluginInterface', 'Drupal\entrypoints\Annotation\EntrypointsInputHandler');
    $this->alterInfo('entrypoints_input_handler_plugin');
    $this->setCacheBackend($cache_backend, 'entrypoints_input_handler_plugin');
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinitions() {
    $definitions = parent::getDefinitions();

    // Sort the definitions by priority.
    $priorities = [];
    foreach ($definitions as $id => $definition) {
      $priorities[(int) $definition['priority']] = $id;
    }
    ksort($priorities, SORT_NUMERIC);
    // Higher priorities must be called earlier.
    $priorities = array_reverse($priorities, TRUE);

    $sorted_definitions = [];
    foreach ($priorities as $id) {
      $sorted_definitions[$id] = $definitions[$id];
    }

    return $sorted_definitions;
  }

}

<?php

namespace Drupal\entrypoints\Plugin;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Process\Process;

/**
 * Trait for plugin implementations that are using a runtime.
 */
trait RuntimePluginTrait {

  /**
   * The alias or absolute path to the executable binary.
   *
   * @var string
   */
  protected $bin;

  /**
   * Internal in-memory cache.
   *
   * @var array
   */
  protected $cache;

  /**
   * The logger channel factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The JSON serialization service.
   *
   * @var \Drupal\Component\Serialization\SerializationInterface
   */
  protected $jsonSerialization;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);

    $instance->reset();
    $instance->loggerFactory = $container->get('logger.factory');
    $instance->jsonSerialization = $container->get('serialization.json');
    $instance->bin = $plugin_id;
    $bin_path = $container->getParameter('entrypoints.bin_path');
    if (!empty($bin_path)) {
      if (substr($bin_path, -strlen(DIRECTORY_SEPARATOR)) === DIRECTORY_SEPARATOR) {
        $instance->bin = $bin_path . $plugin_id;
      }
      else {
        $instance->bin = $bin_path . DIRECTORY_SEPARATOR . $plugin_id;
      }
    }

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getVersion() {
    $process = new Process([$this->bin, '-v']);
    $process->run();
    if (!$process->isSuccessful()) {
      return NULL;
    }
    if ($output = strtok($process->getIncrementalOutput(), "\t\n")) {
      $output = trim($output);
      if (strpos(strtolower($output), 'v') === 0) {
        $output = substr($output, 1);
      }
    }
    if (empty($output)) {
      return NULL;
    }

    return preg_match('/^\d+\.\d+\.\d+$/', $output) === 1 ? $output : NULL;
  }

  /**
   * Get a logger channel.
   *
   * @param string $channel
   *   The channel to get. Default set to the 'entrypoints' channel.
   *
   * @return \Drupal\Core\Logger\LoggerChannelInterface
   *   The required logger channel.
   */
  protected function logger($channel = 'entrypoints') {
    return $this->loggerFactory->get($channel);
  }

  /**
   * Get the maximum execution time setting value.
   *
   * @return int
   *   The maximum execution time setting value.
   */
  protected function getMaxExecutionTime() {
    if (!isset($this->cache['max_execution_time'])) {
      $this->cache['max_execution_time'] = (int) ini_get('max_execution_time');
    }
    return $this->cache['max_execution_time'];
  }

  /**
   * {@inheritdoc}
   */
  public function reset() {
    $this->cache = [];
  }

}

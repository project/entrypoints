<?php

namespace Drupal\entrypoints\Plugin;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * The entrypoints runtime plugin manager.
 */
class EntrypointsRuntimePluginManager extends DefaultPluginManager {

  /**
   * The EntrypointsRuntimePluginManager constructor.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/entrypoints/Runtime', $namespaces, $module_handler, 'Drupal\entrypoints\Plugin\EntrypointsRuntimePluginInterface', 'Drupal\entrypoints\Annotation\EntrypointsRuntime');
    $this->alterInfo('entrypoints_runtime_plugin');
    $this->setCacheBackend($cache_backend, 'entrypoints_runtime_plugin');
  }

}

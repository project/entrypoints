<?php

namespace Drupal\entrypoints\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\entrypoints\EntrypointDefinition;

/**
 * The interface implemented by all entrypoint server-side renderer plugins.
 */
interface EntrypointsRendererPluginInterface extends PluginInspectionInterface {

  /**
   * Get the currently installed version of the renderer.
   *
   * @return string
   *   The version string, or NULL if the renderer is not available or not
   *   accessible.
   */
  public function getVersion();

  /**
   * Performs server-side rendering for the given entrypoint and input.
   *
   * @param \Drupal\entrypoints\EntrypointDefinition $definition
   *   The entrypoint definition that contains 'ssr' assets for the rendering.
   * @param array $input
   *   An array that contains input, which will be passed to the rendering
   *   process as "DrupalServer" Javascript object.
   *
   * @return array|false
   *   The generated output result as array, that typically contains updated
   *   values regards the given $input argument. Returns FALSE if the renderer
   *   execution did not deliver any result, but is not considered to be an
   *   error and thus is not worth throwing an exception.
   *
   * @throws \InvalidArgumentException
   *   When something goes wrong on passing arguments to the renderer, or when
   *   serializing or unserializing data goes wrong.
   * @throws \RuntimeException
   *   When somethong goes wrong on compiling or executing the renderer logic.
   */
  public function render(EntrypointDefinition $definition, array $input);

  /**
   * Clears the whole cache of this renderer plugin.
   */
  public function clearCache();

  /**
   * Resets the internal in-memory cache.
   */
  public function reset();

}

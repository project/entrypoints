<?php

namespace Drupal\entrypoints\Plugin;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * The entrypoints server-side renderer plugin manager.
 */
class EntrypointsRendererPluginManager extends DefaultPluginManager {

  /**
   * The EntrypointsRendererPluginManager constructor.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/entrypoints/Renderer', $namespaces, $module_handler, 'Drupal\entrypoints\Plugin\EntrypointsRendererPluginInterface', 'Drupal\entrypoints\Annotation\EntrypointsRenderer');
    $this->alterInfo('entrypoints_renderer_plugin');
    $this->setCacheBackend($cache_backend, 'entrypoints_renderer_plugin');
  }

}

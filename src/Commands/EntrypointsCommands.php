<?php

namespace Drupal\entrypoints\Commands;

use Drush\Commands\DrushCommands;
use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drupal\Core\Logger\RfcLogLevel;

/**
 * Entrypoints Drush commands.
 */
class EntrypointsCommands extends DrushCommands {

  /**
   * Creates a new Webpack Encore project and adds it to the entrypoints config.
   *
   * @param string $project_source_location
   *   The absolute or relative path from Drupal web root to the project source
   *   location.
   * @param array $options
   *   An associative array of options.
   *
   * @option entrypoint
   *   The name of the entrypoint to be identified as Drupal library
   *  "entrypoints/{entrypoint}".
   * @option output-folder
   *   The output folder to be defined where the production asset builds
   *   will be generated. Must be located at sites/all/assets/{output-folder}.
   *   If not set, the folder name of the source project is being used.
   * @option output-dev-folder
   *   The development build output folder. Must be located at
   *   sites/all/assets/{output-dev-folder}. If not set, the output-folder
   *   option value will be used with '_dev' suffix appended. See the README.md
   *   of this module for more information about working with development
   *   builds.
   * @option template
   *   The project template to use. You can either use 'default' for a plain
   *   and empty project, or 'react' when you want to create a React project.
   *   You can also set a local absolute path that points to your own custom
   *   template folder. You can also use 'default-ssr' for a plain example or
   *   'react-ssr' for a React example that additionally includes a server-side
   *   rendering part.
   * @option watch-outdated
   *   Whether the project should be watched by Drupal status report for having
   *   outdated packages (default is set to 'yes', otherwise set to 'no').
   * @option output-folder-ssr
   *   The production build folder of assets to be rendered on the server-side
   *   (SSR). If not set, the name for the output folder will be used, with
   *   '_ssr' suffix appended. The SSR output folder will only be created when
   *   an SSR entrypoint file exists in the template folder, namely
   *   index.ssr.js.
   * @option output-dev-folder-ssr
   *   The development build folder of assets to be rendered on the server-side
   *   (SSR). If not set, the name for the development output folder will be
   *   used, with '_ssr' suffix appended. The SSR output folder will only be
   *   created when an SSR entrypoint file exists in the template folder, namely
   *   index.ssr.js.
   *
   * @usage entrypoints:create-project ../assets/entry
   *   Creates a new project at ../assets/entry, and adds that project to the
   *   site's entrypoints configuration.
   *
   * @command entrypoints:create-project
   */
  public function createProject($project_source_location = '../assets/entry', array $options = [
    'entrypoint' => 'index',
    'output-folder' => NULL,
    'output-dev-folder' => NULL,
    'template' => 'default',
    'watch-outdated' => 'yes',
    'output-folder-ssr' => NULL,
    'output-dev-folder-ssr' => NULL,
  ]) {
    $output_folder = $options['output-folder'];
    $output_dev_folder = $options['output-dev-folder'];
    $entrypoint_name = $options['entrypoint'];
    $template = $options['template'];
    $watch_outdated = $options['watch-outdated'] === 'yes';
    $output_folder_ssr = $options['output-folder-ssr'];
    $output_dev_folder_ssr = $options['output-dev-folder-ssr'];
    $project_create = static::projectCreateService();
    $project_create->setIo($this->io());
    $project_register = static::projectRegisterService();
    $project_register->setIo($this->io());
    $success = FALSE;
    try {
      $success = $project_create->createSourceProject($project_source_location, $output_folder, $output_dev_folder, $entrypoint_name, $template, $watch_outdated, $output_folder_ssr, $output_dev_folder_ssr);
    }
    catch (\Exception $e) {
      $this->io()->error($e->getMessage());
    }
    if (!$success) {
      $this->io()->error("An error occurred during project creation. Please check the previous log for details.");
    }
  }

  /**
   * Adds an existing Webpack Encore project to the entrypoints configuration.
   *
   * @param string $project_source_location
   *   (Optional) The absolute or relative path from Drupal web root to the
   *   project source location. Skip this argument if there is no existing
   *   source folder on this environment.
   * @param array $options
   *   An associative array of options.
   *
   * @option output-folder
   *   The output folder to be defined where the production asset builds will
   *   be generated. Must be located at sites/all/assets/{output-folder}.
   *   If not set, no output folder is being set for this project.
   * @option build-script
   *   The name of the script for compiling production asset builds. If not set,
   *   the build script name is set to 'production'. Will only be used if the
   *   output-folder option is set with a given value. Make sure that the script
   *   key exists in the package.json file of the source project.
   * @option output-dev-folder
   *   The development build output folder. If not set, no development output is
   *   being set for this project. See the README.md of this module for more
   *   information about working with development builds. Must be located at
   *   sites/all/assets/{output-dev-folder}.
   * @option build-dev-script
   *   The name of the script for compiling development asset builds. If not
   *   set, the build script name is set to 'development'. Will only be used if
   *   the output-dev-folder option is set with a given value. Make sure that
   *   the script key exists in the package.json file of the source project.
   * @option watch-outdated
   *   Whether the project should be watched by Drupal status report for having
   *   outdated packages (default is set to 'yes', otherwise set to 'no').
   * @option output-folder-ssr
   *   The production build folder of assets to be rendered on the server-side
   *   (SSR). Must be either a folder name to be created under
   *   'entrypoints.output_basedir', or a uri that resolves locally to a
   *   sub-folder of 'entrypoints.output_basedir'.
   * @option output-dev-folder-ssr
   *   (Optional) The development build folder of assets to be rendered on the
   *   server-side (SSR).
   * @usage entrypoints:register ../assets/entry --output-folder=entry --output-dev-folder=entry_dev
   * @command entrypoints:register
   */
  public function register($project_source_location = NULL, array $options = [
    'output-folder' => NULL,
    'build-script' => 'production',
    'output-dev-folder' => NULL,
    'build-dev-script' => 'development',
    'watch-outdated' => 'yes',
    'output-folder-ssr' => NULL,
    'output-dev-folder-ssr' => NULL,
  ]) {
    $project_register = static::projectRegisterService();
    $project_register->setIo($this->io());
    if (empty($project_source_location)) {
      $project_source_location = NULL;
    }
    $success = FALSE;
    try {
      $success = $project_register->register($project_source_location, $options['output-folder'], $options['output-dev-folder'], $options['build_sript'], $options['build-dev-script'], $options['watch-outdated'], $options['output-folder-ssr'], $options['output-dev-folder-ssr']);
    }
    catch (\Exception $e) {
      $this->io()->error($e->getMessage());
    }
    if (!$success) {
      $this->io()->error("An error occurred when trying to register the project. Please check the previous log for details.");
    }
  }

  /**
   * Rebuilds all entrypoints by running their asset compilation scripts.
   *
   * @param string $project_source_location
   *   (Optional) Set to a specific source uri that exist in the configuration,
   *   so that the rebuild will only run for this source and skips any other.
   *   Skip this argument to run the rebuild for all known source projects.
   * @param array $options
   *   An associative array of options.
   *
   * @return \Consolidation\OutputFormatters\StructuredData\RowsOfFields
   *   The rows of fields.
   *
   * @option production
   *   Use this flag to run the asset compilation scripts in production mode.
   *   This is a shortcut for --mode=production.
   * @option development
   *   Use this flag to run the asset compilation scripts in development mode.
   *   This is a shortcut for --mode=development.
   * @option mode
   *   The build mode to use. Typically one of 'production' or 'development'.
   *   When no build mode is excplicitly set, then the default chosen build mode
   *   from the entrypoints configuration will be used.
   * @option cleanup
   *   Use this flag to perform a deletion of stale files right after asset
   *   compilation. You can also set a custom threshold by setting
   *   cleanup=<threshold> as a unix timestamp in seconds to only delete files
   *   that are older than the given threshold. By default the threshold is set
   *   to 0.
   * @option update
   *   Use this flag to update the source packages before running the asset
   *   compilation scripts. You can set --update=all to enforce updates on all
   *   project sources, regardless of their 'watch_outdated' setting. Otherwise
   *   this flag only includes project sources that have their 'watch_outdated'
   *   setting set as enabled.
   * @option show-log
   *   Set this flag (yes / no) if log messages should be printed or not.
   * @usage entrypoints:rebuild --production --cleanup=604800 --update
   *   Updates all source projects that have 'watch_outdated' set as enabled,
   *   compiles all assets in production mode and deletes all stale files that
   *   are older than one week.
   * @command entrypoints:rebuild
   * @field-labels
   *   source: Source uri
   *   updated: Source updated
   *   build_mode: Build mode
   *   status: Compilation status
   * @default-fields source,updated,build_mode,status
   * @table-style default
   */
  public function rebuild($project_source_location = NULL, array $options = [
    'production' => NULL,
    'development' => NULL,
    'mode' => NULL,
    'cleanup' => NULL,
    'update' => NULL,
    'show-log' => 'yes',
  ]) {
    $build_mode = $options['mode'];
    if (isset($options['development'])) {
      $build_mode = 'development';
    }
    if (isset($options['production'])) {
      $build_mode = 'production';
    }
    if (empty($build_mode)) {
      $build_mode = static::entrypointsConfig()->get('build_mode');
    }
    if (empty($project_source_location)) {
      $project_source_location = NULL;
    }

    $update_results = [];
    if (isset($options['update'])) {
      $enforce_update_all = strtolower($options['update']) === 'all';
      $update = static::updateService();
      $update->setIo($this->io());
      if (strtolower($options['show-log']) !== 'yes') {
        $update->setMessageLogLevel(RfcLogLevel::ERROR);
      }
      $update_results = $update->updatePackages($project_source_location, $enforce_update_all);
      if (FALSE === $update_results) {
        $this->io()->error("An error occurred when attempting to run updates on source projects. Aborted the rebuild process. Please check the previous log for details.");
        return NULL;
      }
      $updates_success = TRUE;
      foreach ($update_results as $uri => $result) {
        if (!$result) {
          $updates_success = FALSE;
          $this->io()->error(strtr("An error occurred when trying to update the source project at '%uri'.", ['%uri' => $uri]));
        }
      }
      if (!$updates_success) {
        $this->io()->error("Aborted the rebuild process. Please check the previous log for details.");
        return NULL;
      }
    }

    $rebuild = static::rebuildService();
    $rebuild->setIo($this->io());
    if (strtolower($options['show-log']) !== 'yes') {
      $rebuild->setMessageLogLevel(RfcLogLevel::ERROR);
    }
    $results = $rebuild->rebuildEntrypoints($build_mode, $project_source_location);
    if (FALSE === $results) {
      $this->io()->error("An error occurred during rebuild. Please check the previous log for details.");
      return NULL;
    }

    $cleanup = $options['cleanup'];
    if (isset($cleanup)) {
      $threshold = is_numeric($cleanup) ? (int) $cleanup : 0;
      $this->cleanup([
        'threshold' => $threshold,
        'show-log' => $options['show-log'],
      ]);
    }

    $table = [];
    foreach ($results as $uri => $success) {
      $table[] = [
        'source' => $uri,
        'updated' => isset($update_results[$uri]) ? dt('Yes') : dt('No'),
        'build_mode' => $build_mode,
        'status' => $success ? dt('Ok') : dt('Error'),
      ];
    }

    return new RowsOfFields($table);
  }

  /**
   * Removes stale files from the output folders.
   *
   * @param array $options
   *   An associative array of options.
   *
   * @option threshold
   *   Delete stale files that are older than the given threshold, which is a
   *   unix timestamp in seconds. By default the threshold is set to 0.
   * @option show-log
   *   Set this flag (yes / no) if log messages should be printed or not.
   * @usage entrypoints:cleanup --threshold=604800
   *   Deletes all stale files that are older than one week.
   * @command entrypoints:cleanup
   */
  public function cleanup(array $options = [
    'threshold' => 0,
    'show-log' => 'yes',
  ]) {
    $cleanup = static::cleanupService();
    $cleanup->setIo($this->io());
    if (strtolower($options['show-log']) !== 'yes') {
      $cleanup->setMessageLogLevel(RfcLogLevel::ERROR);
    }
    $threshold = (int) $options['threshold'];
    $num_deleted = FALSE;
    try {
      $num_deleted = $cleanup->deleteStaleFiles($threshold, TRUE);
    }
    catch (\Exception $e) {
      $this->io()->error($e->getMessage());
    }
    if (FALSE === $num_deleted) {
      $this->io()->error("An error occurred when trying to cleanup stale files. Please check the previous log for details.");
    }
    if (strtolower($options['show-log']) === 'yes' && $num_deleted === 0) {
      $this->io()->success("Cleanup process completed. No stale files were removed.");
    }
  }

  /**
   * Checks project sources for outdated packages.
   *
   * @param string $project_source_location
   *   (Optional) Set to a specific source uri that exist in the configuration,
   *   so that the check will only run for this source and skips any other.
   *   Skip this argument to check on all known source projects that are enabled
   *   for being checked for outdated packages (i.e. 'watch_outdated' is TRUE).
   * @param array $options
   *   An associative array of options.
   *
   * @return \Consolidation\OutputFormatters\StructuredData\RowsOfFields
   *   The rows of fields.
   *
   * @option enforce-all
   *   Use this flag (yes / no) if you want to ignore existing 'watch_outdated'
   *   settings, and instead enforce to check for outdated packages on all
   *   projects, even if a project has the 'watch_outdated' flag set as
   *   disabled.
   * @option show-log
   *   Set this flag (yes / no) if log messages should be printed or not.
   * @usage entrypoints:outdated
   *   Checks project sources for outdated packages. Includes only projects that
   *   have 'watch_outdated' set as enabled. Use the --enforce-all option to
   *   include any project regardless of its 'watch_outdated' setting.
   * @command entrypoints:outdated
   * @field-labels
   *   source: Source
   *   is_up_to_date: Up to date
   *   outdated_packages: Outdated packages
   * @default-fields source,is_up_to_date,outdated_packages
   * @table-style default
   */
  public function outdated($project_source_location = NULL, array $options = [
    'enforce-all' => 'no',
    'format' => 'table',
    'show-log' => 'yes',
  ]) {
    $enforce_check_all = !empty($project_source_location) || strtolower($options['enforce-all']) === 'yes';
    $project_outdated = static::outdatedCheckService();
    $project_outdated->setIo($this->io());
    if (strtolower($options['show-log']) !== 'yes') {
      $project_outdated->setMessageLogLevel(RfcLogLevel::ERROR);
    }
    if (empty($project_source_location)) {
      $project_source_location = NULL;
    }
    $results = $project_outdated->getOutdatedPackages($project_source_location, $enforce_check_all);
    if (FALSE === $results) {
      $this->io()->error("An error occurred during outdated check. Please check the previous log for details.");
      return NULL;
    }

    $table = [];
    foreach ($results as $uri => $result) {
      if (FALSE === $result) {
        $table[] = [
          'source' => $uri,
          'is_up_to_date' => '?',
          'outdated_packages' => 'An error occurred. Please check log for details.',
        ];
      }
      else {
        $count = count($result);
        $outdated = '';
        if ($count > 0) {
          $subset = array_slice(array_keys($result), 0, 5);
          $outdated = implode(', ', $subset);
          if ($count > 5) {
            $outdated .= strtr('... and %num more', ['%num' => $count - 5]);
          }
        }
        $table[] = [
          'source' => $uri,
          'is_up_to_date' => $count === 0 ? 'yes' : 'no',
          'outdated_packages' => $outdated,
        ];
      }
    }

    return new RowsOfFields($table);
  }

  /**
   * Get the project creation service.
   *
   * @return \Drupal\entrypoints\EntrypointsProjectCreate
   *   The project creation service.
   */
  protected static function projectCreateService() {
    return \Drupal::service('entrypoints.project_create');
  }

  /**
   * Get the project register service.
   *
   * @return \Drupal\entrypoints\EntrypointsProjectRegister
   *   The project register service.
   */
  protected static function projectRegisterService() {
    return \Drupal::service('entrypoints.project_register');
  }

  /**
   * Get the output cleanup service.
   *
   * @return \Drupal\entrypoints\EntrypointsCleanup
   *   The output cleanup service.
   */
  protected static function cleanupService() {
    return \Drupal::service('entrypoints.cleanup');
  }

  /**
   * Get the rebuild service.
   *
   * @return \Drupal\entrypoints\EntrypointsRebuild
   *   The rebuild service.
   */
  protected static function rebuildService() {
    return \Drupal::service('entrypoints.rebuild');
  }

  /**
   * Get the update service.
   *
   * @return \Drupal\entrypoints\EntrypointsProjectUpdate
   *   The update service.
   */
  protected static function updateService() {
    return \Drupal::service('entrypoints.project_update');
  }

  /**
   * Get the check for outdated packages service.
   *
   * @return \Drupal\entrypoints\EntrypointsProjectOutdated
   *   The outdated check service.
   */
  protected static function outdatedCheckService() {
    return \Drupal::service('entrypoints.project_outdated');
  }

  /**
   * Get the editable entrypoints configuration object.
   *
   * @return \Drupal\Core\Config\Config
   *   The editable configuration object.
   */
  protected static function entrypointsConfig() {
    return \Drupal::configFactory()->getEditable('entrypoints.settings');
  }

}

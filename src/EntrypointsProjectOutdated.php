<?php

namespace Drupal\entrypoints;

/**
 * The entrypoints project check outdated service.
 */
class EntrypointsProjectOutdated extends EntrypointsRuntimeTask {

  /**
   * Checks and returns a list of outdated packages per source uri.
   *
   * By default, only project sources that have the 'watch_outdated' setting
   * set as enabled are included. See the parameters below if you want to
   * include all project sources.
   *
   * @param string $source_uri
   *   (Optional) Set to a specific source uri that exist in the configuration,
   *   so that the check will only run for this source and skips any other.
   *   Skip this argument to check on all known source projects that are enabled
   *   for being checked for outdated packages.
   * @param bool $enforce_all
   *   Set to TRUE if you want to ignore existing 'watch_outdated' settings, and
   *   instead enforce to check for outdated packages on all projects, even if a
   *   project has the 'watch_outdated' flag set as disabled.
   *
   * @return array|false
   *   Returns an array grouped by project source uri. Each entry per uri is
   *   either a boolean FALSE on failure or on successful check an array that
   *   lists the outdated packages as keys with outdated info as values. The
   *   outdated info value of each package is an array that consists of
   *   'current', 'wanted' and 'latest' version entries. If a given source uri
   *   is up to date, then its entry is an empty array. Returns FALSE instead of
   *   an array when something is wrong in general, e.g. when no default runtime
   *   is available or a source uri could not be resolved.
   */
  public function getOutdatedPackages($source_uri = NULL, $enforce_all = FALSE) {
    if (!$this->ensureDefaultRuntime()) {
      return FALSE;
    }

    $sources = [];
    if ($projects = $this->config()->get('projects')) {
      foreach ($projects as $project) {
        if (!$enforce_all && empty($project['source']['watch_outdated'])) {
          continue;
        }
        if (!empty($source_uri)) {
          if (empty($project['source']['location']) || ($project['source']['location'] !== $source_uri)) {
            continue;
          }
        }
        elseif (empty($project['source']['location'])) {
          continue;
        }
        try {
          $realpath = $this->localUriResolver->getRealpath($project['source']['location']);
        }
        catch (EntrypointsInvalidUriException $e) {
          $this->log("The entrypoints config contains a source project uri '%uri', but that does not resolve to a local folder. Aborted outdated check.", ['%uri' => $project['source']['location']], 'error');
          return FALSE;
        }
        if (!@file_exists($realpath)) {
          $this->log("The entrypoints config contains a source project that points to '%folder', but that directory does not exist. Aborted outdated check.", ['%folder' => $project['source']['location']], 'error');
          return FALSE;
        }
        $sources[$project['source']['location']] = $realpath;
      }
    }

    if (empty($sources)) {
      if (empty($source_uri)) {
        $this->log("No project sources are known from the entrypoints configuration that could be checked for outdated packages.", [], 'info');
        return [];
      }
      else {
        $this->log("The given source project uri '%uri' is not present in the entrypoints configuration. Aborted outdated check.", ['%uri' => $source_uri], 'error');
        return FALSE;
      }
    }

    $results = [];
    foreach ($sources as $uri => $cwd) {
      $result = $this->runtime->get()->outdated($cwd);

      if (FALSE === $result) {
        $this->log("Something went wrong when trying to check for outdated packages of project source at '%uri'. Make sure the project source exists, is accessible and contains valid project files (e.g. a valid package.json file).", ['%uri' => $uri], 'error');
      }
      elseif (count($result) > 0) {
        $this->log("The entrypoints project source at '%uri' has %num_outdated packages that need to be updated.",
          ['%uri' => $uri, '%num_outdated' => count($result)], 'warning');
      }
      else {
        $this->log("The entrypoints project source at '%uri' got checked for outdated packages. The project source is up to date.", ['%uri' => $uri]);
      }
      $results[$uri] = $result;
    }
    return $results;
  }

}

<?php

namespace Drupal\entrypoints;

use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;

/**
 * Entrypoints local uri resolver service.
 */
class LocalUriResolver {

  /**
   * The app root path.
   *
   * @var string
   */
  protected $appRoot;

  /**
   * The stream wrapper manager.
   *
   * @var Drupal\Core\StreamWrapper\StreamWrapperManagerInterface
   */
  protected $streamWrapperManager;

  /**
   * The LocalUriResolver service constructor.
   *
   * @param \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface $stream_wrapper_manager
   *   The stream wrapper manager.
   */
  public function __construct(StreamWrapperManagerInterface $stream_wrapper_manager) {
    $this->appRoot = _entrypoints_app_root();
    $this->streamWrapperManager = $stream_wrapper_manager;
  }

  /**
   * Get the local absolute path for the given entrypoints source or output uri.
   *
   * @param string $uri
   *   The entrypoints source or output uri.
   *
   * @return string
   *   The realpath as string.
   *
   * @throws \Drupal\entrypoints\EntrypointsInvalidUriException
   *   If the given uri is not valid, e.g. when it's not a local stream wrapper.
   */
  public function getRealpath($uri) {
    if ((strpos($uri, DIRECTORY_SEPARATOR) === 0) || (strpos($uri, ':\\') !== FALSE)) {
      $realpath = $uri;
    }
    elseif (strpos($uri, '://') !== FALSE) {
      if (!($wrapper = $this->streamWrapperManager->getViaUri($uri))) {
        throw new EntrypointsInvalidUriException($uri, "The given entrypoints source or output uri ${uri} does not resolve to a valid stream wrapper.");
      }
      $realpath = $wrapper->realpath();
    }
    elseif (!empty($this->appRoot) && !(strpos($uri, $this->appRoot) === 0)) {
      $realpath = substr($this->appRoot, -strlen(DIRECTORY_SEPARATOR)) === DIRECTORY_SEPARATOR ? $this->appRoot . $uri : $this->appRoot . DIRECTORY_SEPARATOR . $uri;
    }
    if (empty($realpath) || strpos($realpath, '//') !== FALSE) {
      throw new EntrypointsInvalidUriException($uri, "The given entrypoints source or output uri ${uri} does not resolve to a local path.");
    }
    if (DIRECTORY_SEPARATOR !== '/') {
      $realpath = str_replace('/', DIRECTORY_SEPARATOR, $realpath);
    }
    return $realpath;
  }

  /**
   * Get the local relative path from the app root for the given uri.
   *
   * @param string $uri
   *   The entrypoints source or output uri.
   *
   * @return string|false
   *   The relative path as string, or FALSE if not resolvable.
   */
  public function getRelativePath($uri) {
    try {
      $realpath = $this->getRealpath($uri);
    }
    catch (EntrypointsInvalidUriException $e) {
      return FALSE;
    }
    if (strpos($realpath, $this->appRoot) === 0) {
      $relative_path = substr($realpath, strlen($this->appRoot));
      if (strpos($relative_path, DIRECTORY_SEPARATOR) === 0) {
        $relative_path = substr($relative_path, strlen(DIRECTORY_SEPARATOR));
      }
      return $relative_path;
    }
    return FALSE;
  }

}

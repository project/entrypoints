<?php

namespace Drupal\entrypoints;

use Drupal\Component\Serialization\SerializationInterface;

/**
 * Class for retrieving read-only project source information.
 */
class EntrypointsProjectSource {

  /**
   * A list of already loaded sources.
   *
   * @var \Drupal\entrypoints\EntrypointsProjectSource[]
   */
  protected static $loadedSources = [];

  /**
   * The local uri resolver.
   *
   * @var \Drupal\entrypoints\LocalUriResolver
   */
  protected $localUriResolver;

  /**
   * The JSON serialization service, only available at the service instance.
   *
   * @var \Drupal\Component\Serialization\SerializationInterface
   */
  protected $jsonSerialization;

  /**
   * Loads the project source information from the given source uri.
   *
   * This action should only be performed on configuration, not at
   * every site request. It does perform file system operations
   * that might affect performance or even stability on very frequent requests.
   * If you need source-related information frequently, you can work with
   * Entrypoint definitions instead, that you may obtain directly from
   * EntrypointDefinition::getMultiple() or
   * EntrypointDefinition::getBySettings().
   *
   * @return \Drupal\entrypoints\EntrypointsProjectSource
   *   The project source information for the given uri.
   */
  public static function load($uri) {
    if (!isset(static::$loadedSources[$uri])) {
      /** @var \Drupal\entrypoints\EntrypointsProjectSource $service */
      $service = \Drupal::service('entrypoints.source');
      $class = get_class($service);
      static::$loadedSources[$uri] = (new $class())->buildInfo($service, $uri);
    }
    return static::$loadedSources[$uri];
  }

  /**
   * Reloads the project source information for the given source object.
   *
   * @return \Drupal\entrypoints\EntrypointsProjectSource
   *   The same instance that was passed as argument, with reloaded info.
   */
  public static function reload(EntrypointsProjectSource $source) {
    static::$loadedSources[$source->getUri()] = $source;
    return $source->buildInfo(\Drupal::service('entrypoints.source'), $source->getUri());
  }

  /**
   * Get this class as a service instance.
   *
   * Do not call this method directly,
   * use \Drupal::service('entrypoints.source') instead.
   *
   * @param \Drupal\entrypoints\LocalUriResolver $local_uri_resolver
   *   The local uri resolver.
   * @param \Drupal\Component\Serialization\SerializationInterface $json_serialization
   *   The JSON serialization service.
   *
   * @return \Drupal\entrypoints\EntrypointsProjectSource
   *   A service instance of this class.
   */
  public static function service(LocalUriResolver $local_uri_resolver, SerializationInterface $json_serialization) {
    $service = new static();
    $service->localUriResolver = $local_uri_resolver;
    $service->jsonSerialization = $json_serialization;
    return $service;
  }

  /**
   * Wakeup magic method.
   */
  public function __wakeup() {
    if ($this->getUri()) {
      static::$loadedSources[$this->getUri()] = $this;
    }
  }

  /**
   * Empties the internal cache of source information.
   *
   * Might be useful when a non-existing project has been newly created.
   */
  public static function reset() {
    static::$loadedSources = [];
  }

  /**
   * Clears the whole source info cache.
   */
  public function clearCache() {
    static::reset();
  }

  /**
   * The uri to the project folder.
   *
   * @var string
   */
  protected $uri;

  /**
   * The absolute local path to the project folder.
   *
   * @var string
   */
  protected $realpath;

  /**
   * Whether the project folder exists or not.
   *
   * @var bool
   */
  protected $exists;

  /**
   * The project name.
   *
   * @var string
   */
  protected $name;

  /**
   * The project version.
   *
   * @var string
   */
  protected $version;

  /**
   * The available scripts.
   *
   * @var string[]
   */
  protected $scriptsAvailable;

  /**
   * Protected constructor, only instantiate via ::load().
   */
  protected function __construct() {}

  /**
   * Get the project name.
   *
   * @return string
   *   The project name.
   */
  public function getName() {
    return $this->name;
  }

  /**
   * Get the project version.
   *
   * @return string
   *   The project version.
   */
  public function getVersion() {
    return $this->version;
  }

  /**
   * Get the available scripts.
   *
   * @return string[]
   *   The available scripts, with the script as value.
   */
  public function getScriptsAvailable() {
    return array_keys($this->scriptsAvailable);
  }

  /**
   * Get the uri to the project directory.
   *
   * @return string
   *   The uri.
   */
  public function getUri() {
    return $this->uri;
  }

  /**
   * Get the absolute path to the project directory.
   *
   * @return string
   *   The absolute path.
   */
  public function getRealpath() {
    return $this->realpath;
  }

  /**
   * Whether the project folder exists or not.
   *
   * @return bool
   *   Returns TRUE if it exists, FALSE otherwise.
   */
  public function exists() {
    return $this->exists;
  }

  /**
   * Builds the project source information from the given uri.
   *
   * @param \Drupal\entrypoints\EntrypointsProjectSource $service
   *   The service instance of this class.
   * @param string $uri
   *   The uri that points to the project source folder.
   *
   * @return $this
   *   The instance itself.
   *
   * @throws \Drupal\entrypoints\EntrypointsInvalidUriException
   *   If the given uri is not valid, e.g. when it's not a local stream wrapper.
   * @throws \Drupal\entrypoints\EntrypointsInvalidProjectException
   *   If the given uri points to an invalid project, e.g. with a malformed
   *   package.json file.
   */
  protected function buildInfo(EntrypointsProjectSource $service, $uri) {
    $this->setUri($uri);
    $realpath = $service->localUriResolver->getRealpath($uri);
    if (@is_file($realpath)) {
      throw new EntrypointsInvalidUriException($uri, "The given project source uri ${uri} is a file, not a directory.");
    }
    if ($package_json = @file_get_contents($realpath . DIRECTORY_SEPARATOR . 'package.json')) {
      $this->setExists(TRUE);
      $this->setRealpath($realpath);
      $package_json = $service->jsonSerialization->decode($package_json);
      if (isset($package_json['version']) && is_scalar($package_json['version'])) {
        $this->setVersion((string) $package_json['version']);
      }
      if (isset($package_json['name']) && is_scalar($package_json['name'])) {
        $this->setName((string) $package_json['name']);
      }
      if (isset($package_json['scripts']) && is_array($package_json['scripts'])) {
        $scripts = [];
        foreach ($package_json['scripts'] as $script => $command) {
          if (is_string($command)) {
            $scripts[$script] = $command;
          }
        }
        $this->setScriptsAvailable($scripts);
      }
    }
    else {
      $this->setExists(FALSE);
    }

    if ($this->exists() && (empty($package_json) || !isset($this->version) || !isset($this->name) || !isset($this->scriptsAvailable))) {
      throw new EntrypointsInvalidProjectException($uri);
    }

    return $this;
  }

  /**
   * Set the name of the project.
   *
   * @param string $name
   *   The name.
   */
  protected function setName($name) {
    $this->name = $name;
  }

  /**
   * Set the version of the project.
   *
   * @param string $version
   *   The version.
   */
  protected function setVersion($version) {
    $this->version = $version;
  }

  /**
   * Set the available scripts of this project.
   *
   * @param string[] $scripts_available
   *   The available scripts, with the script as key and command as value.
   */
  protected function setScriptsAvailable(array $scripts_available) {
    $this->scriptsAvailable = $scripts_available;
  }

  /**
   * Set the uri to the project directory.
   *
   * @param string $uri
   *   The uri to set.
   */
  protected function setUri($uri) {
    $this->uri = $uri;
  }

  /**
   * Set the absolute path to the project directory.
   *
   * @param string $realpath
   *   The absolute path.
   */
  protected function setRealpath($realpath) {
    $this->realpath = $realpath;
  }

  /**
   * Set whether the project folder exists or not.
   *
   * @param bool $exists
   *   Set to TRUE if exists, FALSE otherwise.
   */
  protected function setExists($exists) {
    $this->exists = $exists;
  }

}

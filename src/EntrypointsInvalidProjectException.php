<?php

namespace Drupal\entrypoints;

/**
 * Entrypoints invalid project exception class.
 */
class EntrypointsInvalidProjectException extends \RuntimeException {

  /**
   * The uri to the invalid project.
   *
   * @var string
   */
  protected $uri;

  /**
   * Constructs an entrypoints invalid project exception.
   *
   * @param string $uri
   *   The uri that points to the invalid project.
   * @param string $message
   *   (optional) The exception message.
   * @param int $code
   *   (optional) The error code.
   * @param \Exception $previous
   *   (optional) The previous exception.
   */
  public function __construct($uri, $message = '', $code = 0, \Exception $previous = NULL) {
    $this->uri = (string) $uri;
    $message = $message ?: "The project that is referenced by '{$this->uri}' is not a valid entrypoints project. It must at least contain a readable valid package.json file that consists of name, version and scripts information.";
    parent::__construct($message, $code, $previous);
  }

}

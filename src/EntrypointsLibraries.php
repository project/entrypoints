<?php

namespace Drupal\entrypoints;

/**
 * Entrypoints libraries service.
 */
class EntrypointsLibraries {

  /**
   * The app root path.
   *
   * @var string
   */
  protected $appRoot;

  /**
   * The entrypoints registry.
   *
   * @var \Drupal\entrypoints\EntrypointsRegistry
   */
  protected $registry;

  /**
   * The algorithm used to calculate file hashes.
   *
   * @var string
   */
  protected static $hashAlgo = 'crc32b';

  /**
   * The entrypoints libraries service constructor.
   *
   * @param \Drupal\entrypoints\EntrypointsRegistry $registry
   *   The entrypoints registry.
   */
  public function __construct(EntrypointsRegistry $registry) {
    $this->appRoot = _entrypoints_app_root();
    $this->registry = $registry;
  }

  /**
   * Builds the Drupal library definitions for all known entrypoints.
   *
   * @return array
   *   The library definitions.
   */
  public function buildLibrariesInfo() {
    $libraries = [];
    $to_merge = [];
    $file_hashes = [];

    foreach ($this->registry->getDefinitions(['*']) as $entrypoint) {
      $settings = $entrypoint->getSettings();
      $settings = !empty($settings['library']) ? $settings['library'] : [];
      $assets = $entrypoint->getAssetFiles();
      $library = [];
      if (!empty($settings['optimization']['merge_chunks'])) {
        $to_merge[] = $entrypoint->getName();
      }
      if (!empty($settings['header'])) {
        $library['header'] = TRUE;
      }
      $library['dependencies'] = !empty($settings['dependencies']) ? array_values($settings['dependencies']) : [];
      foreach (['css', 'js'] as $asset_type) {
        if (!empty($assets[$asset_type])) {
          foreach ($assets[$asset_type] as $file_url => $file_path) {
            if (empty($file_path)) {
              continue;
            }
            $values = [];
            $is_external = FALSE;
            if (!empty($this->appRoot) && strpos($file_path, $this->appRoot) === 0 && !(substr($this->appRoot, -strlen(DIRECTORY_SEPARATOR)) === DIRECTORY_SEPARATOR)) {
              // Drupal core aggregation resolves paths starting with '/' from
              // the app root directory. Thus the absolute file path is being
              // cut off by the app root part here.
              $file_path = str_replace(DIRECTORY_SEPARATOR, '/', substr($file_path, strlen($this->appRoot)));
              // If that did not bring up the expected result, reset to the
              // original value.
              if (!(strpos($file_path, '/') === 0)) {
                $file_path = $assets[$asset_type][$file_url];
              }
            }
            elseif (strpos($file_path, 'http:') === 0 || strpos($file_path, 'https:') === 0) {
              $values['type'] = 'external';
              $values['minified'] = TRUE;
              $is_external = TRUE;
            }
            if (!isset($file_hashes[$file_path])) {
              if (!$is_external && @file_exists($assets[$asset_type][$file_url])) {
                $file_hashes[$file_path] = hash_file(static::$hashAlgo, $assets[$asset_type][$file_url]);
              }
              if (!isset($file_hashes[$file_path])) {
                // When external or not locally resolved,
                // use the filename as fallback for the hash.
                $file_hashes[$file_path] = hash(static::$hashAlgo, $file_path);
              }
            }
            if (empty($settings['optimization']['preprocess'])) {
              $values['preprocess'] = FALSE;
            }
            if (empty($settings['optimization']['minify'])) {
              $values['minified'] = TRUE;
            }
            if ($asset_type === 'js') {
              if (!empty($settings['optimization']['defer'])) {
                $values['attributes']['defer'] = TRUE;
              }
              if (!empty($settings['optimization']['async'])) {
                $values['attributes']['async'] = TRUE;
              }
              $library['js'][$file_path] = $values;
            }
            else {
              $library['css']['theme'][$file_path] = $values;
            }
          }
        }
      }

      $libraries[$entrypoint->getName()] = $library;
    }

    // In case we have more than one entrypoint definition that allows
    // chunk merges, start the complex processing. Otherwise avoid it.
    if (count($to_merge) > 1) {
      // Create the subset of allowed libraries where chunks may be merged.
      // We need to make sure first, that all equivalent file contents are
      // actually using the same file. To accomplish that, we unify all file
      // entries by using the following hash table.
      $subset = [];
      $files_by_hash = array_flip($file_hashes);
      foreach ($to_merge as $name) {
        $library = $libraries[$name];
        if (!empty($library['css'])) {
          foreach ($library['css'] as $css_level => $assets) {
            $reset_assets = [];
            foreach ($assets as $file_path => $asset_settings) {
              $file_hash = $file_hashes[$file_path];
              $reset_assets[$files_by_hash[$file_hash]] = $asset_settings;
            }
            $library['css'][$css_level] = $reset_assets;
          }
        }
        if (!empty($library['js'])) {
          $reset_assets = [];
          foreach ($library['js'] as $file_path => $asset_settings) {
            $file_hash = $file_hashes[$file_path];
            $reset_assets[$files_by_hash[$file_hash]] = $asset_settings;
          }
          $library['js'] = $reset_assets;
        }
        $subset[$name] = $library;
      }

      // Update the definition list by the newly added and changed entries.
      $libraries = array_merge($libraries, $this->doMergeChunks($subset));
    }

    foreach ($libraries as $name => $library) {
      $hashes = '';
      if (!empty($library['css'])) {
        foreach ($library['css'] as $assets) {
          foreach (array_keys($assets) as $file_path) {
            $hashes .= $file_hashes[$file_path];
          }
        }
      }
      if (!empty($library['js'])) {
        foreach (array_keys($library['js']) as $file_path) {
          $hashes .= $file_hashes[$file_path];
        }
      }
      $libraries[$name]['version'] = hash(static::$hashAlgo, $hashes);
    }

    return $libraries;
  }

  /**
   * Helper function to generate the chunk library name.
   *
   * @param string $input
   *   The input to generate the name for.
   *
   * @return string
   *   The generated chunk library name.
   */
  protected function generateChunkName($input) {
    $names = array_filter(array_unique(explode('.', $input)), function ($name) {
      return $name !== 'chunk';
    });
    return 'chunk.' . hash(static::$hashAlgo, implode('.', $names));
  }

  /**
   * Performs the chunk merging operation on the given list of libraries.
   *
   * @param array $libraries
   *   The library definitions.
   *
   * @return array
   *   The updated list of libraries.
   */
  protected function doMergeChunks(array $libraries) {
    // Use the currently built libraries as a baseline for chunk merging.
    // This allows the direct reuse of entrypoints, that are being completely
    // reused in other entrypoint definitions.
    $libraries_before = $libraries;
    foreach ($libraries as $name_subject => $library_subject) {
      foreach ($libraries as $name => $library) {
        if ($name === $name_subject || isset($libraries[$this->generateChunkName($name_subject . '.' . $name)]) || isset($libraries[$this->generateChunkName($name . '.' . $name_subject)])) {
          // Skip checking against the same library,
          // and skip on already (vice-versa) merged chunks.
          continue;
        }
        $common = [];
        if (!empty($library['css']) && !empty($library_subject['css'])) {
          foreach ($library['css'] as $css_level => $assets) {
            if (empty($library_subject['css'][$css_level])) {
              continue;
            }
            $assets_subject = $library_subject['css'][$css_level];
            foreach ($assets as $file_path => $asset_settings) {
              if ($file_path !== key($assets_subject)) {
                break;
              }
              $common['css'][$css_level][$file_path] = $this->mergeAssetSettings($asset_settings, current($assets_subject));
              unset($library['css'][$css_level][$file_path]);
              unset($library_subject['css'][$css_level][$file_path]);
              if (next($assets_subject) === FALSE) {
                break;
              }
            }
          }
        }
        if (!empty($library['js']) && !empty($library_subject['js'])) {
          $assets_subject = $library_subject['js'];
          foreach ($library['js'] as $file_path => $asset_settings) {
            if ($file_path !== key($assets_subject)) {
              break;
            }
            $common['js'][$file_path] = $this->mergeAssetSettings($asset_settings, current($assets_subject));
            unset($library['js'][$file_path]);
            unset($library_subject['js'][$file_path]);
            if (next($assets_subject) === FALSE) {
              break;
            }
          }
        }
        if (!empty($common)) {
          $chunk_name = $this->generateChunkName($name_subject . '.' . $name);
          foreach ($libraries as $existing_name => $existing_chunk) {
            // Header and dependencies are not to be considered here.
            unset($existing_chunk['header'], $existing_chunk['dependencies']);
            if ($existing_chunk == $common) {
              $chunk_name = $existing_name;
              // Use the existing and unchanged library definition.
              $common = $libraries[$existing_name];
              break;
            }
          }
          if (!empty($library['header']) || !empty($library_subject['header'])) {
            $common['header'] = TRUE;
          }
          if (isset($common['dependencies'])) {
            $common['dependencies'] = array_intersect($common['dependencies'], $library['dependencies'], $library_subject['dependencies']);
          }
          else {
            $common['dependencies'] = array_intersect($library['dependencies'], $library_subject['dependencies']);
          }
          $dependency = 'entrypoints/' . $chunk_name;
          if (!in_array($dependency, $library['dependencies'])) {
            $library['dependencies'][] = $dependency;
          }
          if (!in_array($dependency, $library_subject['dependencies'])) {
            $library_subject['dependencies'][] = $dependency;
          }
          $libraries[$name] = $library;
          $libraries[$name_subject] = $library_subject;
          $libraries[$chunk_name] = $common;
        }
      }
    }

    // When the dependency merging does not bring up new items, we are finished.
    // Otherwise, repeat the whole process, until no more merged dependencies
    // are possible. As a final step, filter out redundant dependencies.
    if ($libraries == $libraries_before) {
      return $this->filterDuplicateDependencies($libraries);
    }
    return $this->doMergeChunks($libraries);
  }

  /**
   * Merges the given asset settings.
   *
   * @param array $settings
   *   The first set of asset settings.
   * @param array $settings_subject
   *   The second set of asset settings.
   *
   * @return array
   *   The merges asset settings.
   */
  protected function mergeAssetSettings(array $settings, array $settings_subject) {
    $merged_settings = array_merge($settings, $settings_subject);
    if (isset($merged_settings['minified'])) {
      $merged_settings['minified'] = !empty($settings['minified']) || !empty($settings_subject['minified']);
    }
    if (isset($merged_settings['preprocess'])) {
      $merged_settings['preprocess'] = (isset($settings['preprocess']) && !$settings['preprocess']) || (isset($settings_subject['preprocess']) && !$settings_subject['preprocess']) ? FALSE : TRUE;
    }
    if (isset($merged_settings['attributes'])) {
      if (isset($settings['attributes']) && isset($settings_subject['attributes'])) {
        $merged_settings['attributes'] = array_intersect_assoc($settings['attributes'], $settings_subject['attributes']);
        if (empty($merged_settings['attributes'])) {
          unset($merged_settings['attributes']);
        }
      }
      else {
        unset($merged_settings['attributes']);
      }
    }
    return $merged_settings;
  }

  /**
   * Filters out hierarchically redundant dependency definitions.
   *
   * @param array $libraries
   *   The library definitions.
   *
   * @return array
   *   The updated list of libraries.
   */
  protected function filterDuplicateDependencies(array $libraries) {
    foreach ($libraries as $name => $library) {
      if (empty($library['dependencies'])) {
        continue;
      }
      foreach ($library['dependencies'] as $i => $dependency_subject) {
        foreach ($library['dependencies'] as $dependency) {
          if ($dependency === $dependency_subject || !(strpos($dependency, 'entrypoints/') === 0)) {
            continue;
          }
          if ($this->hasDependency(substr($dependency, strlen('entrypoints/')), $dependency_subject, $libraries)) {
            unset($library['dependencies'][$i]);
          }
        }
      }
      $libraries[$name]['dependencies'] = array_values($library['dependencies']);
    }
    return $libraries;
  }

  /**
   * Recursively checks if the given dependency exists in the dependency tree.
   *
   * @param string $library_name
   *   The library name as current subject.
   * @param string $dependency_name
   *   The dependency to look for.
   * @param array $libraries
   *   The library definitions.
   * @param array $checked
   *   (Optional) A list of already checked library names.
   *
   * @return bool
   *   Returns TRUE if the dependency exists, FALSE otherwise.
   */
  protected function hasDependency($library_name, $dependency_name, array $libraries, array $checked = []) {
    if (empty($libraries[$library_name]['dependencies'])) {
      return FALSE;
    }
    if (in_array($library_name, $checked)) {
      return FALSE;
    }
    $checked[] = $library_name;
    if (in_array($dependency_name, $libraries[$library_name]['dependencies'])) {
      return TRUE;
    }
    foreach ($libraries[$library_name]['dependencies'] as $dependency) {
      if (!(strpos($dependency, 'entrypoints/') === 0)) {
        continue;
      }
      if ($this->hasDependency(substr($dependency, strlen('entrypoints/')), $dependency_name, $libraries, $checked)) {
        return TRUE;
      }
    }
    return FALSE;
  }

}

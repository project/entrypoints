<?php

namespace Drupal\entrypoints;

/**
 * Helper class for listing available manifest entry methods.
 */
abstract class ManifestEntry {

  /**
   * Get a list of manifest.json entries by the given entry keys.
   *
   * @param string[] $keys
   *   The entry keys. Leave empty to get all existing entries.
   *   In many occasions, the manifest keys include the base directory of the
   *   output folders (which is the value of the 'entrypoints.output_basedir'
   *   service parameter). For those entries, the base directory part does not
   *   need to be included. For example, when 'entrypoints.output_basedir' is
   *   set to be 'sites/all/assets', and an entry exists in the manifest.json
   *   with a key 'sites/all/assets/entry/index.js', then it can be obtained by
   *   setting the $keys argument as ['entry/index.js'].
   *
   * @return array
   *   A list of manifest entries, keyed by the original manifest keys (which
   *   occasionally includes the 'entrypoints.output_basedir' parameter as a
   *   prefix). Each entry is an array that is keyed by 'url' and 'file' string
   *   values. The 'url' might be absolute or relative. The 'file' value might
   *   be an absolute local path or an external url. If a given key does not
   *   exist, no entry value exists for it in the returned array.
   */
  public static function getMultiple(array $keys = []) {
    return \Drupal::service('entrypoints.registry')->getManifestEntries($keys);
  }

  /**
   * Get a single manifest.json entry.
   *
   * @param string $key
   *   The entry key.
   *   In many occasions, the manifest keys include the base directory of the
   *   output folders (which is the value of the 'entrypoints.output_basedir'
   *   service parameter). For those entries, the base directory part does not
   *   need to be included. For example, when 'entrypoints.output_basedir' is
   *   set to be 'sites/all/assets', and an entry exists in the manifest.json
   *   with a key 'sites/all/assets/entry/index.js', then it can be obtained by
   *   setting the $key argument as 'entry/index.js'.
   *
   * @return array|false
   *   If exists, the entry as array with 'url' and 'file' key.
   *   The 'url' might be absolute or relative. The 'file' value might be
   *   an absolute local path or an external url. If it does not exists,
   *   the method returns the boolean value FALSE.
   */
  public static function get($key) {
    return \Drupal::service('entrypoints.registry')->getManifestEntry($key);
  }

  /**
   * Checks if a single manifest.json entry exists.
   *
   * @param string $key
   *   The entry key to check for.
   *
   * @return bool
   *   Returns TRUE if the entry exists, FALSE otherwise.
   */
  public static function exists($key) {
    return \Drupal::service('entrypoints.registry')->hasManifestEntry($key);
  }

  /**
   * Get a list of file content of manifest.json entries by given entry keys.
   *
   * @param string[] $keys
   *   The entry keys. Leave empty to get all existing entries.
   *   In many occasions, the manifest keys include the base directory of the
   *   output folders (which is the value of the 'entrypoints.output_basedir'
   *   service parameter). For those entries, the base directory part does not
   *   need to be included. For example, when 'entrypoints.output_basedir' is
   *   set to be 'sites/all/assets', and an entry exists in the manifest.json
   *   with a key 'sites/all/assets/entry/index.js', then it can be obtained by
   *   setting the $keys argument as ['entry/index.js'].
   * @param bool $use_cache
   *   (Optional) Set to TRUE if the file contents should be cached once loaded
   *   or not. Caching is enabled by default.
   *
   * @return array
   *   An array of file contents, keyed by the manifest keys. The file content
   *   value might be a string or FALSE on failure when reading the file.
   *   If the manifest key itself does not exist, no value exists for the key in
   *   the returned array.
   */
  public static function getContentMultiple(array $keys = [], $use_cache = TRUE) {
    return \Drupal::service('entrypoints.registry')->getManifestEntriesContent($keys, $use_cache);
  }

  /**
   * Get the file content of a manifest.json entry.
   *
   * @param string $key
   *   The entry key.
   *   In many occasions, the manifest keys include the base directory of the
   *   output folders (which is the value of the 'entrypoints.output_basedir'
   *   service parameter). For those entries, the base directory part does not
   *   need to be included. For example, when 'entrypoints.output_basedir' is
   *   set to be 'sites/all/assets', and an entry exists in the manifest.json
   *   with a key 'sites/all/assets/entry/index.js', then it can be obtained by
   *   setting the $key argument as 'entry/index.js'.
   * @param bool $use_cache
   *   (Optional) Set to TRUE if the file content should be cached once loaded
   *   or not. Caching is enabled by default.
   *
   * @return string|false
   *   If exists, the file content of the manifest entry.
   *   Returns FALSE if it does not exist or if there was a failure on reading
   *   the file.
   */
  public static function getContent($key, $use_cache = TRUE) {
    return \Drupal::service('entrypoints.registry')->getManifestEntryContent($key, $use_cache);
  }

}

<?php

namespace Drupal\entrypoints;

/**
 * Entrypoints rebuild service.
 */
class EntrypointsRebuild extends EntrypointsRuntimeTask {

  /**
   * Rebuilds all entrypoints by running their asset compilation scripts.
   *
   * Before rebuilding entrypoints, consider updating the source projects first
   * by using the 'entrypoints.project_update' service. When the rebuild is
   * finished, consider a cleanup of old stale files by using the
   * 'entrypoints.cleanup' service.
   *
   * @param string $build_mode
   *   The build mode to use. Skip this argument to use the default assigned
   *   build mode from the entrypoints configuration.
   * @param string $source_uri
   *   (Optional) Set to a specific source uri that exist in the configuration,
   *   so that the rebuild will only run for this source and skips any other.
   *   Skip this argument to run the rebuild for all known source projects.
   *
   * @return array|false
   *   Returns an array keyed by project source uri. Each entry is a boolean
   *   value TRUE on successful rebuild, FALSE otherwise. Returns FALSE instead
   *   of an array when something is wrong in general, e.g. when no default
   *   runtime is available or a source uri could not be resolved.
   */
  public function rebuildEntrypoints($build_mode = NULL, $source_uri = NULL) {
    if (!$this->ensureDefaultRuntime()) {
      return FALSE;
    }

    $config_build_mode = $this->config()->get('build_mode');
    $build_modes = $this->runtime->getBuildModesAvailable();

    if (empty($build_mode) && !($build_mode = $config_build_mode)) {
      $this->log("No build mode is assigned as default in the entrypoints configuration. Assign a default build mode at /admin/config/entrypoints. Aborted entrypoints rebuild.", [], 'error');
      return FALSE;
    }
    elseif (!isset($build_modes[$build_mode])) {
      $this->log("The requested build mode '%mode' is not available. Aborted entrypoints rebuild.", ['%mode' => $build_mode], 'error');
      return FALSE;
    }
    elseif ($config_build_mode && $build_mode != $config_build_mode) {
      $this->log("The requested build mode '%mode' differs from the default build mode of the entrypoints configuration. Your recent build might be ignored in the frontend, because the Drupal library will use the %default asset builds. Change the build mode in the configuration accordingly (and clear cache). Please make sure that you will not use the wrong builds in your non-development environments.",
      [
        '%mode' => isset($build_modes[$build_mode]) ? $build_modes[$build_mode] : $build_mode,
        '%default' => isset($build_modes[$config_build_mode]) ? $build_modes[$config_build_mode] : $config_build_mode,
      ], 'warning');
    }

    $sources = [];
    if ($projects = $this->config()->get('projects')) {
      foreach ($projects as $project) {
        if (!empty($source_uri)) {
          if (empty($project['source']['location']) || ($project['source']['location'] !== $source_uri)) {
            continue;
          }
        }
        elseif (empty($project['source']['location'])) {
          if (!empty($project['build'][$build_mode]['location'])) {
            $this->log("The entrypoints config contains a project with a %mode output folder pointing at '%folder', but has no source location defined. Skipping the rebuild for this project.",
            [
              '%mode' => isset($build_modes[$build_mode]) ? $build_modes[$build_mode] : $build_mode,
              '%folder' => $project['build'][$build_mode]['location'],
            ]);
          }
          if (!empty($project['build'][$build_mode]['location_ssr'])) {
            $this->log(
              "The entrypoints config contains a project with a %mode SSR output folder pointing at '%folder', but has no source location defined. Skipping the rebuild for this project.",
              [
                '%mode' => isset($build_modes[$build_mode]) ? $build_modes[$build_mode] : $build_mode,
                '%folder' => $project['build'][$build_mode]['location_ssr'],
              ]
            );
          }
          continue;
        }
        try {
          $realpath = $this->localUriResolver->getRealpath($project['source']['location']);
        }
        catch (EntrypointsInvalidUriException $e) {
          $this->log("The entrypoints config contains a source project uri '%uri', but that does not resolve to a local folder. Aborted entrypoints rebuild.", ['%uri' => $project['source']['location']], 'error');
          return FALSE;
        }
        if (!@file_exists($realpath)) {
          $this->log("The entrypoints config contains a source project that points to '%folder', but that directory does not exist. Aborted entrypoints rebuild.", ['%folder' => $project['source']['location']], 'error');
          return FALSE;
        }
        if (empty($project['build'][$build_mode]['script'])) {
          $this->log("The entrypoints config contains a source project at '%uri', but it does not have a build script assigned for build mode %mode. Skipping the rebuild for this project.",
            [
              '%uri' => $project['source']['location'],
              '%mode' => isset($build_modes[$build_mode]) ? $build_modes[$build_mode] : $build_mode,
            ], 'notice');
          continue;
        }
        $sources[$project['source']['location']] = [
          'cwd' => $realpath,
          'script' => $project['build'][$build_mode]['script'],
        ];
      }
    }
    if (empty($sources)) {
      if (empty($source_uri)) {
        $this->log("No project source locations that can be rebuild in %mode mode are present in the entrypoints configuration.",
        [
          '%mode' => isset($build_modes[$build_mode]) ? $build_modes[$build_mode] : $build_mode,
        ], 'notice');
        return [];
      }
      else {
        $this->log("The given source project uri '%uri' is not present in the entrypoints configuration or cannot be rebuild in %mode mode.",
        [
          '%uri' => $source_uri,
          '%mode' => isset($build_modes[$build_mode]) ? $build_modes[$build_mode] : $build_mode,
        ], 'error');
        return FALSE;
      }
    }

    $results = [];
    foreach ($sources as $uri => $source) {
      $output = $this->runtime->get()->script($source['cwd'], $source['script']);
      if (FALSE === $output) {
        $this->log("Something went wrong when running the build script '%script' for source project at '%realpath'. Make sure the configured build script is available, executable and does not produce errors.",
        [
          '%script' => $source['script'],
          '%realpath' => $source['cwd'],
        ], 'error');
        $results[$uri] = FALSE;
      }
      else {
        $this->log($output);
        $results[$uri] = TRUE;
      }
    }

    static::clearCaches();
    static::ensureProtection();
    $this->log("Entrypoints rebuild completed. To make sure that your whole site takes effect of your most recent builds, consider running a complete cache rebuild.");
    return $results;
  }

  /**
   * Clears relevant caches regards rebuilt entrypoints.
   */
  protected static function clearCaches() {
    entrypoints_cache_flush();
    \Drupal::service('library.discovery')->clearCachedDefinitions();
  }

  /**
   * Ensures protection on SSR output directories.
   */
  protected static function ensureProtection() {
    \Drupal::service('entrypoints.output_protection')->ensure();
  }

}

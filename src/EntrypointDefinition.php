<?php

namespace Drupal\entrypoints;

/**
 * Class for entrypoint definitions.
 */
class EntrypointDefinition {

  /**
   * The definition values as plain array.
   *
   * @var array
   */
  protected $values;

  /**
   * Get a list of Entrypoint definitions.
   *
   * @param string[] $entrypoints
   *   An array of entrypoint names. Wildcards are allowed by using the
   *   star (*) symbol. Example: 'navbar.*' would load all definitions whose
   *   name begin with 'navbar.'. You can use an empty array or the (*) alone
   *   to load all existing entrypoint instances.
   * @param bool $log_failures
   *   (Optional) Whether failures should be logged as errors, when entrypoint
   *   names exist but whose definition could not be loaded. Please note that
   *   this method does not throw an exception when certain entrypoint names
   *   could not be loaded. Default is set to TRUE.
   *
   * @return \Drupal\entrypoints\EntrypointDefinition[]
   *   The entrypoint instances that match the given criteria.
   */
  public static function getMultiple(array $entrypoints = [], $log_failures = TRUE) {
    return \Drupal::service('entrypoints.registry')->getDefinitions($entrypoints, $log_failures);
  }

  /**
   * Get a list of Entrypoint definitions that match the given settings.
   *
   * @param array $settings
   *   The subset of settings whose values should match. For example,
   *   ['header' => TRUE, 'optimization' => ['preprocess' => FALSE]]
   *   is a settings subset that would match to all existing entrypoints
   *   that are supposed to be included in the header, and not included
   *   in Drupal's internal asset aggregation. See the 'entrypoints.schema.yml'
   *   for available settings within the 'entrypoints_settings' type definition.
   *
   * @return \Drupal\entrypoints\EntrypointDefinition[]
   *   The entrypoint instances that match the given criteria.
   */
  public static function getBySettings(array $settings) {
    return \Drupal::service('entrypoints.registry')->getDefinitionsBySettings($settings);
  }

  /**
   * The Entrypoint definition constructor.
   *
   * @param array $values
   *   The definition values as plain array.
   */
  public function __construct(array $values) {
    $this->values = $values;
  }

  /**
   * Returns the plain array representation of this object.
   *
   * @return array
   *   The plain array.
   */
  public function toArray() {
    return $this->values;
  }

  /**
   * Get the entrypoint name.
   *
   * @return string
   *   The entrypoint name.
   */
  public function getName() {
    return $this->values['name'];
  }

  /**
   * Get the Drupal library name.
   *
   * @return string
   *   The Drupal library name.
   */
  public function getLibraryName() {
    return $this->values['library_name'];
  }

  /**
   * Get the build mode used for this entrypoint.
   *
   * In general, all entrypoints would be compiled with the same build mode.
   * This method exists for convenience, and possible future extents.
   *
   * @return string
   *   The used build mode.
   */
  public function getBuildMode() {
    return $this->values['build_mode'];
  }

  /**
   * Get the settings inherited from the entrypoints configuration.
   *
   * @return array
   *   The settings as array.
   */
  public function getSettings() {
    return $this->values['settings'];
  }

  /**
   * Whether the entrypoint has an existing source project folder.
   *
   * @return bool|null
   *   Returns TRUE if it has a configured source project, FALSE if the source
   *   is defined as configuration but has no existing folder, NULL if no
   *   source is defined via configuration.
   */
  public function hasSource() {
    return $this->values['source']['exists'];
  }

  /**
   * Get the source project name, if set.
   *
   * @return string|null
   *   The source project name as string, if set.
   */
  public function getSourceName() {
    return $this->values['source']['name'];
  }

  /**
   * Get the source project version, if set.
   *
   * @return string|null
   *   The source project version as string, if set.
   */
  public function getSourceVersion() {
    return $this->values['source']['version'];
  }

  /**
   * Get the source location uri, if set.
   *
   * @return string|null
   *   The source location, if set.
   */
  public function getSourceLocation() {
    return $this->values['source']['location'] ?: NULL;
  }

  /**
   * Get the flag whether the source is being watched for outdated packages.
   *
   * @return bool|null
   *   The flag value, if set.
   */
  public function getWatchOutdated() {
    return $this->values['source']['watch_outdated'];
  }

  /**
   * Get the whole available source information as plain array.
   *
   * @return array
   *   The source information as array.
   */
  public function getSourceArray() {
    return $this->values['source'];
  }

  /**
   * Get the output location uri.
   *
   * @return string
   *   The output location uri.
   */
  public function getOutputLocation() {
    return $this->values['location'];
  }

  /**
   * Get the server-side rendering (SSR) output location uri.
   *
   * @return string
   *   The SSR output location uri.
   */
  public function getSsrOutputLocation() {
    return $this->values['location_ssr'];
  }

  /**
   * Get the assigned build script for asset compilation, if set.
   *
   * @return string|null
   *   The build script, if set.
   */
  public function getBuildScript() {
    return $this->values['script'];
  }

  /**
   * Get the asset files.
   *
   * @param string $key
   *   (Optional) If you only want to retrieve a subset of certain files,
   *   use this argument to be set to either 'css', 'js' or 'ssr'.
   *
   * @return array
   *   An array of asset files, grouped by 'css', 'js' and optionally 'ssr'
   *   assets. The 'ssr' assets are Javascript files for server-side rendering.
   *   The subset array is returned if the $key argument is set. Each group is
   *   keyed and ordered by the original url definition. Each value within each
   *   group is either the absolute local path to the asset file, or an external
   *   absolute url.
   */
  public function getAssetFiles($key = NULL) {
    if (isset($key)) {
      return isset($this->values['assets'][$key]) ? $this->values['assets'][$key] : [];
    }
    return $this->values['assets'];
  }

  /**
   * Get the server-side rendering definition for this entrypoint.
   *
   * @return \Drupal\entrypoints\EntrypointDefinition|null
   *   The SSR definition, or NULL if this entrypoint does not have any SSR.
   */
  public function getSsrDefinition() {
    $name = $this->values['name'];
    if (!empty($this->values['settings']['ssr']['entrypoint'])) {
      $name = $this->values['settings']['ssr']['entrypoint'];
    }
    if ($name === $this->values['name']) {
      return empty($this->values['assets']['ssr']) ? NULL : $this;
    }
    if ($definition = static::getMultiple([$name])) {
      $definition = reset($definition);
      return $definition->getSsrDefinition();
    }
    return NULL;
  }

}

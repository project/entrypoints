<?php

namespace Drupal\entrypoints\EventSubscriber;

use Drupal\Core\Render\HtmlResponse;
use Drupal\entrypoints\Render\HtmlResponseSsrEntrypointsProcessor;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * The event subscriber that includes server-side rendering entrypoints.
 */
class HtmlResponseSsrEntrypointsSubscriber implements EventSubscriberInterface {

  /**
   * The server-side rendering processor for entrypoint libraries.
   *
   * @var \Drupal\entrypoints\Render\HtmlResponseSsrEntrypointsProcessor
   */
  protected $ssrEntrypointsProcessor;

  /**
   * The event subscriber constructor.
   *
   * @param \Drupal\entrypoints\Render\HtmlResponseSsrEntrypointsProcessor $ssr_entrypoints_processor
   *   The server-side rendering processor for entrypoint libraries.
   */
  public function __construct(HtmlResponseSsrEntrypointsProcessor $ssr_entrypoints_processor) {
    $this->ssrEntrypointsProcessor = $ssr_entrypoints_processor;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    // Run after any main content processing is done, but before a Big Pipe
    // response might be created. Some server-side rendering results are
    // expected to be 100% unchanged, e.g. when a React component is being
    // pre-rendered and then being hydrated by its client counterpart.
    $events[KernelEvents::RESPONSE][] = ['onRespond', -2000];
    return $events;
  }

  /**
   * Executes SSR entrypoints and updates the response content accordingly.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   The event to process.
   */
  public function onRespond($event) {
    $response = $event->getResponse();
    if (!($response instanceof HtmlResponse)) {
      return;
    }
    $event->setResponse($this->ssrEntrypointsProcessor->processSsrEntrypoints($response));
  }

}

<?php

namespace Drupal\entrypoints\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Annotation for entrypoints input handler plugins.
 *
 * @see plugin_api
 *
 * @Annotation
 */
class EntrypointsInputHandler extends Plugin {

  /**
   * The handler plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the handler plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * The priority (higher numbers mean earlier execution).
   *
   * @var int
   */
  public $priority;

}

<?php

namespace Drupal\entrypoints\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Annotation for entrypoint runtime plugins.
 *
 * @see plugin_api
 *
 * @Annotation
 */
class EntrypointsRuntime extends Plugin {

  /**
   * The runtime plugin ID that equals the alias of the executable binary.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the runtime plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}

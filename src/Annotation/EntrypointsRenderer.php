<?php

namespace Drupal\entrypoints\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Annotation for entrypoint server-side renderer plugins.
 *
 * @see plugin_api
 *
 * @Annotation
 */
class EntrypointsRenderer extends Plugin {

  /**
   * The renderer plugin ID that equals the alias of the executable binary.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the renderer plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}

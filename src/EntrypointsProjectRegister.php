<?php

namespace Drupal\entrypoints;

use Drupal\Component\FileSecurity\FileSecurity;

/**
 * The entrypoints project register service.
 */
class EntrypointsProjectRegister extends EntrypointsRuntimeTask {

  use OutputDirectoryTrait;

  /**
   * Adds an existing Webpack Encore project to the entrypoints configuration.
   *
   * @param string $source_uri
   *   (Optional) The absolute or relative path from Drupal web root to the
   *   project source location. Skip this argument if there is no existing
   *   source folder on this environment.
   * @param string $output_uri
   *   (Optional) The production asset build folder. Must be either a folder
   *   name to be created under 'entrypoints.output_basedir', or a uri that
   *   resolves locally to a sub-folder of 'entrypoints.output_basedir'.
   * @param string $output_dev_uri
   *   (Optional) The development asset build folder.
   * @param string $build_script
   *   (Optional) The name of the script for compiling production asset builds.
   *   By default, the build script name is set to 'production'. Will only be
   *   used if the output-folder option is set with a given value. Make sure
   *   that the script key exists in the package.json file of the source
   *   project.
   * @param string $build_dev_script
   *   (Optional) The name of the script for compiling development asset builds.
   *   By default, the build script name is set to 'development'. Will only be
   *   used if the output-dev-folder option is set with a given value. Make sure
   *   that the script key exists in the package.json file of the source
   *   project.
   * @param bool $watch_outdated
   *   (Optional) Whether the project should be watched by Drupal status report
   *   for having outdated packages. Enabled by default.
   * @param string $output_uri_ssr
   *   (Optional) The production build folder of assets to be rendered on the
   *   server-side (SSR). Must be either a folder name to be created under
   *   'entrypoints.output_basedir', or a uri that resolves locally to a
   *   sub-folder of 'entrypoints.output_basedir'.
   * @param string $output_dev_uri_ssr
   *   (Optional) The development build folder of assets to be rendered on the
   *   server-side (SSR).
   *
   * @return bool
   *   Returns TRUE on success, FALSE on failure.
   */
  public function register($source_uri = NULL, $output_uri = NULL, $output_dev_uri = NULL, $build_script = 'production', $build_dev_script = 'development', $watch_outdated = TRUE, $output_uri_ssr = NULL, $output_dev_uri_ssr = NULL) {
    if (!$this->ensureDefaultRuntime()) {
      return FALSE;
    }

    if (empty($output_uri) && empty($output_dev_uri) && empty($output_uri_ssr) && empty($output_dev_uri_ssr)) {
      $this->log("Neither a production output folder nor a development output folder is defined. At least one output folder for client or server-side rendering must be defined. Aborted project registration.", [], 'error');
      return FALSE;
    }
    if ((!empty($output_uri) && $output_uri === $output_dev_uri) || (!empty($output_uri_ssr) && $output_uri_ssr === $output_dev_uri_ssr)) {
      $this->log("Using the same output folder for both production and development is not recommended. Please reconsider this decision. Using different output folders for each build mode can reduce the risk of using wrong asset builds in non-development environments.", [], 'warning');
    }

    if (!empty($output_uri)) {
      $output_uri = $this->normalizeOutputUri($output_uri);
      if (strpos($output_uri, $this->outputBasedir) === FALSE) {
        $this->log("The given output directory is not a child directory of '%basedir'. Aborted project registration.", ['%basedir' => $this->outputBasedir], 'error');
        return FALSE;
      }
    }
    if (!empty($output_dev_uri)) {
      $output_dev_uri = $this->normalizeOutputUri($output_dev_uri);
      if (strpos($output_dev_uri, $this->outputBasedir) === FALSE) {
        $this->log("The given development output directory is not a child directory of '%basedir'. Aborted project registration.", ['%basedir' => $this->outputBasedir], 'error');
        return FALSE;
      }
    }
    if (!empty($output_uri_ssr)) {
      $output_uri_ssr = $this->normalizeOutputUri($output_uri_ssr);
      if (strpos($output_uri_ssr, $this->outputBasedir) === FALSE) {
        $this->log("The given SSR output directory is not a child directory of '%basedir'. Aborted project registration.", ['%basedir' => $this->outputBasedir], 'error');
        return FALSE;
      }
      if (!FileSecurity::writeHtaccess($this->localUriResolver->getRealpath($output_uri_ssr))) {
        $this->log("Could not write .htaccess file into the SSR output directory '%directory'. Aborted project registration.", ['%directory' => $this->localUriResolver->getRealpath($output_uri_ssr)], 'error');
        return FALSE;
      }
    }
    if (!empty($output_dev_uri_ssr)) {
      $output_dev_uri_ssr = $this->normalizeOutputUri($output_dev_uri_ssr);
      if (strpos($output_dev_uri_ssr, $this->outputBasedir) === FALSE) {
        $this->log("The given development SSR output directory is not a child directory of '%basedir'. Aborted project registration.", ['%basedir' => $this->outputBasedir], 'error');
        return FALSE;
      }
      if (!FileSecurity::writeHtaccess($this->localUriResolver->getRealpath($output_dev_uri_ssr))) {
        $this->log("Could not write .htaccess file into the development SSR output directory '%directory'. Aborted project registration.", ['%directory' => $this->localUriResolver->getRealpath($output_dev_uri_ssr)], 'error');
        return FALSE;
      }
    }

    $config = $this->config();
    $projects = $config->get('projects');

    if (!empty($projects)) {
      foreach ($projects as $project) {
        if (!empty($source_uri) && !empty($project['source']['location']) && $source_uri === $project['source']['location']) {
          $this->log("The given source uri '%uri' already exists in the entrypoints configuration. Aborted project registration.", ['%uri' => $source_uri], 'error');
          return FALSE;
        }
        if (!empty($project['build'])) {
          foreach ($project['build'] as $build_info) {
            if (!empty($output_uri) && ((!empty($build_info['location']) && $output_uri === $build_info['location']) || (!empty($build_info['location_ssr']) && $output_uri === $build_info['location_ssr']))) {
              $this->log("The given output uri '%uri' already exists in the entrypoints configuration. Aborted project registration.", ['%uri' => $output_uri], 'error');
              return FALSE;
            }
            if (!empty($output_dev_uri) && ((!empty($build_info['location']) && $output_dev_uri === $build_info['location']) || (!empty($build_info['location_ssr']) && $output_dev_uri === $build_info['location_ssr']))) {
              $this->log("The given development output uri '%uri' already exists in the entrypoints configuration. Aborted project registration.", ['%uri' => $output_dev_uri], 'error');
              return FALSE;
            }
            if (!empty($output_uri_ssr) && ((!empty($build_info['location']) && $output_uri_ssr === $build_info['location']) || (!empty($build_info['location_ssr']) && $output_uri_ssr === $build_info['location_ssr']))) {
              $this->log("The given SSR output uri '%uri' already exists in the entrypoints configuration. Aborted project registration.", ['%uri' => $output_uri_ssr], 'error');
              return FALSE;
            }
            if (!empty($output_dev_uri_ssr) && ((!empty($build_info['location']) && $output_dev_uri_ssr === $build_info['location']) || (!empty($build_info['location_ssr']) && $output_dev_uri_ssr === $build_info['location_ssr']))) {
              $this->log("The given development SSR output uri '%uri' already exists in the entrypoints configuration. Aborted project registration.", ['%uri' => $output_dev_uri_ssr], 'error');
              return FALSE;
            }
          }
        }
      }
    }

    if (!empty($source_uri)) {
      try {
        $source = EntrypointsProjectSource::load($source_uri);
      }
      catch (\Exception $e) {
        $this->log("An error occurred when trying to load information from the project source location. Aborted project registration. The error was: %error", ['%error' => $e->getMessage()], 'error');
        return FALSE;
      }
      if (!$source->exists()) {
        $this->log("The given source uri '%uri' does not resolve to an existing or valid project folder.", ['%uri' => $source_uri], 'error');
        return FALSE;
      }

      $scripts_available = $source->getScriptsAvailable();
      if (!empty($output_uri) && !empty($build_script) && !in_array($build_script, $scripts_available)) {
        $this->log("The source project does not have a '%name' script defined. Make sure the script name is available as a key in the 'scripts' list of the package.json file.", ['%name' => $build_script], 'error');
        return FALSE;
      }
      if (!empty($output_dev_uri) && !empty($build_dev_script) && !in_array($build_dev_script, $scripts_available)) {
        $this->log("The source project does not have a '%name' script defined. Make sure the script name is available as a key in the 'scripts' list of the package.json file.", ['%name' => $build_script], 'error');
        return FALSE;
      }
    }

    $build_modes = $this->runtime->getBuildModesAvailable();
    if (empty($build_modes['production']) && !empty($output_uri)) {
      $this->log("A %mode output uri '%folder' has been defined, but the %mode build mode is not available on this environment. It will be saved in configuration, but you need to enable the build mode if you want to use it.",
        ['%mode' => 'production', '%folder' => $output_uri], 'warning');
    }
    if (empty($build_modes['development']) && !empty($output_dev_uri)) {
      $this->log("A %mode output uri '%folder' has been defined, but the %mode build mode is not available on this environment. It will be saved in configuration, but you need to enable the build mode if you want to use it.",
        ['%mode' => 'development', '%folder' => $output_dev_uri], 'warning');
    }

    $new_project = [
      'source' => [
        'location' => !empty($source_uri) ? $source_uri : NULL,
        'watch_outdated' => !empty($source_uri) && $watch_outdated,
      ],
      'build' => [],
    ];
    if (!empty($output_uri)) {
      $new_project['build']['production'] = [
        'location' => $output_uri,
        'location_ssr' => $output_uri_ssr,
        'script' => !empty($source_uri) ? $build_script : NULL,
      ];
    }
    if (!empty($output_dev_uri)) {
      $new_project['build']['development'] = [
        'location' => $output_dev_uri,
        'location_ssr' => $output_dev_uri_ssr,
        'script' => !empty($source_uri) ? $build_dev_script : NULL,
      ];
    }
    foreach (array_keys($build_modes) as $build_mode) {
      if (!isset($new_project['build'][$build_mode])) {
        $new_project['build'][$build_mode] = [
          'location' => NULL,
          'location_ssr' => NULL,
          'script' => NULL,
        ];
      }
    }

    $projects[] = $new_project;
    $config->set('projects', $projects);
    $config->save();

    $this->log("Added the project to the entrypoints configuration.");
    return TRUE;
  }

  /**
   * Normalize the given output uri to a relative path from the app root.
   *
   * @param string $output_uri
   *   The output uri that needs to be normalized. Might be a single folder
   *   name or an absolute path.
   *
   * @return string
   *   The relative path to the output folder from the app root.
   */
  protected function normalizeOutputUri($output_uri) {
    $normalized = $this->outputFolderTransformRelative($output_uri);
    if ($output_uri != $normalized) {
      $this->log("The output folder '%folder' will be located in '%directory'. Please make sure that this folder exists, is accessible and contains valid asset compilation builds (including entrypoints.json and manifest.json files).",
      [
        '%folder' => $output_uri,
        '%directory' => $this->appRoot . DIRECTORY_SEPARATOR . $normalized,
      ]);
    }
    return $normalized;
  }

}

<?php

namespace Drupal\entrypoints\Form;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\MessageCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\entrypoints\EntrypointsOutputScan;
use Drupal\entrypoints\EntrypointsProjectSource;
use Drupal\entrypoints\EntrypointsRendererNegotiator;
use Drupal\entrypoints\EntrypointsRuntimeNegotiator;
use Drupal\entrypoints\OutputDirectoryTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The entrypoints settings form.
 */
class EntrypointsSettingsForm extends ConfigFormBase {

  use OutputDirectoryTrait;

  /**
   * The configuration ID.
   *
   * @var string
   */
  protected static $configId = 'entrypoints.settings';

  /**
   * The form ID.
   *
   * @var string
   */
  protected static $formId = 'entrypoints_settings';

  /**
   * The HTML ID for wrapping the form.
   *
   * @var string
   */
  protected static $formWrapperId = 'entrypoints-settings-form-wrapper';

  /**
   * The runtime negotiator.
   *
   * @var \Drupal\entrypoints\EntrypointsRuntimeNegotiator
   */
  protected $runtimeNegotiator;

  /**
   * The server-side renderer negotiator.
   *
   * @var \Drupal\entrypoints\EntrypointsRendererNegotiator
   */
  protected $rendererNegotiator;

  /**
   * The entrypoints output scan service.
   *
   * @var \Drupal\entrypoints\EntrypointsOutputScan
   */
  protected $outputScan;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The internal in-memory cache.
   *
   * @var array
   */
  protected $cache = [];

  /**
   * All known runtime plugin IDs.
   *
   * @var string[]
   */
  protected $allRuntimes = [];

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /** @var \Drupal\entrypoints\Form\EntrypointsSettingsForm $instance */
    $instance = parent::create($container);
    $instance->setOutputBasedir($container->getParameter('entrypoints.output_basedir'));
    $instance->setRuntimeNegotiator($container->get('entrypoints.runtime'));
    $instance->setRendererNegotiator($container->get('entrypoints.renderer'));
    $instance->setOutputScan($container->get('entrypoints.output_scan'));
    $instance->setModuleHandler($container->get('module_handler'));
    $instance->allRuntimes = $container->getParameter('entrypoints.runtimes');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return static::$formId;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [static::$configId];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::$configId);

    $none_option = $this->getNoneOption();
    $none_key = key($none_option);

    $form = parent::buildForm($form, $form_state);
    $form['#prefix'] = '<div id="' . static::$formWrapperId . '">';
    $form['#suffix'] = '</div>';
    $form['#tree'] = TRUE;
    $form['#access'] = $this->currentUser()->hasPermission('edit entrypoints config');
    $form['actions']['#weight'] = 1000;

    $form['build_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('Build mode'),
      '#description' => $this->t('The chosen build mode will determine which asset ouput folder will be used on this site, and this mode will be used as a default when rebuilding (i.e. compiling) assets.'),
      '#multiple' => FALSE,
      '#required' => TRUE,
      '#options' => $this->getBuildModeOptions(),
      '#default_value' => $config->get('build_mode'),
      '#weight' => 10,
      '#ajax' => [
        'callback' => [$this, 'ajaxRefresh'],
      ],
    ];

    $runtimes_available = $this->getRuntimeOptions();
    $runtimes_not_available = $this->getRuntimeOptionsNotAvailable();
    $default_runtime = $config->get('runtime');
    if ($default_runtime && isset($runtimes_not_available[$default_runtime])) {
      $this->messenger()->addWarning($this->t('The chosen runtime (@runtime) could not be found. It is possible that this runtime is available via CLI, but not for the web server. This is probably ok since assets are being built via CLI and not via web UI. For either case, you need to make sure on your own, that the runtime is available.', ['@runtime' => $default_runtime]));
    }

    $form['runtime'] = [
      '#type' => 'select',
      '#title' => $this->t('Scripts runtime'),
      '#description' => $this->t('Choose an available runtime for running the scripts for rebuilding assets. When none is chosen, you cannot perform asset rebuilding with Drush or the Rebuild form, and no status check on possibly outdated packages can be performed on existing sources.'),
      '#multiple' => FALSE,
      '#required' => FALSE,
      '#options' => $none_option + $runtimes_available + $runtimes_not_available,
      '#default_value' => $default_runtime,
      '#empty_value' => $none_key,
      '#weight' => 20,
      '#ajax' => [
        'callback' => [$this, 'ajaxRefresh'],
      ],
    ];

    $form['renderer'] = [
      '#type' => 'select',
      '#title' => $this->t('Server-side renderer (SSR)'),
      '#description' => $this->t('Choose an available plugin for server-side rendering. When none is chosen, entrypoints cannot be rendered on the server-side.'),
      '#multiple' => FALSE,
      '#required' => FALSE,
      '#options' => $none_option + $this->getRendererOptions(),
      '#default_value' => $config->get('renderer'),
      '#empty_value' => $none_key,
      '#weight' => 30,
      '#ajax' => [
        'callback' => [$this, 'ajaxRefresh'],
      ],
    ];

    $projects = $config->get('projects') ?: [];
    if ($form_state->hasValue('projects')) {
      $projects = $form_state->getValue('projects');
      foreach ($projects as $i => $project) {
        unset($projects[$i]['actions']);
      }
      unset($projects['actions']);
    }
    $projects = array_values($projects);
    $form['projects'] = [
      '#type' => 'details',
      '#title' => $this->t('Projects (@num)', ['@num' => count($projects)]),
      '#open' => $this->isElementInScope(['projects'], $form_state) || $form_state->has('projects_changed') || $form_state->has('add_project'),
      '#weight' => 40,
    ];

    foreach ($projects as $project) {
      $this->buildProjectElements($form, $form_state, $project);
    }
    if ($form_state->has('add_project') && count($projects) === $form_state->get('add_project')) {
      $this->buildProjectElements($form, $form_state);
    }
    else {
      $form['projects']['actions'] = [
        '#type' => 'container',
        '#weight' => (count(Element::children($form['projects'])) + 1) * 1000,
      ];
      $form['projects']['actions']['add'] = [
        '#type' => 'submit',
        '#value' => $this->t('Add'),
        '#name' => 'project-add',
        '#ajax' => [
          'callback' => [$this, 'ajaxRefresh'],
        ],
        '#submit' => [
          [$this, 'projectAddSubmit'],
        ],
      ];
    }

    $this->complainAboutDuplicateNames();

    $form['defaults'] = [
      '#type' => 'details',
      '#title' => $this->t('Default entrypoint settings'),
      '#description' => $this->t('Default settings to be applied for every entrypoint, which has no override settings.'),
      '#open' => $this->isElementInScope(['defaults'], $form_state),
      '#weight' => 50,
    ];
    $this->buildEntrypointSpecificSettingsElements($form['defaults'], $form_state, $config->get('defaults'), ['defaults']);

    $overrides = $config->get('overrides') ?: [];
    if ($form_state->hasValue('overrides')) {
      $overrides = $form_state->getValue('overrides');
      foreach ($overrides as $i => $override) {
        unset($overrides[$i]['actions']);
      }
      unset($overrides['actions']);
    }
    $overrides = array_values($overrides);
    $form['overrides'] = [
      '#type' => 'details',
      '#title' => $this->t('Override entrypoint settings (@num)', ['@num' => count($overrides)]),
      '#description' => $this->t('Settings which override the default settings.'),
      '#open' => $this->isElementInScope(['overrides'], $form_state),
      '#weight' => 60,
    ];
    if ($form_state->has('add_override') && (empty($overrides) || count($overrides) === $form_state->get('add_override'))) {
      $overrides[] = [];
    }
    $triggering_element = $form_state->getTriggeringElement();
    foreach ($overrides as $i => $override) {
      if (isset($override['remove']) || ($form_state->has('overrides_changed') && isset($triggering_element['#name']) && $triggering_element['#name'] === 'remove-override-' . $i)) {
        $form['overrides'][$i]['remove'] = [
          '#type' => 'hidden',
          '#value' => $i,
        ];
        continue;
      }
      $is_new = empty($override) || ($form_state->has('add_override') && $form_state->get('add_override') === $i);
      if ($is_new) {
        $override_title = $this->t('New override settings (please fill in values, then click on "@save" at the bottom of this form)', ['@save' => $this->t('Save configuration')]);
      }
      else {
        $override_title = $this->t('Override for "@entrypoint"', ['@entrypoint' => $override['entrypoint']]);
      }
      $form['overrides'][$i] = [
        '#type' => 'details',
        '#title' => $override_title,
        '#open' => $is_new || $this->isElementInScope(['overrides', $i], $form_state),
        '#weight' => ($i + 1) * 10,
        '#attributes' => [
          'class' => ['override-details'],
          'data-override-index' => $i,
        ],
      ];
      $form['overrides'][$i]['entrypoint'] = [
        '#type' => 'textfield',
        '#maxlength' => 300,
        '#required' => TRUE,
        '#title' => $this->t('Entrypoint'),
        '#default_value' => isset($override['entrypoint']) ? $override['entrypoint'] : '',
        '#weight' => 0,
      ];
      $form['overrides'][$i]['entrypoint_description'] = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $this->t('The specified entrypoint where these override settings are being applied. Example: <em>navbar</em>. Wildcards are allowed. Example: <em>navbar.*</em> applies on every entrypoint starting with "<em>navbar.</em>", i.e. every Drupal library starting with "<em>entrypoints/navbar.</em>")'),
        '#weight' => 2,
      ];
      $this->buildEntrypointSpecificSettingsElements(
        $form['overrides'][$i], $form_state, $override, ['overrides', $i]);
      if (!$is_new) {
        $form['overrides'][$i]['actions'] = [
          '#type' => 'container',
          '#weight' => (count(Element::children($form['overrides'][$i])) + 1) * 1000,
        ];
        $form['overrides'][$i]['actions']['remove'] = [
          '#type' => 'submit',
          '#name' => 'remove-override-' . $i,
          '#value' => $this->t('Remove from configuration'),
          '#ajax' => [
            'callback' => [$this, 'ajaxRefresh'],
          ],
          '#submit' => [
            [$this, 'overrideRemoveSubmit'],
          ],
          '#attributes' => [
            'class' => ['override-remove-submit'],
            'data-override-index' => $i,
          ],
        ];
      }
    }
    if (!$form_state->has('add_override')) {
      $form['overrides']['actions'] = [
        '#type' => 'container',
        '#weight' => (count(Element::children($form['overrides'])) + 1) * 1000,
      ];
      $form['overrides']['actions']['add'] = [
        '#type' => 'submit',
        '#value' => $this->t('Add'),
        '#name' => 'override-add',
        '#ajax' => [
          'callback' => [$this, 'ajaxRefresh'],
        ],
        '#submit' => [
          [$this, 'overrideAddSubmit'],
        ],
      ];
    }

    $form['tweaks'] = [
      '#type' => 'details',
      '#title' => $this->t('Optimization and compatibility tweaks'),
      '#open' => FALSE,
      '#weight' => 70,
    ];
    $form['tweaks']['disable_active_link'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Prevent Javascript of Drupal active link (core/drupal.active-link) to be loaded on every page (except admin pages).'),
      '#default_value' => !empty($config->get('tweaks.disable_active_link')),
      '#return_value' => TRUE,
      '#weight' => 10,
    ];
    $form['tweaks']['disable_system_base'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Prevent base CSS (system/base) to be loaded on every page (except admin pages).'),
      '#default_value' => !empty($config->get('tweaks.disable_system_base')),
      '#return_value' => TRUE,
      '#weight' => 20,
    ];
    $form['tweaks']['disable_contextual_links'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Prevent contextual links to be loaded on every page (except admin pages).'),
      '#default_value' => !empty($config->get('tweaks.disable_contextual_links')),
      '#return_value' => TRUE,
      '#weight' => 30,
    ];
    if (!$this->moduleHandler->moduleExists('contextual')) {
      $form['tweaks']['disable_contextual_links']['#disabled'] = TRUE;
      $form['tweaks']['disable_contextual_links']['#description'] = $this->t('This option is not available, because the Contextual Links module is not installed.');
    }

    return $form;
  }

  /**
   * Helper function to build the form elements for the given project.
   *
   * @param array &$form
   *   The form array that is being passed as reference.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The corresponding form state.
   * @param array $project
   *   (Optional) The project in scope. An empty array indicates form elements
   *   for a newly added project.
   */
  protected function buildProjectElements(array &$form, FormStateInterface $form_state, array $project = []) {
    $config = $this->config(static::$configId);
    $none_option = $this->getNoneOption();
    $none_key = key($none_option);

    $i = count(Element::children($form['projects']));
    $is_new = empty($project) || ($form_state->has('add_project') && $form_state->get('add_project') === $i);

    if (!$is_new && ($triggering_element = $form_state->getTriggeringElement())) {
      if (isset($project['remove']) || ($form_state->has('projects_changed') && isset($triggering_element['#name']) && $triggering_element['#name'] === 'remove-project-' . $i)) {
        $form['projects'][$i]['remove'] = [
          '#type' => 'hidden',
          '#value' => $i,
        ];
        return;
      }
    }

    $script_options = [];
    $source_info = NULL;
    $source_location = !$is_new ? $project['source']['location'] : NULL;
    if ($form_state->hasValue(['projects', $i, 'source', 'location'])) {
      $source_location = $form_state->getValue(
        ['projects', $i, 'source', 'location']);
    }
    if (!empty($source_location)) {
      try {
        $source_info = EntrypointsProjectSource::load($source_location);
        if ($source_info->exists()) {
          foreach ($source_info->getScriptsAvailable() as $script) {
            $script_options[$script] = $script;
          }
        }
        else {
          $this->messenger()->addWarning($this->t('The given project source location <em>@uri</em> does not exist. Please create the project folder with a valid package.json that includes Webpack Encore. If you have Drush installed, have a look at <em>drush help entrypoints:create-project</em> to kickstart a new project. See the README.md for details.', ['@uri' => $source_location]));
        }
      }
      catch (\RuntimeException $e) {
        $this->messenger()->addError($e->getMessage());
      }
    }

    $form['projects'][$i] = [
      '#type' => 'details',
      '#title' => $this->getProjectSummary($project, $i, $form_state),
      '#open' => $is_new || $this->isElementInScope(['projects', $i], $form_state),
      '#weight' => ($i + 1) * 10,
      '#attributes' => [
        'class' => ['project-details'],
        'data-project-index' => $i,
      ],
    ];
    $form['projects'][$i]['source'] = [
      '#type' => 'details',
      '#title' => $this->t('Source'),
      '#open' => $is_new || $this->isElementInScope(['projects', $i], $form_state),
      '#weight' => 10,
    ];
    $form['projects'][$i]['source']['location'] = [
      '#type' => 'textfield',
      '#maxlength' => 300,
      '#required' => FALSE,
      '#title' => $this->t('Location uri'),
      '#default_value' => isset($project['source']['location']) ? $project['source']['location'] : '',
      '#weight' => 10,
      '#ajax' => [
        'callback' => [$this, 'ajaxRefresh'],
        'disable-refocus' => TRUE,
      ],
    ];
    $form['projects'][$i]['source']['location_description'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $this->t('Must be either a relative path from the Drupal web application root, or a locally accessible stream wrapper. Example: <em>../assets/entry</em>, <em>private://asset-source/entry</em>'),
      '#weight' => 20,
    ];
    $form['projects'][$i]['source']['watch_outdated'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Keep an eye on outdated packages (exposed as status report on /admin/reports/status)'),
      '#default_value' => !empty($project['source']['location']) && !empty($project['source']['watch_outdated']),
      '#return_value' => TRUE,
      '#weight' => 30,
      '#disabled' => !$source_info || !$source_info->exists(),
    ];

    $form['projects'][$i]['build'] = [
      '#type' => 'container',
      '#weight' => 20,
      '#attributes' => [
        'class' => ['project-build-settings'],
      ],
    ];
    $build_mode_i = 0;
    $build_modes = $this->getBuildModeOptions();
    $default_build_mode = $form_state->hasValue('build_mode') ? $form_state->getValue('build_mode') : $config->get('build_mode');
    foreach ($build_modes as $build_mode => $label) {
      $form['projects'][$i]['build'][$build_mode] = [
        '#type' => 'details',
        '#title' => $this->t('Build mode @mode', ['@mode' => $label]),
        '#open' => $is_new || $this->isElementInScope(['projects', $i], $form_state) || $default_build_mode === $build_mode,
        '#weight' => ($build_mode_i + 1) * 10,
      ];
      $form['projects'][$i]['build'][$build_mode]['location'] = [
        '#type' => 'textfield',
        '#maxlength' => 300,
        '#required' => ($default_build_mode === $build_mode),
        '#title' => $this->t('Output folder'),
        '#default_value' => isset($project['build'][$build_mode]['location']) ? $project['build'][$build_mode]['location'] : '',
        '#weight' => 10,
      ];
      $example_folder = $build_mode === 'development' ? 'entry_dev' : 'entry';
      $form['projects'][$i]['build'][$build_mode]['location_description'] = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $this->t(
          'Must be either a relative path from the Drupal web application root that resolves to a sub-folder of @basedir, or a single folder name that is located under @basedir. Examples: <em>@basedir/@folder</em>, or short <em>@folder</em>',
          ['@basedir' => $this->outputBasedir, '@folder' => $example_folder]
        ),
        '#weight' => 20,
      ];
      $form['projects'][$i]['build'][$build_mode]['location_ssr'] = [
        '#type' => 'textfield',
        '#maxlength' => 300,
        '#required' => FALSE,
        '#title' => $this->t('Output folder of scripts for server-side rendering (SSR)'),
        '#default_value' => isset($project['build'][$build_mode]['location_ssr']) ? $project['build'][$build_mode]['location_ssr'] : '',
        '#weight' => 30,
      ];
      $example_folder = $build_mode === 'development' ? 'entry_dev_ssr' : 'entry_ssr';
      $form['projects'][$i]['build'][$build_mode]['location_ssr_description'] = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $this->t(
          'Must be either a relative path from the Drupal web application root that resolves to a sub-folder of @basedir, or a single folder name that is located under @basedir. Examples: <em>@basedir/@folder</em>, or short <em>@folder</em>',
          ['@basedir' => $this->outputBasedir, '@folder' => $example_folder]
        ) . '. ' . $this->t('Entrypoints that are defined for server-side rendering (SSR) will be matched automatically with client-side entrypoints that have the same name. If you have SSR entrypoints defined with a different name, you can assign them manually to client-side entrypoints with default or override settings (see below).'),
        '#weight' => 40,
      ];
      $form['projects'][$i]['build'][$build_mode]['script'] = [
        '#type' => 'select',
        '#required' => FALSE,
        '#title' => $this->t('Build script'),
        '#description' => $this->t('To be resolved with package.json "scripts" definition. Reload this config page in case you added new scripts to your package.json files.'),
        '#options' => $none_option + $script_options,
        '#default_value' => isset($project['build'][$build_mode]['script']) ? $project['build'][$build_mode]['script'] : $none_key,
        '#empty_value' => $none_key,
        '#weight' => 50,
      ];
      $build_mode_i++;
    }

    $output_uri = !$is_new && !empty($project['build'][$default_build_mode]['location']) ? $project['build'][$default_build_mode]['location'] : NULL;
    if ($form_state->hasValue(
      ['projects', $i, 'build', $default_build_mode, 'location'])) {
      $output_uri = $form_state->getValue(
        ['projects', $i, 'build', $default_build_mode, 'location']);
    }
    $output_uri_ssr = !$is_new && !empty($project['build'][$default_build_mode]['location_ssr']) ? $project['build'][$default_build_mode]['location_ssr'] : NULL;
    if ($form_state->hasValue(
      ['projects', $i, 'build', $default_build_mode, 'location_ssr'])) {
      $output_uri_ssr = $form_state->getValue(
        ['projects', $i, 'build', $default_build_mode, 'location_ssr']
      );
    }
    if (!empty($default_build_mode) && isset($build_modes[$default_build_mode]) && (!empty($output_uri) || !empty($output_uri_ssr))) {
      $entrypoint_names_client = !empty($output_uri) ? $this->getEntrypointNames($output_uri, FALSE) : [];
      $entrypoint_names_ssr = !empty($output_uri_ssr) ? $this->getEntrypointNames($output_uri_ssr, TRUE) : [];
      if (is_array($entrypoint_names_client) && is_array($entrypoint_names_ssr)) {
        $entrypoint_names = [];
        foreach ($entrypoint_names_client as $name) {
          if (in_array($name, $entrypoint_names_ssr)) {
            $entrypoint_names[] = $name . ' (' . $this->t('with SSR') . ')';
          }
          else {
            $entrypoint_names[] = $name;
          }
        }
        foreach ($entrypoint_names_ssr as $name) {
          if (!in_array($name, $entrypoint_names_client)) {
            $entrypoint_names[] = $name . ' (' . $this->t('SSR only') . ')';
          }
        }
        $form['projects'][$i]['entrypoints'] = [
          '#type' => 'details',
          '#title' => $this->t('Available @mode entrypoints (@num)',
          [
            '@mode' => $build_modes[$default_build_mode],
            '@num' => count($entrypoint_names),
          ]),
          '#open' => $is_new || $this->isElementInScope(['projects', $i], $form_state),
          '#weight' => 30,
        ];
        if (empty($entrypoint_names)) {
          $form['projects'][$i]['entrypoints']['description'] = [
            '#type' => 'html_tag',
            '#tag' => 'p',
            '#value' => $this->t('No entrypoint definitions are available. Make sure the @mode output directory contains all asset builds and has a valid entrypoints.json file.', ['@mode' => $build_modes[$default_build_mode]]),
          ];
        }
        else {
          $example_name = !empty($entrypoint_names_client) ? reset($entrypoint_names_client) : reset($entrypoint_names_ssr);
          $form['projects'][$i]['entrypoints']['description'] = [
            '#type' => 'html_tag',
            '#tag' => 'p',
            '#value' => $this->t('Each listed entrypoint name here is available as a Drupal library with the exact same name and "entrypoints/" string as prefix. Example: Entrypoint <em>@name</em> is available as Drupal library <em>entrypoints/@name</em>.', ['@name' => $example_name]),
            '#weight' => 10,
          ];
          $form['projects'][$i]['entrypoints']['list'] = [
            '#theme' => 'item_list',
            '#items' => $entrypoint_names,
            '#weight' => 20,
          ];
        }
      }
    }

    if (!$is_new) {
      $form['projects'][$i]['actions'] = [
        '#type' => 'container',
        '#weight' => 40,
      ];
      $form['projects'][$i]['actions']['remove'] = [
        '#type' => 'submit',
        '#name' => 'remove-project-' . $i,
        '#value' => $this->t('Remove from configuration'),
        '#ajax' => [
          'callback' => [$this, 'ajaxRefresh'],
        ],
        '#submit' => [
          [$this, 'projectRemoveSubmit'],
        ],
        '#attributes' => [
          'class' => ['project-remove-submit'],
          'data-project-index' => $i,
        ],
      ];
    }
  }

  /**
   * Helper function to build the entrypoint-specific settings.
   *
   * @param array &$element
   *   The part of the form where to build the elements, passed as reference.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The corresponding form state.
   * @param array $settings
   *   (Optional) The entrypoint settings in scope. An empty array indicates
   *   new override settings.
   * @param array $form_key
   *   (Optional) The key to retrieve existing corresponding form state values.
   */
  protected function buildEntrypointSpecificSettingsElements(array &$element, FormStateInterface $form_state, array $settings = [], array $form_key = []) {
    $is_new = empty($settings);
    $none_option = $this->getNoneOption();
    $none_key = key($none_option);

    $element['library'] = [
      '#type' => 'details',
      '#title' => $this->t('Library'),
      '#open' => !empty($element['#open']),
      '#weight' => 10,
    ];
    $element['library']['header'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include the entrypoint assets in the HTML &lt;head&gt; instead of the &lt;body&gt;'),
      '#default_value' => !empty($settings['library']['header']),
      '#return_value' => TRUE,
      '#weight' => 10,
    ];
    $element['library']['dependencies'] = [
      '#type' => 'details',
      '#title' => $this->t('Dependencies to other Drupal libraries'),
      '#description' => $this->t('Example values (one value per field): <b>core/drupalSettings</b>, <b>core/jquery</b>'),
      '#open' => TRUE,
      '#weight' => 20,
    ];
    $dependencies = !empty($settings['library']['dependencies']) ? $settings['library']['dependencies'] : [];
    $dependencies_form_key = array_merge($form_key, ['library', 'dependencies']);
    if ($form_state->hasValue($form_key)) {
      $dependencies = $form_state->getValue($dependencies_form_key);
    }
    foreach ($dependencies as $i => $dependency) {
      $dependency = trim((string) $dependency);
      if (empty($dependency)) {
        unset($dependencies[$i]);
      }
      elseif (strpos($dependency, ',') !== FALSE) {
        unset($dependencies[$i]);
        foreach (explode(',', $dependency) as $dependency_split) {
          $dependency_split = trim((string) $dependency_split);
          if (!empty($dependency_split)) {
            $dependencies[] = $dependency_split;
          }
        }
      }
      else {
        $dependencies[$i] = $dependency;
      }
    }
    $dependencies = array_values(array_unique($dependencies));
    $dependencies = array_merge($dependencies, ['']);
    if ($form_state->hasValue($form_key) && $form_state->getValue($dependencies_form_key) != $dependencies) {
      $form_state->setValue($dependencies_form_key, $dependencies);
      $user_input = $form_state->getUserInput();
      NestedArray::setValue($user_input, $dependencies_form_key, $dependencies);
      $form_state->setUserInput($user_input);
    }
    foreach ($dependencies as $i => $dependency) {
      $element['library']['dependencies'][$i] = [
        '#type' => 'textfield',
        '#maxlength' => 300,
        '#required' => FALSE,
        '#default_value' => $dependency,
        '#weight' => ($i + 1) * 10,
        '#ajax' => [
          'callback' => [$this, 'ajaxRefresh'],
          'disable-refocus' => TRUE,
        ],
      ];
    }

    $element['library']['optimization'] = [
      '#type' => 'details',
      '#title' => $this->t('Optimization'),
      '#open' => TRUE,
      '#weight' => 30,
    ];
    $element['library']['optimization']['merge_chunks'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Handle chunks, that are also present in other entrypoints, as the same dependency.'),
      '#default_value' => $is_new || !empty($settings['library']['optimization']['merge_chunks']),
      '#return_value' => TRUE,
      '#weight' => 10,
    ];
    $element['library']['optimization']['preprocess'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Apply Drupal's built-in aggregation (if enabled on @path).", ['@path' => '/admin/config/development/performance']),
      '#default_value' => $is_new || !empty($settings['library']['optimization']['preprocess']),
      '#return_value' => TRUE,
      '#weight' => 20,
    ];
    $element['library']['optimization']['minify'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Apply Drupal's built-in minification (if enabled on @path).", ['@path' => '/admin/config/development/performance']),
      '#default_value' => !empty($settings['library']['optimization']['minify']),
      '#return_value' => TRUE,
      '#weight' => 30,
    ];
    $element['library']['optimization']['defer'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Use the defer attribute on script inclusion."),
      '#default_value' => !empty($settings['library']['optimization']['defer']),
      '#return_value' => TRUE,
      '#weight' => 40,
    ];
    $element['library']['optimization']['async'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Use the async attribute on script inclusion."),
      '#default_value' => !empty($settings['library']['optimization']['async']),
      '#return_value' => TRUE,
      '#weight' => 50,
    ];

    $element['attach'] = [
      '#type' => 'details',
      '#title' => $this->t('Attachment'),
      '#open' => FALSE,
      '#weight' => 20,
    ];
    $element['attach']['everywhere'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include on every page (including admin pages)'),
      '#default_value' => !empty($settings['attach']['everywhere']),
      '#return_value' => TRUE,
      '#weight' => 10,
    ];
    $element['attach']['non_admin'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include on every page (except admin pages)'),
      '#default_value' => !empty($settings['attach']['non_admin']),
      '#return_value' => TRUE,
      '#weight' => 20,
    ];
    $element['attach']['path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Include on every page that matches the specified path'),
      '#default_value' => !empty($settings['attach']['path']),
      '#weight' => 30,
      '#maxlength' => 300,
      '#required' => FALSE,
    ];
    $element['attach']['theme'] = [
      '#type' => 'select',
      '#title' => $this->t("Include on every page that's using the selected theme"),
      '#required' => FALSE,
      '#weight' => 40,
      '#options' => $none_option + $this->getThemeOptions(),
      '#empty_value' => $none_key,
      '#default_value' => isset($settings['attach']['theme']) ? $settings['attach']['theme'] : $none_key,
    ];

    $element['ssr'] = [
      '#type' => 'details',
      '#title' => $this->t('Server-side rendering (SSR)'),
      '#open' => FALSE,
      '#weight' => 30,
    ];
    $element['ssr']['entrypoint'] = [
      '#type' => 'select',
      '#title' => $this->t("Entrypoint"),
      '#description' => $this->t('Choose an entrypoint that should be used for server-side rendering. If none is chosen, the entrypoint itself will be used if it has SSR assets. Server-side rendering only works if a renderer is available and chosen (see above).'),
      '#required' => FALSE,
      '#weight' => 10,
      '#options' => $none_option + $this->getSsrEntrypointOptions(),
      '#empty_value' => $none_key,
      '#default_value' => isset($settings['ssr']['entrypoint']) ? $settings['ssr']['entrypoint'] : $none_key,
    ];
  }

  /**
   * Get the available build modes as options array.
   *
   * @return string[]
   *   The available build modes with translated labels.
   */
  protected function getBuildModeOptions() {
    $available = [];
    foreach ($this->runtimeNegotiator->getBuildModesAvailable() as $mode => $label) {
      $available[$mode] = $this->t($label);
    }
    return $available;
  }

  /**
   * Get the available runtimes as options array.
   *
   * @return string[]
   *   The available runtimes with translated label and version info.
   */
  protected function getRuntimeOptions() {
    $available = [];
    foreach ($this->runtimeNegotiator->getRuntimesAvailable() as $runtime => $info) {
      $available[$runtime] = $this->t("@label (v@version)",
        ['@label' => $info['label'], '@version' => $info['version']]);
    }
    return $available;
  }

  /**
   * Get the not available runtimes as options array.
   *
   * @return string[]
   *   The not available runtimes with translated label info.
   */
  protected function getRuntimeOptionsNotAvailable() {
    /** @var \Drupal\entrypoints\Plugin\EntrypointsRuntimePluginManager $plugin_manager */
    $plugin_manager = \Drupal::service('plugin.manager.entrypoints.runtime');
    $available = $this->runtimeNegotiator->getRuntimesAvailable();
    $not_available = [];
    foreach ($this->allRuntimes as $runtime) {
      if (!isset($available[$runtime])) {
        $info = $plugin_manager->getDefinition($runtime);
        $not_available[$runtime] = $this->t("@label",
          ['@label' => $info['label']]);
      }
    }
    return $not_available;
  }

  /**
   * Get the available server-side renderers as options array.
   *
   * @return string[]
   *   The available renderers with translated label and version info.
   */
  protected function getRendererOptions() {
    $available = [];
    foreach ($this->rendererNegotiator->getRenderersAvailable() as $renderer => $info) {
      $available[$renderer] = $this->t(
        "@label (v@version)",
        ['@label' => $info['label'], '@version' => $info['version']]
      );
    }
    return $available;
  }

  /**
   * Get the none option as array.
   *
   * @return array
   *   The none option as option array.
   */
  protected function getNoneOption() {
    return ['_none' => $this->t('- None -')];
  }

  /**
   * Get available theme options.
   *
   * @return array
   *   The available theme options.
   */
  protected function getThemeOptions() {
    $default_theme = $this->config('system.theme') ? $this->config('system.theme')->get('default') : 'stable';
    $installed_themes = $this->config('core.extension')->get('theme') ?: [];

    // Change orders, the default theme should appear first.
    $installed_themes = array_keys($installed_themes);
    foreach ($installed_themes as $index => $theme_name) {
      if ($default_theme == $theme_name) {
        unset($installed_themes[$index]);
      }
    }
    if (!empty($default_theme)) {
      array_unshift($installed_themes, $default_theme);
    }

    return array_combine($installed_themes, $installed_themes);
  }

  /**
   * Set the runtime negotiator.
   *
   * @param \Drupal\entrypoints\EntrypointsRuntimeNegotiator $negotiator
   *   The runtime negotiator.
   */
  public function setRuntimeNegotiator(EntrypointsRuntimeNegotiator $negotiator) {
    $this->runtimeNegotiator = $negotiator;
  }

  /**
   * Set the server-side renderer negotiator.
   *
   * @param \Drupal\entrypoints\EntrypointsRendererNegotiator $negotiator
   *   The server-side renderer negotiator.
   */
  public function setRendererNegotiator(EntrypointsRendererNegotiator $negotiator) {
    $this->rendererNegotiator = $negotiator;
  }

  /**
   * Set the entrypoints output scan service.
   *
   * @param \Drupal\entrypoints\EntrypointsOutputScan $output_scan
   *   The output scan service.
   */
  public function setOutputScan(EntrypointsOutputScan $output_scan) {
    $this->outputScan = $output_scan;
  }

  /**
   * Set the module handler.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function setModuleHandler(ModuleHandlerInterface $module_handler) {
    $this->moduleHandler = $module_handler;
  }

  /**
   * Project add submit action callback.
   *
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The corresponding form state.
   */
  public function projectAddSubmit(array &$form, FormStateInterface $form_state) {
    $projects = $this->config(static::$configId)->get('projects');
    $num_projects = empty($projects) ? 0 : count($projects);
    $form_state->set('add_project', $num_projects);
    $form_state->setRebuild();
  }

  /**
   * Override add submit action callback.
   *
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The corresponding form state.
   */
  public function overrideAddSubmit(array &$form, FormStateInterface $form_state) {
    $overrides = $this->config(static::$configId)->get('overrides');
    $num_overrides = empty($overrides) ? 0 : count($overrides);
    $form_state->set('add_override', $num_overrides);
    $form_state->setRebuild();
  }

  /**
   * Override remove submit action callback.
   *
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The corresponding form state.
   */
  public function overrideRemoveSubmit(array &$form, FormStateInterface $form_state) {
    $form_state->set('overrides_changed', TRUE);
    $form_state->setRebuild();
  }

  /**
   * Project remove submit action callback.
   *
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The corresponding form state.
   */
  public function projectRemoveSubmit(array &$form, FormStateInterface $form_state) {
    $form_state->set('projects_changed', TRUE);
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $projects = $form_state->getValue('projects');
    $default_build_mode = $form_state->getValue('build_mode');
    unset($projects['actions']);
    if (!empty($projects)) {
      foreach ($projects as $i => $project) {
        if (!empty($project['build'])) {
          foreach ($project['build'] as $build_mode => $info) {
            if ($default_build_mode !== $build_mode) {
              continue;
            }
            if (!empty($info['location'])) {
              try {
                $this->outputScan->scan($info['location']);
              }
              catch (\Exception $e) {
                $form_state->setError($form['projects'][$i]['build'][$build_mode]['location'], $e->getMessage());
              }
            }
            if (!empty($info['location_ssr'])) {
              try {
                $this->outputScan->scan($info['location_ssr']);
              }
              catch (\Exception $e) {
                $form_state->setError($form['projects'][$i]['build'][$build_mode]['location_ssr'], $e->getMessage());
              }
            }
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config(static::$configId);
    $this->updateConfigValues($form_state);
    if (static::ensureProtection()) {
      $config->save();
      static::clearCachedDefinitions();
      parent::submitForm($form, $form_state);
      $this->messenger()->addStatus($this->t('<a href="/admin/config/development/performance">Clear the cache</a> to make sure that your recent changes take effect.'));
    }
    else {
      $this->messenger()->addError($this->t('The configuration has not been saved. Something went wrong when trying to ensure that SSR output directories are protected from public access. Beware that your server-side render scripts might be publicly exposed. Please check the log for details, and make sure that your SSR output directories are properly protected from public access.'));
    }
  }

  /**
   * Performs an Ajax refresh on the form.
   *
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The corresponding form state.
   */
  public function ajaxRefresh(array $form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand('#' . static::$formWrapperId, $form));
    // Remove all previous messages from initial page load.
    // This removal also prevents strange looking nested message containers.
    $response->addCommand(new ReplaceCommand('[data-drupal-messages]', '<div data-drupal-messages=""></div>'));
    foreach ($this->messenger()->all() as $type => $messages) {
      foreach ($messages as $message) {
        switch ($type) {
          case $this->messenger()::TYPE_WARNING:
            $response->addCommand(new MessageCommand($message, NULL, ['type' => 'warning'], FALSE));
            break;

          case $this->messenger()::TYPE_ERROR:
            $response->addCommand(new MessageCommand($message, NULL, ['type' => 'error'], FALSE));
            break;

          default:
            $response->addCommand(new MessageCommand($message, NULL, [], FALSE));
        }
      }
    }
    $this->messenger()->deleteAll();
    return $response;
  }

  /**
   * Get a brief entrypoints project summary as translatable string.
   *
   * @param array $project
   *   The project info array, which is an entry of
   *   'entrypoints.settings.projects'.
   * @param int $index
   *   The index position of the project.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The project summary.
   */
  protected function getProjectSummary(array $project, $index, FormStateInterface $form_state) {
    $is_new = empty($project) || ($form_state->has('add_project') && $form_state->get('add_project') === $index);
    if ($is_new) {
      return $this->t('New project (please fill in values, then click on "@save" at the bottom of this form)', ['@save' => $this->t('Save configuration')]);
    }

    $summary = '@source_info, @output_info';
    $args = [];

    $source_location = !empty($project['source']['location']) ? $project['source']['location'] : NULL;
    if ($form_state->hasValue(['projects', $index, 'source', 'location'])) {
      $source_location = $form_state->getValue(
        ['projects', $index, 'source', 'location']);
    }
    if (empty($source_location)) {
      $args['@source_info'] = $this->t('No source');
    }
    else {
      $args['@source_info'] = $this->t('Source in "@folder"', ['@folder' => $source_location]);
    }

    $output_info = [];
    $build_modes = $this->getBuildModeOptions();
    foreach ($project['build'] as $mode => $info) {
      $output_uri = isset($info['location']) ? $info['location'] : NULL;
      if ($form_state->hasValue(['projects', $index, 'build', $mode, 'location'])) {
        $output_uri = $form_state->getValue(
          ['projects', $index, 'build', $mode, 'location']);
      }
      if (empty($output_uri) || empty($build_modes[$mode])) {
        continue;
      }
      $output_folder = substr(strrchr('/' . str_replace(DIRECTORY_SEPARATOR, '/', $output_uri), '/'), 1);
      $output_info[] = $this->t('@mode output "@folder"',
        ['@mode' => $build_modes[$mode], '@folder' => $output_folder]);
    }
    $args['@output_info'] = implode(', ', $output_info);

    return $this->t($summary, $args);
  }

  /**
   * Get available server-side rendering entrypoint options.
   *
   * @return array
   *   The available server-side rendering entrypoint options.
   */
  protected function getSsrEntrypointOptions() {
    if (empty($this->cache['entrypoints_info']['ssr'])) {
      return [];
    }
    $options = [];
    foreach ($this->cache['entrypoints_info']['ssr'] as $names) {
      foreach ($names as $name) {
        $options[$name] = $name;
      }
    }
    return $options;
  }

  /**
   * Get defined entrypoint names for the given output uri.
   *
   * @param string $output_uri
   *   The output uri as single folder, relative from app root or absolute path.
   * @param bool $is_ssr
   *   Whether the output uri points to a folder that contains SSR entrypoints.
   *   Default is set to FALSE.
   *
   * @return array|false
   *   An array of entrypoint names that are registered within that output uri.
   *   Returns FALSE if something was wrong when scanning the output directory.
   */
  protected function getEntrypointNames($output_uri, $is_ssr = FALSE) {
    $key = $is_ssr ? 'ssr' : 'client';
    if (!isset($this->cache['entrypoints_info'][$key][$output_uri])) {
      try {
        $scanned = $this->outputScan->scan($output_uri);
        $this->cache['entrypoints_info'][$key][$output_uri] = array_keys($scanned['asset_files']);
      }
      catch (\Exception $e) {
        $this->messenger()->addError($e->getMessage());
        $this->cache['entrypoints_info'][$key][$output_uri] = FALSE;
      }
    }
    return $this->cache['entrypoints_info'][$key][$output_uri];
  }

  /**
   * This method prompts a warning if there are duplicate entrypoint names.
   */
  protected function complainAboutDuplicateNames() {
    $duplicates = ['ssr' => [], 'client' => []];
    foreach (['ssr', 'client'] as $key) {
      if (isset($this->cache['entrypoints_info'][$key])) {
        foreach ($this->cache['entrypoints_info'][$key] as $output_uri => $names) {
          foreach ($this->cache['entrypoints_info'][$key] as $output_uri_subject => $names_subject) {
            if ($output_uri === $output_uri_subject || !is_array($names) || !is_array($names_subject)) {
              continue;
            }
            $duplicates[$key] = array_merge($duplicates[$key], array_intersect($names, $names_subject));
          }
        }
      }
    }
    if (!empty($duplicates['ssr']) || !empty($duplicates['client'])) {
      $duplicates = array_unique(array_merge($duplicates['ssr'], $duplicates['client']));
      $num_duplicates = count($duplicates);
      $duplicates_info = '';
      if ($num_duplicates > 3) {
        $duplicates_info = $this->t('<em>@names</em> and @num more',
        [
          '@names' => implode(', ', array_splice($duplicates, 0, 3)),
          '@num' => $num_duplicates - 3,
        ]);
      }
      else {
        $duplicates_info = $this->t('<em>@names</em>', ['@names' => implode(', ', $duplicates)]);
      }
      $this->messenger()->addWarning($this->t('Entrypoint name collision detected. Multiple projects use the same entrypoint name (@duplicates). This causes undefined behavior when using entrypoints as libraries. Please use globally unique entrypoint names to avoid naming conflicts.', ['@duplicates' => $duplicates_info]));
    }
  }

  /**
   * Updates the configuration values with the current form state values.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  protected function updateConfigValues(FormStateInterface $form_state) {
    $config = $this->config(static::$configId);
    $build_mode_value = $form_state->getValue('build_mode');
    if (empty($build_mode_value) || $build_mode_value === '_none') {
      $build_mode_value = NULL;
    }
    $config->set('build_mode', $build_mode_value);
    $runtime_value = $form_state->getValue('runtime');
    if (empty($runtime_value) || $runtime_value === '_none') {
      $runtime_value = NULL;
    }
    $config->set('runtime', $runtime_value);
    $renderer_value = $form_state->getValue('renderer');
    if (empty($renderer_value) || $renderer_value === '_none') {
      $renderer_value = NULL;
    }
    $config->set('renderer', $renderer_value);
    $project_values = $form_state->getValue('projects');
    unset($project_values['actions']);
    foreach ($project_values as $i => $project_value) {
      if (isset($project_value['remove'])) {
        unset($project_values[$i]);
        continue;
      }
      unset($project_value['actions'], $project_value['entrypoints']);
      if (!empty($project_value['build'])) {
        foreach ($project_value['build'] as $build_mode => $build_info) {
          $project_value['build'][$build_mode]['location'] = empty($build_info['location']) || empty(trim($build_info['location'])) ? NULL : $build_info['location'];
          $project_value['build'][$build_mode]['location_ssr'] = empty($build_info['location_ssr']) || empty(trim($build_info['location_ssr'])) ? NULL : $build_info['location_ssr'];
          $project_value['build'][$build_mode]['script'] = empty($build_info['script']) || $build_info['script'] === '_none' ? NULL : $build_info['script'];
        }
      }
      $project_values[$i] = $project_value;
    }
    $config->set('projects', array_values($project_values));
    $defaults_values = $form_state->getValue('defaults');
    if (!empty($defaults_values['library']['dependencies'])) {
      foreach ($defaults_values['library']['dependencies'] as $i => $dependency) {
        $dependency = trim((string) $dependency);
        if (empty($dependency)) {
          unset($defaults_values['library']['dependencies'][$i]);
        }
        elseif (strpos($dependency, ',') !== FALSE) {
          unset($defaults_values['library']['dependencies'][$i]);
          foreach (explode(',', $dependency) as $dependency_split) {
            $dependency_split = trim((string) $dependency_split);
            if (!empty($dependency_split)) {
              $defaults_values['library']['dependencies'][$i][] = $dependency_split;
            }
          }
        }
        else {
          $defaults_values['library']['dependencies'][$i] = $dependency;
        }
      }
      $defaults_values['library']['dependencies'] = array_values(array_unique($defaults_values['library']['dependencies']));
    }
    $defaults_values['attach']['everywhere'] = (bool) $defaults_values['attach']['everywhere'];
    $defaults_values['attach']['non_admin'] = (bool) $defaults_values['attach']['non_admin'];
    $defaults_values['attach']['theme'] = empty($defaults_values['attach']['theme']) || $defaults_values['attach']['theme'] === '_none' ? NULL : $defaults_values['attach']['theme'];
    $defaults_values['ssr']['entrypoint'] = empty($defaults_values['ssr']['entrypoint']) || $defaults_values['ssr']['entrypoint'] === '_none' ? NULL : $defaults_values['ssr']['entrypoint'];
    unset($defaults_values['actions']);
    $config->set('defaults', $defaults_values);
    $overrides_values = $form_state->getValue('overrides');
    unset($overrides_values['actions']);
    if (empty($overrides_values)) {
      $overrides_values = [];
    }
    else {
      foreach ($overrides_values as $i => $override) {
        if (isset($override['remove']) || !isset($override['entrypoint'])) {
          unset($overrides_values[$i]);
          continue;
        }
        if (!empty($override['library']['dependencies'])) {
          foreach ($override['library']['dependencies'] as $k => $dependency) {
            $dependency = trim((string) $dependency);
            if (empty($dependency)) {
              unset($override['library']['dependencies'][$k]);
            }
            elseif (strpos($dependency, ',') !== FALSE) {
              unset($override['library']['dependencies'][$k]);
              foreach (explode(',', $dependency) as $dependency_split) {
                $dependency_split = trim((string) $dependency_split);
                if (!empty($dependency_split)) {
                  $override['library']['dependencies'][$k] = $dependency_split;
                }
              }
            }
            else {
              $override['library']['dependencies'][$k] = $dependency;
            }
          }
          $override['library']['dependencies'] = array_values(array_unique($override['library']['dependencies']));
        }
        $override['library']['header'] = (bool) $override['library']['header'];
        foreach ($override['library']['optimization'] as $key => $value) {
          $override['library']['optimization'][$key] = (bool) $value;
        }
        $override['attach']['everywhere'] = (bool) $override['attach']['everywhere'];
        $override['attach']['non_admin'] = (bool) $override['attach']['non_admin'];
        $override['attach']['theme'] = empty($override['attach']['theme']) || $override['attach']['theme'] === '_none' ? NULL : $override['attach']['theme'];
        $override['ssr']['entrypoint'] = empty($override['ssr']['entrypoint']) || $override['ssr']['entrypoint'] === '_none' ? NULL : $override['ssr']['entrypoint'];
        unset($override['actions']);
        $overrides_values[$i] = $override;
      }
    }
    $config->set('overrides', array_values($overrides_values));
    $config->set('tweaks', $form_state->getValue('tweaks'));
  }

  /**
   * Checks whether the form key is part of scope of the triggering element.
   *
   * @param array $form_key
   *   The form key to check for.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return bool
   *   Returns TRUE if it is part of the scope, FALSE otherwise.
   */
  protected function isElementInScope(array $form_key, FormStateInterface $form_state) {
    if (!($triggering_element = $form_state->getTriggeringElement())) {
      return FALSE;
    }
    if (empty($form_key) || empty($triggering_element['#parents'])) {
      return FALSE;
    }
    $parents = $triggering_element['#parents'];
    foreach ($form_key as $i => $key) {
      if (!isset($parents[$i]) || (string) $parents[$i] !== (string) $key) {
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * Ensures protection on SSR output directories.
   *
   * @return bool
   *   Returns TRUE on success, FALSE otherwise.
   */
  protected static function ensureProtection() {
    return \Drupal::service('entrypoints.output_protection')->ensure();
  }

  /**
   * Clears configuration-relevant cached definitions.
   */
  protected static function clearCachedDefinitions() {
    entrypoints_cache_flush();
    \Drupal::service('library.discovery')->clearCachedDefinitions();
  }

}

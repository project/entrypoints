<?php

namespace Drupal\entrypoints\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * The entrypoints rebuild form.
 *
 * @todo Implement UI form to rebuild entrypoints.
 */
class EntrypointsRebuildForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'entrypoints_rebuild';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $this->messenger()->addWarning($this->t('This UI to rebuild entrypoints has not been implemented yet. If you have Drush installed, you can use the command <em>entrypoints:rebuild</em> instead. Feel free to contribute to this project on <a href="https://www.drupal.org/project/entrypoints" target="_blank" rel="noopener noreferrer">drupal.org</a>.'));
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}

<?php

namespace Drupal\entrypoints;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\State\StateInterface;

/**
 * The entrypoints cleanup service.
 */
class EntrypointsCleanup {

  use MessageLogTrait;

  /**
   * The entrypoints registry service.
   *
   * @var \Drupal\entrypoints\EntrypointsRegistry
   */
  protected $registry;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The state storage.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The EntrypointsCleanup service constructor.
   *
   * @param \Drupal\entrypoints\EntrypointsRegistry $registry
   *   The entrypoints registry service.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state storage.
   */
  public function __construct(EntrypointsRegistry $registry, FileSystemInterface $file_system, TimeInterface $time, StateInterface $state) {
    $this->registry = $registry;
    $this->fileSystem = $file_system;
    $this->time = $time;
    $this->state = $state;
  }

  /**
   * Deletes all stales files that are older than the given threshold.
   *
   * @param int $threshold
   *   The threshold as a Unix timestamp value.
   * @param bool $no_cache
   *   (Optional) When set to TRUE, the cleanup process makes sure that
   *   all most recent files are included by clearing the whole registry cache.
   *   This should only be done on regular job runs, not on frequent requests.
   *
   * @return int
   *   The number of cleaned up files.
   */
  public function deleteStaleFiles($threshold = 0, $no_cache = FALSE) {
    if ($no_cache) {
      $this->registry->clearCache();
    }
    $stale_files = $this->registry->getStaleFiles();
    $count_cleanup = 0;
    foreach ($stale_files as $i => $stale_file) {
      if ($mtime = @filemtime($stale_file)) {
        if (!$threshold || ($this->time->getRequestTime() - $mtime > $threshold)) {
          $this->fileSystem->delete($stale_file);
          unset($stale_files[$i]);
          $count_cleanup++;
        }
      }
      else {
        unset($stale_files[$i]);
        $count_cleanup++;
      }
    }
    if ($count_cleanup > 0) {
      $this->log("Cleaned up %num files within entrypoint output directories.", ['%num' => $count_cleanup]);
      if ($cached = $this->state->get('entrypoints_registry_cache')) {
        // Manually update the registry cache, so that stale file deletion
        // will not be repeated on the ones that now got deleted.
        $cached['stale_files'] = $stale_files;
        $this->state->set('entrypoints_registry_cache', $cached);
        $this->registry->reset();
      }
    }
    return $count_cleanup;
  }

}

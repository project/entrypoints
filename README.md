# Entrypoints

**Use NPM/YARN asset builds as Drupal libraries from entrypoint definitions.**

**tldr; Integrates Webpack Encore into the Drupal library ecosystem.**

## 0. Contents

- 1. Introduction
- 2. Getting started
- 3. Recommendations
- 4. Features
- 5. Planned features
- 6. Missing features
- 7. Use cases

## 1. Introduction

This module enables you to include generated assets, that are formerly defined
with an entrypoints.json file, as regular Drupal libraries.
That entrypoints.json file comes along with generated assets, typically when
using Symfony's Webpack Encore package.

Because the whole concept of this module is based on the entrypoints.json
definition, it's recommended to use Webpack Encore here. Otherwise, that file
needs to be generated on your own to make use of this module.

Example content of an entrypoints.json file:

```javascript
{
  "entrypoints": {
    "navbar": {
      "js": [
        "/sites/all/assets/entry/runtime.41b815d0.js",
        "/sites/all/assets/entry/935.f5ed0a83.js",
        "/sites/all/assets/entry/navbar.e125a472.js"
      ]
    }
  }
}
```

The entrypoints.json file defines the entrypoints by entry key. In this example
there is one entrypoint "navbar" defined with three versioned Javascript files.

The Entrypoints module now enables you to use those entrypoints as regular
Drupal libraries. In this example, the "navbar" entrypoint would be available as
Drupal library "entrypoints/navbar". Example code for using it:

```php
function hook_page_attachments(array &$attachments) {
  $attachments['#attached']['library'][] = 'entrypoints/navbar';
}
```

More details about how the Symfony Framework uses the entrypoints.json file can
be read here: https://symfony.com/doc/current/frontend/encore/versioning.html

## 2. Getting started

Install this module as usual. If you need help regards installing a module,
have a look at https://www.drupal.org/docs/extending-drupal/installing-modules.

The easiest way to use this module is when you already use Symfony's Webpack
Encore in a project. You can create a new project like the following:

```bash
# The project root is the base directory that contains the drupal application
# folder (mostly named 'web' or 'docroot'), i.e. it's accessible via
# <YOUR_DRUPAL_PROJECT_ROOT>/web or <YOUR_DRUPAL_PROJECT_ROOT>/docroot.
cd <YOUR_DRUPAL_PROJECT_ROOT>
mkdir -p assets/entry && cd assets/entry && npm init -y
npm install --save-dev @symfony/webpack-encore webpack-notifier
# Alternatively, use this helper command if you have Drush installed.
# This command creates the project under <YOUR_DRUPAL_PROJECT_ROOT> for you,
# and adds the newly created project to your entrypoints configuration.
drush entrypoints:create-project ../assets/entry
```

Here is an example config for the corresponding `webpack.config.js` file - take
attention to the output and public path settings:

```javascript
const Encore = require('@symfony/webpack-encore');
if (!Encore.isRuntimeEnvironmentConfigured()) {
  Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

Encore
  // This defines an entrypoint "navbar".
  .addEntry('navbar', './src/navbar.js')
  // When chunk splitting is enabled, Drupal takes care of proper aggregation.
  .splitEntryChunks()
  .enableSingleRuntimeChunk()
  .enableBuildNotifications()
  .configureBabelPresetEnv((config) => {
    config.useBuiltIns = false;
  })
;

if (Encore.isProduction()) {
  Encore
    // The output path must exist and match within the module configuration
    // (see entrypoints.settings).
    .setOutputPath('../../web/sites/all/assets/entry')
    // Public path used by the web server to access the output path.
    .setPublicPath('/sites/all/assets/entry')
    .enableVersioning(true)
    .enableSourceMaps(false)
    ;
}
else {
  Encore
    // For more information about why a different output path
    // is being used here, see section 3.1.
    .setOutputPath('../../web/sites/all/assets/entry_dev')
    .setPublicPath('/sites/all/assets/entry_dev')
    // For more information about these settings,
    // have a look at section 3.2.
    .enableVersioning(false)
    .cleanupOutputBeforeBuild()
    .enableSourceMaps(true)
    ;
}

module.exports = Encore.getWebpackConfig();
```

To keep things simple, it's recommended to have a unified folder structure. In
this example, the project folder `assets/entry` would generate its production
build to `web/sites/all/assets/entry`, which would be publicly accessible
through the webserver via `/sites/all/assets/entry`.

Create a production build of your assets, whose builds will be placed into
the above configured public path `/sites/all/assets/entry`:

```bash
npx encore production
# Or use this wrapper command if you have Drush installed
# and you have the project folder location defined (see section 3.3).
drush entrypoints:rebuild --production
```

For more info regards how to setup your Webpack configuration with Encore, see
https://symfony.com/doc/current/frontend/encore/installation.html

After creating the build, you can include the defined entrypoint "navbar"
as a regular Drupal library like the following inside your mymodule.module file:

```php
/**
 * Implements hook_page_attachments().
 */
function mymodule_page_attachments(array &$page) {
  $page['#attached']['library'][] = 'entrypoints/navbar';
}
```

Instead of writing the code shown above, you could also use one of the
convenient attachment settings, which can be found in the configuration form
at `/admin/config/entrypoints`. For more advanced use cases though,
attaching entrypoints conditionally - e.g. only when a certain block
is being shown - it's recommended to always only attach the entrypoint libraries
when they are needed. For more details on how to accomplish this, have a look at
https://www.drupal.org/docs/creating-custom-modules/adding-stylesheets-css-and-javascript-js-to-a-drupal-module#attaching.

If you have a running site, clear Drupal's cache so that the library uses your
most recent version of compiled assets.

## 3. Recommendations

### 3.1 Avoid development builds on production sites

Within your Webpack configuration (example see section 2), it's recommended
to use a separate output directory for production and development purposes.

The paths to the output folders are configurable within `entrypoints.settings`
at `/admin/config/entrypoints` and can be defined separately per production
and development build mode.

The current build mode of the site can be switched by setting
`entrypoints.settings.build_mode` to either `development` or `production`.

If you want to avoid accidential development builds to be used on your
production site, you can enforce a specific mode to be used via settings.php
configuration override. For example, use this config override to always use
production builds on your production environment:

```php
$config['entrypoints.settings']['build_mode'] = 'production';
```

By following this approach, it's also recommended to exclude the
development output path from your VCS by adding it to your .gitignore file.

For more information about configuration overrides, have a look at
https://www.drupal.org/docs/drupal-apis/configuration-api/configuration-override-system.

You can also restrict to overall available build modes, to avoid confusion on
configuration forms when you enforce a specific build mode anyway. You can do
so by overriding the corresponding service parameter `entrypoints.build_modes`.
If you already have a site-specific services.yml file, you can add the
parameter override like the following:

```yaml
parameters:
  entrypoints.build_modes:
    production: Production
```

### 3.2 Choose your asset build strategy

There are two options to consider regards compiling assets for production.
It is important to know the consequences of each option, and it's strongly
recommended to clearly decide for one of them.

**Option 1**:
```javascript
Encore.enableVersioning(false).cleanupOutputBeforeBuild()
```
This option cleans up all previously generated asset builds, and does not
include a version hash in the asset build filenames.
It has the following consequences:
- Good: The output files will be of the same file name
  and thus will reduce output "polluting" to a minimum.
- Bad: An immediate Drupal cache rebuild is required, so that the Drupal
  libraries are ensured to use all of the most recent builds.
- Good: The Drupal libraries will always use the most recent version after
  a cache rebuild, because the internal Drupal library versioning involves
  change detection by calculating file content hashes.
- Bad: When using Webpack's built-in support for dynamic imports, a Browser- or
  CDN-cached asset might be loaded that is older than the most recent version.
- Bad: This option is not recommended for bulding assets outside of a regular
  site deployment process. Because a live site might access a set of incomplete
  built assets during compilation time, frontend might be more prone to errors.
- Bad: Backwards compatibility might break on older cached sites. This might
  be a problem, e.g. when a cached HTML document contains a link to an older
  entrypoint version that is not compatible with the new asset version. Also,
  newly added (or removed) asset chunks might be missed within the previously
  cached output, especially when Drupal's built-in aggregation is not applied
  on your entrypoint assets.

**Option 2**:
```javascript
Encore.enableVersioning(Encore.isProduction()).cleanupOutputBeforeBuild();
```
This option cleans up all previously generated asset builds, and includes
a version hash in the asset build filenames on production.
It has the following consequences:
- This option has the same advantages and disadvantages as listed in option 1.
- It's more prone to the problem, that an immediate Drupal cache rebuild is
  required for using the newly created builds. When new file versions are
  created, the Drupal libraries still try to access the previous ones,
  until a cache rebuild was run.

**Option 3**:
```javascript
Encore.enableVersioning(Encore.isProduction());
```
This option keeps previously generated asset builds - i.e.
`cleanupOutputBeforeBuild()` is not being used here - and
includes a version hash in the asset build filenames on production.
It has the following consequences:
- Good/bad: This option is better suited than the others when you want to
  rebuild assets outside of a regular site deployment process. But you still
  need to run a Drupal cache rebuild in order to make use of the most recent
  builds.
- Good: Backwards compatibility with cached HTML that link to older asset
  versions can be kept up, also when not including the asset builds within
  Drupal's internal CSS & CS aggregation.
- Good: Very old stale output files will be removed automatically, because the
  Drupal cache rebuild involves a cleanup of stale files, that are
  older than the `system.performance.stale_file_threshold` timestamp value.
- Bad: The output directory might be heavily polluted on frequent asset
  rebuilds within a short period of time.
- Bad: In case of a major flaw within your assets, you need to cleanup
  the output directory manually.

So which option is the right one? For most cases, **option 1 for development**,
and **option 3 for production** should be fine along with the following:
- Drupal CSS & JS aggregation enabled via `system.performance`
  (/admin/config/development/performance).
- Entrypoints are allowed to be included within Drupal CSS & JS aggregation via
  `entrypoints.settings` (`/admin/config/entrypoints`).
- Avoiding the rebuild of assets outside of a regular site deployment.
- If you're in a situation on production, where you need to cleanup the output
  directory on production, you can run the rebuild command including `--cleanup`
  option, i.e. `drush entrypoints:rebuild --production --cleanup=0`, which
  removes all stale files right *after* the build process.
This should prevent a bunch of possible headaches because of a broken frontend.

Of course you're free to decide and also use a different option than the ones
mentioned above. If you have an option that you think has explicitly better
advantages than the ones listed here, please describe it in the issue queue
and I can add your option to this list to help others on their decision.
Thank you!

### 3.3 Include the asset compilation in your CI deployment

This module can help you to include the asset compilation into your
CI / deployment workflow. In your package.json, you may define the scripts
to be run by the Drush command `entrypoints:rebuild`.

Within the `entrypoints.settings` configuration, you can reference to the
defined script keys for asset compilation in your package.json.

You can then use the following command to compile the production assets,
which includes a rebuild of the Drupal library definitions:

```bash
drush entrypoints:rebuild --production
```

This command prompts a warning, if the `--development` preset is being used
but `entrypoints.settings.build_mode` is set to `production` (and vice versa).
The corresponding Drupal library will use the asset output path by the given
`entrypoints.settings.build_mode`, regardless of the build preset being used.

If you are confident to not break any features at deployment building, you can
include the `--update` flag to the rebuild command. With that option, the
command performs an update on the asset packages before creating new builds.

## 4. Features

1. When `Encore.splitEntryChunks()` is enabled, this module takes care
   of proper asset aggregation of the commonly used chunks (if enabled in
   configuration by setting the `merge_chunks` optimization option). It does so
   by generating "artificial" Drupal library definitions, which will be used as
   regular library dependencies on the entrypoint library definitions.

2. To support independent asset projects, multiple asset output folders can be
   defined within the module configuration `entrypoints.settings`. Please note
   that it's mostly easier to have one single repository for managing multiple
   asset components, to avoid possible headaches e.g. by versioning conflicts
   and/or scopes. Also please note that there are no explicit "namespaces"
   between multiple asset projects. **You need to take care of unique entrypoint
   names on your own.**

When Node.js is available:

3. Server-side Rendering (SSR) is possible. To use it, you need to assign an
   available renderer plugin (`entrypoints.settings.renderer`) in the
   entrypoints configuration at `/admin/config/entrypoints`. You have a separate
   output folder for server-side rendering logic, to be defined for each project
   in the `location_ssr` field (labeled as "Output folder of scripts for
   server-side rendering (SSR)" in the configuration form). That folder will be
   automatically protected from public access with an .htacccess file.

   Entrypoints which have both client-side and server-side rendering logic will
   be handled automatically by their matching names. If you have a "big" server-
   side rendering script for all your client-side entrypoint implementations,
   you can assign that SSR entrypoint in the default and override entrypoint
   settings (section "Server-side rendering (SSR)").

   The server-side rendering will be invoked during the response building chain.
   Any SSR entrypoint will be passed a `DrupalServer` object that contains the
   raw response `content` as property, which can be arbitrarily modified by the
   entrypoint implementation. You can write a custom Drupal plugin of type
   `EntrypointsInputHandlerPluginInterface` that may change or provide further
   data to be passed as input to the SSR process.

   To understand how server-side rendering works with this module, you can try
   out the "kickstart" project creation command
   `drush entrypoints:create-project ../assets/entry --template=default-ssr`
   There is also a React template `react-ssr` available, that shows how to
   use `ReactDOMServer.renderToString` on the server-side and `ReactDOM.hydrate`
   on the client-side.

   If you are interested in implementing further renderer plugins, e.g. by using
   V8JS or SpiderMonkey, you are welcome to contribute to this project. Create
   an issue regards your requests / contributions or if you need any help at the
   Drupal issue queue (https://www.drupal.org/project/issues/entrypoints).

When NPM / YARN is available:

4. Includes Drush `entrypoints` command-sets that wrap NPM / YARN based
   scripts for rebuilds, updates and checks regarding outdated packages
   inside of your defined asset projects.

5. The Drupal status page `/admin/reports/status` includes info about possible
   outdated packages within your asset project folders.

**Have an environment without any NPM or YARN runtime at all?**
No problem. Just specify your production config without any build runtime.
The whole build process is optional. If defined, the entrypoints are still
being included as regular Drupal libraries. The module will complain though,
if you have a runtime or build mode configured that is not available.

## 5. Planned features

1. Dynamic Drupal Library Imports is a planned feature to be included, as soon
   as a proof of concept has been implemented. Webpack's built-in support for
   asynchronous code splitting might already be possible, but has not been
   tested yet. For more information about this topic, have a look at
   https://symfony.com/doc/current/frontend/encore/code-splitting.html.

## 6. Missing features

1. This module is not meant to be an equivalent of Symfony's Asset component.
   See also https://symfony.com/doc/current/components/asset.html regards the
   Asset component. But you can obtain entries from the generated manifest.json
   files with the help of the `ManifestEntry` class. Entrypoint definitions
   can be obtained with the help of the `EntrypointDefinition` class.

## 7. Use cases

### 7.1 UI Patterns

The UI Patterns module (https://www.drupal.org/project/ui_patterns) enables you
to define self-contained component patterns. With that framework, you can
define a whole component at one place, including all corresponding libraries.

Backend engineers may define the components with possible entrypoint libraries.
The pattern is easy to understand, and may be previewed.

Frontend engineers can introspect and understand the definitions without
needing to know Drupal at its entirety. They may focus on implementing the
corresponding assets as entrypoints. They can write and build their asset
implementations with their known tools.

The asset builds will be written into the configured output directories,
which will then be automatically included due to their pattern definitions.

<?php

/**
 * @file
 * The entrypoints module file.
 */

/**
 * Implements hook_cron().
 */
function entrypoints_cron() {
  entrypoints_rebuild();
  \Drupal::service('entrypoints.output_protection')->ensure();
}

/**
 * Implements hook_cache_flush().
 */
function entrypoints_cache_flush() {
  \Drupal::service('entrypoints.source')->clearCache();
  \Drupal::service('entrypoints.registry')->clearCache();
  \Drupal::service('entrypoints.runtime')->clearCache();
  \Drupal::service('entrypoints.renderer')->clearCache();
}

/**
 * Implements hook_rebuild().
 */
function entrypoints_rebuild() {
  $threshold = \Drupal::config('system.performance')->get('stale_file_threshold');
  try {
    \Drupal::service('entrypoints.cleanup')->deleteStaleFiles($threshold);
  }
  catch (\Exception $e) {
    \Drupal::logger('entrypoints')->error('An error occurred while trying to delete stale files of entrypoint output folders. The error was: %message', ['%message' => $e->getMessage()]);
  }
}

/**
 * Implements hook_library_info_build().
 */
function entrypoints_library_info_build() {
  $libraries = [];
  try {
    $libraries = \Drupal::service('entrypoints.libraries')->buildLibrariesInfo();
  }
  catch (\Exception $e) {
    \Drupal::logger('entrypoints')->error('An error occurred while trying to build the library info for entrypoints. The error was: %message', ['%message' => $e->getMessage()]);
  }
  return $libraries;
}

/**
 * Implements hook_page_attachments().
 */
function entrypoints_page_attachments(array &$attachments) {
  $config = \Drupal::config('entrypoints.settings');
  $settings = [];
  if ($defaults = $config->get('defaults.attach')) {
    $settings[] = $defaults;
  }
  if ($overrides = $config->get('overrides')) {
    foreach ($overrides as $override) {
      if (!empty($override['attach'])) {
        $settings[] = $override['attach'];
      }
    }
  }

  $libraries = [];
  $everywhere = FALSE;
  $non_admin = FALSE;
  $paths = [];
  $themes = [];
  foreach ($settings as $attach) {
    if (!empty($attach['everywhere'])) {
      $everywhere = TRUE;
    }
    if (!empty($attach['non_admin'])) {
      $non_admin = TRUE;
    }
    if (!empty($attach['path']) && !in_array($attach['path'], $paths)) {
      $paths[] = $attach['path'];
    }
    if (!empty($attach['theme']) && !in_array($attach['theme'], $themes)) {
      $themes[] = $attach['theme'];
    }
  }
  if ($everywhere) {
    $entrypoints = \Drupal::service('entrypoints.registry')->getDefinitionsBySettings(['attach' => ['everywhere' => TRUE]]);
    foreach ($entrypoints as $entrypoint) {
      if (!in_array($entrypoint->getLibraryName(), $libraries)) {
        $libraries[] = $entrypoint->getLibraryName();
      }
    }
  }

  if ($non_admin && !\Drupal::service('router.admin_context')->isAdminRoute()) {
    $entrypoints = \Drupal::service('entrypoints.registry')->getDefinitionsBySettings(['attach' => ['non_admin' => TRUE]]);
    foreach ($entrypoints as $entrypoint) {
      if (!in_array($entrypoint->getLibraryName(), $libraries)) {
        $libraries[] = $entrypoint->getLibraryName();
      }
    }
  }

  if (!empty($paths)) {
    foreach ($paths as $path) {
      $path_pattern = mb_strtolower($path);
      $current_path = \Drupal::service('path.current')->getPath();
      $current_path = $current_path === '/' ? $current_path : rtrim($current_path, '/');
      $path_alias = mb_strtolower(\Drupal::service('path_alias.manager')->getAliasByPath($current_path));
      $path_matcher = \Drupal::service('path.matcher');
      if ($path_matcher->matchPath($path_alias, $path_pattern) || (($current_path != $path_alias) && $path_matcher->matchPath($current_path, $path_pattern))) {
        $entrypoints = \Drupal::service('entrypoints.registry')->getDefinitionsBySettings(['attach' => ['path' => $path]]);
        foreach ($entrypoints as $entrypoint) {
          if (!in_array($entrypoint->getLibraryName(), $libraries)) {
            $libraries[] = $entrypoint->getLibraryName();
          }
        }
      }
    }
  }

  if (!empty($themes) && \Drupal::theme()->hasActiveTheme()) {
    $theme = \Drupal::theme()->getActiveTheme();
    $active_themes = [$theme->getName()];
    foreach ($theme->getBaseThemeExtensions() as $base) {
      $active_themes[] = $base->getName();
    }
    foreach ($active_themes as $active_theme) {
      if (in_array($active_theme, $themes)) {
        $entrypoints = \Drupal::service('entrypoints.registry')->getDefinitionsBySettings(['attach' => ['theme' => $active_theme]]);
        foreach ($entrypoints as $entrypoint) {
          if (!in_array($entrypoint->getLibraryName(), $libraries)) {
            $libraries[] = $entrypoint->getLibraryName();
          }
        }
      }
    }
  }

  foreach ($libraries as $library) {
    $attachments['#attached']['library'][] = $library;
  }
}

/**
 * Implements hook_page_attachments_alter().
 */
function entrypoints_page_attachments_alter(array &$attachments) {
  if (empty($attachments['#attached']['library']) || \Drupal::service('router.admin_context')->isAdminRoute()) {
    return;
  }
  if (\Drupal::moduleHandler()->moduleExists('layout_builder') && strpos(\Drupal::routeMatch()->getRouteName(), 'layout_builder') === 0) {
    return;
  }
  _entrypoints_page_attachments_disable_active_link($attachments);
  _entrypoints_page_attachments_disable_system_base($attachments);
  _entrypoints_page_attachments_disable_contextual_links($attachments);
}

/**
 * Removes the active link library, if configured.
 *
 * @param array &$attachments
 *   The page attachments array.
 */
function _entrypoints_page_attachments_disable_active_link(array &$attachments) {
  if (!\Drupal::config('entrypoints.settings')->get('tweaks.disable_active_link')) {
    return;
  }
  $index = array_search('core/drupal.active-link', $attachments['#attached']['library']);
  if ($index !== FALSE) {
    unset($attachments['#attached']['library'][$index]);
  }
}

/**
 * Removes the system base library, if configured.
 *
 * @param array &$attachments
 *   The page attachments array.
 */
function _entrypoints_page_attachments_disable_system_base(array &$attachments) {
  if (!\Drupal::config('entrypoints.settings')->get('tweaks.disable_system_base')) {
    return;
  }
  $index = array_search('system/base', $attachments['#attached']['library']);
  if ($index !== FALSE) {
    unset($attachments['#attached']['library'][$index]);
  }
}

/**
 * Removes the contextual links library, if configured.
 *
 * @param array &$attachments
 *   The page attachments array.
 */
function _entrypoints_page_attachments_disable_contextual_links(array &$attachments) {
  if (!\Drupal::config('entrypoints.settings')->get('tweaks.disable_contextual_links') || !\Drupal::moduleHandler()->moduleExists('contextual')) {
    return;
  }
  $index = array_search('contextual/drupal.contextual-links', $attachments['#attached']['library']);
  if ($index !== FALSE) {
    unset($attachments['#attached']['library'][$index]);
  }
}

/**
 * Implements hook_toolbar_alter().
 */
function entrypoints_toolbar_alter(&$items) {
  if (!isset($items['contextual']) || !\Drupal::config('entrypoints.settings')->get('tweaks.disable_contextual_links') || !\Drupal::moduleHandler()->moduleExists('contextual')) {
    return;
  }
  $items['contextual']['#cache']['contexts'][] = 'route.name';
  if (\Drupal::service('router.admin_context')->isAdminRoute()) {
    return;
  }
  if (\Drupal::moduleHandler()->moduleExists('layout_builder') && strpos(\Drupal::routeMatch()->getRouteName(), 'layout_builder') === 0) {
    return;
  }
  $cache = $items['contextual']['#cache'];
  $items['contextual'] = ['#cache' => $cache];
}

/**
 * Implements hook_preprocess().
 */
function entrypoints_preprocess(&$variables, $hook, $info) {
  if (!isset($variables['title_suffix']['contextual_links']) || !\Drupal::config('entrypoints.settings')->get('tweaks.disable_contextual_links')) {
    return;
  }
  $variables['#cache']['contexts'][] = 'route.name';
  if (\Drupal::service('router.admin_context')->isAdminRoute()) {
    return;
  }
  if (\Drupal::moduleHandler()->moduleExists('layout_builder') && strpos(\Drupal::routeMatch()->getRouteName(), 'layout_builder') === 0) {
    return;
  }
  unset($variables['title_suffix']['contextual_links']);
  if (!empty($info['variables'])) {
    $keys = array_keys($info['variables']);
    $key = $keys[0];
  }
  elseif (!empty($info['render element'])) {
    $key = $info['render element'];
  }
  if (!empty($key) && isset($variables[$key]['#contextual_links'])) {
    if (!empty($variables['attributes']['class'])) {
      $variables['attributes']['class'] = array_filter($variables['attributes']['class'], function($value) {
        return $value !== 'contextual-region';
      });
    }
    unset($variables[$key]['#contextual_links']);
  }
}

/**
 * Get the Drupal app root.
 *
 * @internal
 *
 * @return string
 *   The Drupal app root.
 */
function _entrypoints_app_root() {
  $app_root = &drupal_static(__FUNCTION__);
  if (!isset($app_root)) {
    $app_root = str_replace('/', DIRECTORY_SEPARATOR, \Drupal::root());
  }
  return $app_root;
}

import React from 'react';
import ReactDOM from 'react-dom';

ReactDOM.hydrate(
  <React.StrictMode>
    <h1>Say hello from server-side rendered React</h1>
    <div>I replaced all of this. Please replace me too!</div>
  </React.StrictMode>,
  document.querySelector('#ssr-root')
);

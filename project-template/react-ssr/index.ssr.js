import React from 'react';
import ReactDOMServer from 'react-dom/server';

(function (DrupalServer, content) {
  let result = ReactDOMServer.renderToString(
    <React.StrictMode>
      <h1>Say hello from server-side rendered React</h1>
      <div>I replaced all of this. Please replace me too!</div>
    </React.StrictMode>
  );
  // DrupalServer object manipulation will update the document content
  // for the current response.
  let bodyTag = content.match(/\<body[^\>]*\>/i)[0];
  DrupalServer.content = content.replace(bodyTag, bodyTag + '<div id="ssr-root">' + result + '</div>');
})(DrupalServer, DrupalServer.content);

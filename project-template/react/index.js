import React from 'react'
import ReactDOM from 'react-dom'

ReactDOM.render(
  <React.StrictMode>
    <h1>Say hello from React</h1>
    <div>I replaced all of this. Please replace me too!</div>
  </React.StrictMode>,
  document.querySelector('div')
)

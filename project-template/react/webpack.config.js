const path = require('path');
const webpack = require('webpack');
const Encore = require('@symfony/webpack-encore');
if (!Encore.isRuntimeEnvironmentConfigured()) {
  Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

const drupalRoot = ':drupal_root'.replace('/', path.sep);
const outputBasedir = ':output_basedir'.replace('/', path.sep);
const outputFolder = ':output_folder'.replace('/', path.sep);
const outputDevFolder = ':output_dev_folder'.replace('/', path.sep);

Encore
  .addEntry(':entrypoint', './src/:entrypoint.js')
  .splitEntryChunks()
  .enableSingleRuntimeChunk()
  .enableBuildNotifications()
  .configureBabelPresetEnv((config) => {
    config.useBuiltIns = false;
  })
  ;

if (Encore.isProduction()) {
  Encore
    .setOutputPath(drupalRoot + path.sep + outputBasedir + path.sep + outputFolder)
    .setPublicPath(('/' + outputBasedir + '/' + outputFolder).replace(path.sep, '/'))
    .enableVersioning(true)
    .enableSourceMaps(false)
    ;
}
else {
  Encore
    .setOutputPath(drupalRoot + path.sep + outputBasedir + path.sep + outputDevFolder)
    .setPublicPath(('/' + outputBasedir + '/' + outputDevFolder).replace(path.sep, '/'))
    .enableVersioning(false)
    .cleanupOutputBeforeBuild()
    .enableSourceMaps(true)
    ;
}

Encore.enableReactPreset();

const config = Encore.getWebpackConfig();

config.plugins.push(new webpack.ProvidePlugin({
  React: 'react'
}));

module.exports = config;
